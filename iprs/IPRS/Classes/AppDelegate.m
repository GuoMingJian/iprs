/*
 Licensed to the Apache Software Foundation (ASF) under one
 or more contributor license agreements.  See the NOTICE file
 distributed with this work for additional information
 regarding copyright ownership.  The ASF licenses this file
 to you under the Apache License, Version 2.0 (the
 "License"); you may not use this file except in compliance
 with the License.  You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing,
 software distributed under the License is distributed on an
 "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 KIND, either express or implied.  See the License for the
 specific language governing permissions and limitations
 under the License.
 */

//
//  AppDelegate.m
//  IPRS
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//  Copyright ___ORGANIZATIONNAME___ ___YEAR___. All rights reserved.
//

#import "AppDelegate.h"
#import "MainViewController.h"
//推送
#import "NewsPush.h"
#import "UserSetUpServer.h"
#import "PullTheMessage.h"
#import <UserNotifications/UserNotifications.h>
//通用头文件
#import "CommentDefine.h"
//地图
#import <BaiduMapAPI_Base/BMKBaseComponent.h>
#import <BaiduMapAPI_Location/BMKLocationComponent.h>
#import "MJSoundPlayer.h"
#import "TBTipView.h"

#define kAPS_ID @"kAPS_ID"

@interface AppDelegate()<UNUserNotificationCenterDelegate, BMKGeneralDelegate>
//是否未启动app收到的消息
@property (nonatomic, assign, getter = isOnLine) BOOL onLine;
//地图管理者
@property (nonatomic, strong) BMKMapManager *mapManager;
//定位管理者
@property (nonatomic, strong) CLLocationManager* locationManager;
@end
@implementation AppDelegate

- (BOOL)application:(UIApplication*)application didFinishLaunchingWithOptions:(NSDictionary*)launchOptions
{
    self.viewController = [[MainViewController alloc] init];
#if TARGET_IPHONE_SIMULATOR
#else
    //注册推送
    [self pushRegister];
    //注册本地通知
    [self setUserNotificationSettings];
    //正常启动
    _onLine = YES;
    if (launchOptions) {
        NSDictionary *dicUserInfo = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
        if(dicUserInfo) {
            _onLine = NO;
            [self handlePushMsg:dicUserInfo];
        }
    }
    //地图初始化
    _mapManager = [[BMKMapManager alloc]init];
    BOOL bmkSuccess = [_mapManager start:kBaiduMapKit generalDelegate:self];
    if (!bmkSuccess) {
        NSLog(@"百度地图注册失败");
    }else {
        NSLog(@"百度地图注册成功");
    }
    
    //由于IOS8中定位的授权机制改变 需要进行手动授权
    _locationManager = [[CLLocationManager alloc] init];
    //获取授权认证
    [_locationManager requestAlwaysAuthorization];
    [_locationManager requestWhenInUseAuthorization];
#endif
    return [super application:application didFinishLaunchingWithOptions:launchOptions];
}

#pragma mark - 注册推送
- (void)pushRegister {
#if TARGET_IPHONE_SIMULATOR
#else
    [UserSetUpServer sharedNetworkRequest].serverUrl = kPushUrl;
    [UserSetUpServer sharedNetworkRequest].apikey = @"IPRS";
    [NewsPush registerForRemoteNotificationTypes:(UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert) categories:nil];
#endif
}

#pragma mark - 注册推送设备ID
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
#if TARGET_IPHONE_SIMULATOR
#else
    NSMutableString *deviceTokenStr = [NSMutableString string];
    const char *bytes = deviceToken.bytes;
    int iCount = (int)deviceToken.length;
    for (int i = 0; i < iCount; i++) {
        [deviceTokenStr appendFormat:@"%02x", bytes[i]&0x000000FF];
    }
    NSLog(@"deviceToken：%@", deviceTokenStr);
    [NewsPush registerDeviceToken:deviceToken];
    NSLog(@"注册推送成功");
#endif
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error{
    NSLog(@"注册推送失败");
}

#pragma mark - 注册本地通知
- (void)setUserNotificationSettings {
#if TARGET_IPHONE_SIMULATOR
#else
    // 设置应用程序的图标右上角的数字
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    
    // 使用 UNUserNotificationCenter 来管理通知
    UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
    //监听回调事件
    center.delegate = self;
    
    //iOS 10 使用以下方法注册，才能得到授权
    [center requestAuthorizationWithOptions:(UNAuthorizationOptionAlert + UNAuthorizationOptionSound)
                          completionHandler:^(BOOL granted, NSError * _Nullable error) {
                              // Enable or disable features based on authorization.
                          }];
#endif
}

//在展示通知前进行处理，即有机会在展示通知前再修改通知内容。
- (void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions))completionHandler {
#if TARGET_IPHONE_SIMULATOR
#else
    completionHandler(UNNotificationPresentationOptionAlert);
    //==
    NSDictionary *userInfo = notification.request.content.userInfo;
    NSString *apsID = userInfo[@"id"];
    [[NSUserDefaults standardUserDefaults] setObject:apsID forKey:kAPS_ID];
    [self handlePushMsg:userInfo];
#endif
}

- (void)handlePushMsg:(NSDictionary *)userInfo
{
    [NewsPush handleRemoteNotification:userInfo];
    [NewsPush getMessage:userInfo callback:@selector(getExtrasMessage:) object:self];
}

#pragma mark - iOS7+ 接收推送
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult result))completionHandler {
#if TARGET_IPHONE_SIMULATOR
#else
    if(self.isOnLine)
    {
        //在线
        NSString *idStr = userInfo[@"id"];
        NSString *apsID = [[NSUserDefaults standardUserDefaults] objectForKey:kAPS_ID];
        if ([idStr isEqualToString:apsID])
        {
            NSLog(@"本地通知已处理该通知。--%@", apsID);
        }
        else
        {
            [self handlePushMsg:userInfo];
            [[NSUserDefaults standardUserDefaults] setObject:idStr forKey:kAPS_ID];
        }
    }
    else
    {
        [self application:application didFinishLaunchingWithOptions:userInfo];
    }
#endif
}

#pragma mark - 解析推送消息
- (void)getExtrasMessage:(id)message
{
#if TARGET_IPHONE_SIMULATOR
#else
    if ([message isKindOfClass:[PullTheMessage class]])
    {
        PayLoad *payLoad = ((PullTheMessage *)message).payload;
        if (!payLoad.extras)
        {
            payLoad.extras = [NSMutableDictionary dictionary];
        }
        NSString *msg = payLoad.alert;
        [self soundPlay:msg];
    }
#endif
}

#pragma mark - 语音播报
- (void)soundPlay:(NSString *)content
{
    [[MJSoundPlayer shareInstance] play:content];
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    [[UIApplication sharedApplication]setApplicationIconBadgeNumber:0];//进入前台取消应用消息图标
}

@end
