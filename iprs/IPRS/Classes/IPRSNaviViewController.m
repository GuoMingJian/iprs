//
//  IPRSNaviViewController.m
//  IPRS
//
//  Created by linyingwei on 2018/5/4.
//

#import "IPRSNaviViewController.h"

@interface IPRSNaviViewController ()

@end

@implementation IPRSNaviViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    //颜色
    self.navigationBar.barTintColor = kColorWithRGB(53, 147, 214);
    self.navigationBar.tintColor = [UIColor whiteColor];
    //标题
    [self.navigationBar setTitleTextAttributes:
     @{NSFontAttributeName:[UIFont systemFontOfSize:19],
       NSForegroundColorAttributeName:[UIColor whiteColor]}];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
