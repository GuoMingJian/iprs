//
//  InAppBrowser.h
//  IPRS
//
//  Created by 郭明健 on 2018/7/26.
//

#import <Cordova/CDV.h>

@interface InAppBrowser : CDVPlugin

- (void)open:(CDVInvokedUrlCommand *)command;

@end
