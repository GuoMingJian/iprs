//
//  UIImage+WLCompress.m
//  merWalletMerWalletIphone
//
//  Created by linyingwei on 2018/4/2.
//

#import "UIImage+WLCompress.h"

@implementation UIImage (WLCompress)
#pragma mark - 压缩图片到指定大小
- (NSData *)compressQualityWithMaxLength:(NSInteger)maxLength {
    CGFloat compression = 1;
    NSData *data = UIImageJPEGRepresentation(self, compression);
    if (data.length < maxLength) return data;
    CGFloat max = 1;
    CGFloat min = 0;
    for (int i = 0; i < 6; ++i) {
        compression = (max + min) / 2;
        data = UIImageJPEGRepresentation(self, compression);
        if (data.length < maxLength * 0.9) {
            min = compression;
        } else if (data.length > maxLength) {
            max = compression;
        } else {
            break;
        }
    }
    return data;
}

#pragma mark - 压缩图片到指定尺寸和加个水印
- (UIImage *)compressOriginaltoSize:(CGSize)size text:(NSString *)text{
    UIImage * resultImage = self;
    //上下文
    UIGraphicsBeginImageContext(size);
    //绘制大小
    [resultImage drawInRect:CGRectMake(0, 0, size.width, size.height)];
    //格式
    NSMutableParagraphStyle *textStyle = [[NSMutableParagraphStyle defaultParagraphStyle] mutableCopy];
    //居右
    textStyle.alignment = NSTextAlignmentRight;
    //字体
    UIFont * font = [UIFont systemFontOfSize:18.0];
    //构建属性集合
    NSDictionary *attributes = @{NSFontAttributeName:font, NSParagraphStyleAttributeName:textStyle,NSForegroundColorAttributeName:[UIColor redColor]};
    //绘制
    [text drawInRect:CGRectMake(0, size.height - 21, size.width, size.height) withAttributes:attributes];
    //获取上下文
    UIImage * newImage = UIGraphicsGetImageFromCurrentImageContext();
    //完成绘制
    UIGraphicsEndImageContext();
    return newImage;
}

@end
