//
//  IPRSTakePhotoes.m
//  IPRS
//
//  Created by linyingwei on 2018/5/3.
//

#import "IPRSTakePhotoes.h"
#import "CommentDefine.h"
#import "UIImage+WLCompress.h"

#define kMaxImageSize 500 * 1000 //500K

@interface IPRSTakePhotoes()<UIImagePickerControllerDelegate, UINavigationControllerDelegate>
@end
@implementation IPRSTakePhotoes
#pragma mark - 打开相册
- (void)openCamera{
    // 打开相机
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        [self openWithSourceType:UIImagePickerControllerSourceTypeCamera];
    }
}

#pragma mark - 打开相机
- (void)openPhotoLibrary{
    // 打开相册
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary])
    {
        [self openWithSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
    }
}

#pragma mark  -- 打开相机或相册
/**
 *  打开相机或相册
 */
- (void)openCameraOrPhotoLibrary
{
    UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    [kRootVc presentViewController:alertVC animated:YES completion:nil];
    
    [alertVC addAction:[UIAlertAction actionWithTitle:@"相机" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        // 打开相机 比较懒，暂时先这样获取访问权限
        if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
        {
            [self openWithSourceType:UIImagePickerControllerSourceTypeCamera];
        }
    }]];
    
    [alertVC addAction:[UIAlertAction actionWithTitle:@"相册" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        // 打开相册
        if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary])
        {
            [self openWithSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
        }
    }]];
    
    [alertVC addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil]];
}

//
- (void)openWithSourceType:(UIImagePickerControllerSourceType)sourceType
{
    UIImagePickerController *imagePickerVC = [[UIImagePickerController alloc] init];
    imagePickerVC.sourceType = sourceType;
    imagePickerVC.delegate = self;
    
    [kRootVc presentViewController:imagePickerVC animated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
//    NSLog(@"%@",info[UIImagePickerControllerOriginalImage]);
    UIImage *img = info[UIImagePickerControllerOriginalImage];
    NSData *data = [self compressImage:img toMaxImageSize:kMaxImageSize];
//    NSData *data = [img compressQualityWithMaxLength:100];
    if (_photoBlock) {
        _photoBlock([data base64EncodedStringWithOptions:0]);
    }
    [kRootVc dismissViewControllerAnimated:YES completion:nil];
}

/**
 压缩图片  maxImageSize 单位 byte
 */
- (NSData *)compressImage:(UIImage *)image
           toMaxImageSize:(NSInteger)maxImageSize
{
    image = [self fixImageOrientation:image];//矫正图片朝向
    CGFloat compression = 0.9f;//当前压缩比
    CGFloat maxCompression = 0.1f;//最大压缩比
    NSData *imageData = UIImageJPEGRepresentation(image, compression);

    while (imageData.length > maxImageSize && compression > maxCompression)
    {
        if (imageData.length > 4 * 1000 * 1000)
        {//  >4M
            compression -= 0.4f;
            maxCompression = 0.4f;
        }
        else if(imageData.length > 2 * 1000 * 1000 && imageData.length <= 4 * 1000 * 1000)
        {//  2-4M
            compression -= 0.3f;
            maxCompression = 0.3f;
        }
        else if(imageData.length > 1 * 1000 * 1000 && imageData.length <= 2 * 1000 * 1000)
        {//  1-2M
            compression -= 0.2f;
            maxCompression = 0.2f;
        }
        else
        {//  <=1M
            compression -= 0.1f;
            maxCompression = 0.1f;
        }
        imageData = UIImageJPEGRepresentation(image, compression);
    }

    UIImage *compressedImage = [UIImage imageWithData:imageData];
    if (imageData.length > maxImageSize)
    {
        //压缩后还是超出最大size，则进行图片裁剪。
        CGFloat cutWidth = 10;
        [self image:compressedImage scaledToSize:CGSizeMake(compressedImage.size.width - cutWidth, compressedImage.size.height - cutWidth)];
    }

    return imageData;
}

/**
 裁剪图片
 */
- (UIImage *)image:(UIImage *)image
      scaledToSize:(CGSize)newSize
{
    UIGraphicsBeginImageContext(newSize);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    return newImage;
}

/**
 修复图片朝向
 */
- (UIImage *)fixImageOrientation:(UIImage *)aImage
{
    // 无操作如果取向已经正确
    if (aImage.imageOrientation == UIImageOrientationUp)
        return aImage;

    // 我们需要计算合适的变换使正直的形象。
    // 我们用2步骤:如果左/右/向下旋转,然后如果镜像翻转。
    CGAffineTransform transform = CGAffineTransformIdentity;

    switch (aImage.imageOrientation) {
        case UIImageOrientationDown:
        case UIImageOrientationDownMirrored:
            transform = CGAffineTransformTranslate(transform, aImage.size.width, aImage.size.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;

        case UIImageOrientationLeft:
        case UIImageOrientationLeftMirrored:
            transform = CGAffineTransformTranslate(transform, aImage.size.width, 0);
            transform = CGAffineTransformRotate(transform, M_PI_2);
            break;

        case UIImageOrientationRight:
        case UIImageOrientationRightMirrored:
            transform = CGAffineTransformTranslate(transform, 0, aImage.size.height);
            transform = CGAffineTransformRotate(transform, -M_PI_2);
            break;
        default:
            break;
    }

    switch (aImage.imageOrientation) {
        case UIImageOrientationUpMirrored:
        case UIImageOrientationDownMirrored:
            transform = CGAffineTransformTranslate(transform, aImage.size.width, 0);
            transform = CGAffineTransformScale(transform, -1, 1);
            break;

        case UIImageOrientationLeftMirrored:
        case UIImageOrientationRightMirrored:
            transform = CGAffineTransformTranslate(transform, aImage.size.height, 0);
            transform = CGAffineTransformScale(transform, -1, 1);
            break;
        default:
            break;
    }

    // 现在我们把底层CGImage到一个新的背景下,应用变换
    // 上面的计算。
    CGContextRef ctx = CGBitmapContextCreate(NULL, aImage.size.width, aImage.size.height,
                                             CGImageGetBitsPerComponent(aImage.CGImage), 0,
                                             CGImageGetColorSpace(aImage.CGImage),
                                             CGImageGetBitmapInfo(aImage.CGImage));
    CGContextConcatCTM(ctx, transform);
    switch (aImage.imageOrientation) {
        case UIImageOrientationLeft:
        case UIImageOrientationLeftMirrored:
        case UIImageOrientationRight:
        case UIImageOrientationRightMirrored:
            // Grr...
            CGContextDrawImage(ctx, CGRectMake(0,0,aImage.size.height,aImage.size.width), aImage.CGImage);
            break;

        default:
            CGContextDrawImage(ctx, CGRectMake(0,0,aImage.size.width,aImage.size.height), aImage.CGImage);
            break;
    }

    // 现在我们创建一个新的用户界面图像从绘图上下文
    CGImageRef cgimg = CGBitmapContextCreateImage(ctx);
    UIImage *img = [UIImage imageWithCGImage:cgimg];
    CGContextRelease(ctx);
    CGImageRelease(cgimg);
    return img;
}

@end
