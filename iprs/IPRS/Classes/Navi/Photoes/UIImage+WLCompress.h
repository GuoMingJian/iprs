//
//  UIImage+WLCompress.h
//  merWalletMerWalletIphone
//
//  Created by linyingwei on 2018/4/2.
//

#import <UIKit/UIKit.h>

@interface UIImage (WLCompress)
//压缩图片到指定大小
- (NSData *)compressQualityWithMaxLength:(NSInteger)maxLength;
//压缩图片到指定尺寸和加个水印
- (UIImage *)compressOriginaltoSize:(CGSize)size text:(NSString *)text;
@end
