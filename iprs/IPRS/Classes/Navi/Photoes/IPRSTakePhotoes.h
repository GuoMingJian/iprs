//
//  IPRSTakePhotoes.h
//  IPRS
//
//  Created by linyingwei on 2018/5/3.
//

#import <Foundation/Foundation.h>

@interface IPRSTakePhotoes : NSObject
//回调
@property (nonatomic,copy) void (^photoBlock)(NSString *img);
//打开相机或相册
- (void)openCameraOrPhotoLibrary;
//打开相册
- (void)openCamera;
//打开相机
- (void)openPhotoLibrary;
@end
