//
//  CherryCodeSecurityManager.m
//  MobileOfficing
//
//  Created by ChenZheng on 15/11/24.
//  Copyright © 2015年 ICITIC. All rights reserved.
//

#import "CherryCodeSecurityManager.h"
#import <CommonCrypto/CommonCryptor.h>
#import <CommonCrypto/CommonHMAC.h>
#import <CommonCrypto/CommonDigest.h>
#import "sm2.h"
#import "NSData+LBXPadding.h"
#import "NSData+LBXSM4.h"
#import "RSAEnc.h"
#define CRSecurityManager   [CherryCodeSecurityManager sharedInstance]
static NSString * const GMALGORITHM_OPEN_STATUS         = @"1"; // 0关闭国密  1开启国密
static char BASE64_ENCODE_TABLE[64] = {
    65,  66,  67,  68,  69,  70,  71,  72,  // 00 - 07
    73,  74,  75,  76,  77,  78,  79,  80,  // 08 - 15
    81,  82,  83,  84,  85,  86,  87,  88,  // 16 - 23
    89,  90,  97,  98,  99, 100, 101, 102,  // 24 - 31
    103, 104, 105, 106, 107, 108, 109, 110,  // 32 - 39
    111, 112, 113, 114, 115, 116, 117, 118,  // 40 - 47
    119, 120, 121, 122,  48,  49,  50,  51,  // 48 - 55
    52,  53,  54,  55,  56,  57,  43,  47 };// 56 - 63

static unsigned int BASE64_DECODE_TABLE[256] = {
    255, 255, 255, 255, 255, 255, 255, 255, //  00 -  07
    255, 255, 255, 255, 255, 255, 255, 255, //  08 -  15
    255, 255, 255, 255, 255, 255, 255, 255, //  16 -  23
    255, 255, 255, 255, 255, 255, 255, 255, //  24 -  31
    255, 255, 255, 255, 255, 255, 255, 255, //  32 -  39
    255, 255, 255,  62, 255, 255, 255,  63, //  40 -  47
    52,  53,  54,  55,  56,  57,  58,  59, //  48 -  55
    60,  61, 255, 255, 255, 255, 255, 255, //  56 -  63
    255,   0,   1,   2,   3,   4,   5,   6, //  64 -  71
    7,   8,   9,  10,  11,  12,  13,  14, //  72 -  79
    15,  16,  17,  18,  19,  20,  21,  22, //  80 -  87
    23,  24,  25, 255, 255, 255, 255, 255, //  88 -  95
    255,  26,  27,  28,  29,  30,  31,  32, //  96 - 103
    33,  34,  35,  36,  37,  38,  39,  40, // 104 - 111
    41,  42,  43,  44,  45,  46,  47,  48, // 112 - 119
    49,  50,  51, 255, 255, 255, 255, 255, // 120 - 127
    255, 255, 255, 255, 255, 255, 255, 255,
    255, 255, 255, 255, 255, 255, 255, 255,
    255, 255, 255, 255, 255, 255, 255, 255,
    255, 255, 255, 255, 255, 255, 255, 255,
    255, 255, 255, 255, 255, 255, 255, 255,
    255, 255, 255, 255, 255, 255, 255, 255,
    255, 255, 255, 255, 255, 255, 255, 255,
    255, 255, 255, 255, 255, 255, 255, 255,
    255, 255, 255, 255, 255, 255, 255, 255,
    255, 255, 255, 255, 255, 255, 255, 255,
    255, 255, 255, 255, 255, 255, 255, 255,
    255, 255, 255, 255, 255, 255, 255, 255,
    255, 255, 255, 255, 255, 255, 255, 255,
    255, 255, 255, 255, 255, 255, 255, 255,
    255, 255, 255, 255, 255, 255, 255, 255,
    255, 255, 255, 255, 255, 255, 255, 255 };


@implementation CherryCodeSecurityManager

#pragma mark - recycle methods
+(instancetype)sharedInstance
{
    static dispatch_once_t onceToken;
    static CherryCodeSecurityManager *sharedInstance = nil;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[CherryCodeSecurityManager alloc] init];
    });
    return sharedInstance;
}

#pragma mark - public methods
- (NSString *)securitySM2Encrypt:(NSString *)src key:(NSString *)key
{
    if ([src length] == 0 || [key length] == 0) {
        return nil;
    }
    
    if (key.length > 128) {
        key = [key substringFromIndex:2];
    }
    
    unsigned char result[1024] = {0};
    unsigned long outlen = 1024;
//    (char *)[src bytes]
    const char *encryptData = [src UTF8String];
    NSData *keyData =  [self dataFromHexString:key];
    
    int ret = GM_SM2Encrypt(result,&outlen,(unsigned char *)encryptData,strlen(encryptData),(unsigned char *)keyData.bytes,keyData.length);
    
    if (outlen < 2 || ret != MP_OKAY) {
        //加密出错了
        return @"";
    }
    NSData *data = [NSData dataWithBytes:result length:outlen];
    NSLog(@"well.data %@ 长度=%lu",data,outlen);
//    return [self securityHEXEncode:data];
    return [data base64EncodedStringWithOptions:0];
//    return [self securityBytesToBase64:data];
}

- (NSData *)securitySM2Decrypt:(NSData *)data key:(NSString *)key
{
    //密文长度至少也需要64+32位
    if ([data length] < 64 + 32 || [key length] == 0) {
        return nil;
    }
    unsigned char result[1024 * 8] = {0};
    data = [self securityHEXDecode:data];
    NSData *keyData =  [self dataFromHexString:key];
    unsigned long outlen = 1024;
    
    int ret = GM_SM2Decrypt((unsigned char *)result, &outlen, (unsigned char *)data.bytes, data.length, (unsigned char *)keyData.bytes, keyData.length);
    
    if (outlen == 0 || ret != MP_OKAY) {
        //解密出错了
        return [NSData data];
    }
    NSData *resultData = [NSData dataWithBytes:result length:outlen];
    return resultData;
}

- (NSData *)securitySM3:(NSData *)src
{
    if (src == nil || [src length] == 0) {
        return nil;
    }
    unsigned char output[32];
    sm3((unsigned char *)src.bytes, src.length, output);
    
    NSData *tmpData = [NSData dataWithBytes:output length:32];
    return tmpData;
}

- (NSString *) securitySM3HexString:(NSData *)src
{
    if (src == nil || [src length] == 0) {
        return nil;
    }
    unsigned char output[32];
    sm3((unsigned char *)src.bytes, src.length, output);
    
    NSData *tmpData = [NSData dataWithBytes:output length:32];
    NSString *tmpStr = [[self hexStringFromData:tmpData] lowercaseString];
    return tmpStr;
}

- (NSData *)securitySM4CBCEncryptWithSecretKey:(NSData *)secretKey src:(NSData *)src
{
    if (src == nil || [src length] == 0 || [secretKey length] == 0 || secretKey == nil) {
        return nil;
    }
    NSData *plainData = [src LBXPaddingWithMode:LBXPaddingMode_PKCS5 blockSize:16];
    NSData *cryptData = [plainData sm4WithOp:LBXOperaton_Encrypt optionMode:LBXOptionMode_ECB key:secretKey iv:secretKey];
//    cryptData = [CRSecurityManager securityBase64Encode:cryptData];
    return cryptData;
}

- (NSData *)securitySM4CBCDecryptWithSecretKey:(NSData *)secretKey src:(NSData *)src
{
    if (src == nil || [src length] == 0 || [secretKey length] == 0 || secretKey == nil) {
        return nil;
    }
//    src = [CRSecurityManager securityBase64Decode:src];
    NSData *decryptData = [src sm4WithOp:LBXOperaton_Decrypt optionMode:LBXOptionMode_ECB key:secretKey iv:secretKey];
    //解密后除去补位数据
    decryptData = [decryptData LBXUnPaddingWithMode:LBXPaddingMode_PKCS5];
    return decryptData;
}

-(NSData *)security3DESFileEncrypt:(NSData *)src key:(NSString *)base64Key
{
    if (src == nil || [src length] == 0 ||
        base64Key == nil || [base64Key length] == 0) {
        return nil;
    }
    
    if ([GMALGORITHM_OPEN_STATUS isEqualToString:@"1"]) {
        // 国密SM4加密
        NSData *decodeData = [CRSecurityManager securityBase64Decode:[base64Key dataUsingEncoding:NSUTF8StringEncoding]];
        NSData *encryptData = [CRSecurityManager securitySM4CBCEncryptWithSecretKey:decodeData src:src];
        return encryptData;
    }
    
    const void *vplainText;
    size_t plainTextBufferSize;
    
    plainTextBufferSize = [src length];
    vplainText = [src bytes];
    
    CCCryptorStatus ccStatus;
    uint8_t *bufferPtr = NULL;
    size_t bufferPtrSize = 0;
    size_t movedBytes = 0;
    bufferPtrSize = (plainTextBufferSize + kCCBlockSize3DES) & ~(kCCBlockSize3DES - 1);
    
    bufferPtr = malloc(bufferPtrSize * sizeof(uint8_t));
    memset((void *)bufferPtr, 0x00, bufferPtrSize);
    
    
    //首先将base64编码的字符串进行解码生成NSData
    NSData *base64Data = [self securityBase64ToBytes:base64Key];

    NSString *key = [[NSString alloc] initWithData:base64Data encoding:NSUTF8StringEncoding];
    
    if (!key.length) {
        key = base64Key;
    }
    
    NSString *initVec = @"01234567";
    
    const void *vKey = (const void *)[key UTF8String];
    const void *vinitVec = (const void *)[initVec UTF8String];
    
    uint8_t iv[kCCBlockSize3DES];
    memset((void *)iv, 0x00, (size_t)sizeof(iv));
    
    ccStatus = CCCrypt(kCCEncrypt, kCCAlgorithm3DES, kCCOptionPKCS7Padding | kCCOptionECBMode, vKey, kCCKeySize3DES, vinitVec, vplainText, plainTextBufferSize, (void *)bufferPtr, bufferPtrSize, &movedBytes);
    if (ccStatus != kCCSuccess) {
        free(bufferPtr);
        return nil;
    }
    
    NSData *result = [NSData dataWithBytes:bufferPtr length:movedBytes];
    free(bufferPtr);
    
    return result;
}

-(NSData *)security3DESFileDecrypt:(NSData *)src key:(NSString *)base64Key
{
    if (src == nil || [src length] == 0 ||
        base64Key == nil || [base64Key length] == 0) {
        
        return nil;
    }
    
    if ([GMALGORITHM_OPEN_STATUS isEqualToString:@"1"]) {
        // 国密SM4加密
        NSData *decodeData = [CRSecurityManager securityBase64Decode:[base64Key dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSData *decryptData = [CRSecurityManager securitySM4CBCDecryptWithSecretKey:decodeData src:src];
        return decryptData;
    }
    
    const void *vplainText;
    size_t plainTextBufferSize;
    
    plainTextBufferSize = [src length];
    vplainText = [src bytes];
    
    CCCryptorStatus ccStatus;
    uint8_t *bufferPtr = NULL;
    size_t bufferPtrSize = 0;
    size_t movedBytes = 0;
    
    bufferPtrSize = (plainTextBufferSize + kCCBlockSize3DES) & ~(kCCBlockSize3DES - 1);
    bufferPtr = malloc(bufferPtrSize * sizeof(uint8_t));
    memset((void *)bufferPtr, 0x00, bufferPtrSize);
    
    //首先将base64编码的字符串进行解码生成NSData
    NSData *base64Data = [self securityBase64ToBytes:base64Key];
    NSString *key = [[NSString alloc] initWithData:base64Data encoding:NSUTF8StringEncoding];
    
    if (!key.length) {
        key = base64Key;
    }

    NSString *initVec = @"01234567";
    
    const void *vkey = (const void *)[key UTF8String];
    const void *vinitVec = (const void *)[initVec UTF8String];
    
    uint8_t iv[kCCBlockSize3DES];
    memset((void *)iv, 0x00, (size_t)sizeof(iv));
    
    ccStatus = CCCrypt(kCCDecrypt, kCCAlgorithm3DES, kCCOptionPKCS7Padding | kCCOptionECBMode, vkey, kCCKeySize3DES, vinitVec, vplainText, plainTextBufferSize, (void *)bufferPtr, bufferPtrSize, &movedBytes);
    if (ccStatus != kCCSuccess) {
        free(bufferPtr);
        return nil;
    }
    
    NSData *result = [NSData dataWithBytes:bufferPtr length:movedBytes];
    free(bufferPtr);
    
    return result;
}

-(NSData *)security3DESEncrypt:(NSData *)src key1:(NSString *)key1 key2:(NSString *)key2 key3:(NSString *)key3
{
    if (src == nil || [src length] == 0 ||
        key1 == nil || [key1 length] == 0 ||
        key2 == nil || [key2 length] == 0 ||
        key3 == nil || [key3 length] == 0) {
        
        return nil;
    }
    
    const void *vplainText;
    size_t plainTextBufferSize;
    
    plainTextBufferSize = [src length];
    vplainText = [src bytes];
    
    CCCryptorStatus ccStatus;
    uint8_t *bufferPtr = NULL;
    size_t bufferPtrSize = 0;
    size_t movedBytes = 0;
    bufferPtrSize = (plainTextBufferSize + kCCBlockSize3DES) & ~(kCCBlockSize3DES - 1);
    
    bufferPtr = malloc(bufferPtrSize * sizeof(uint8_t));
    memset((void *)bufferPtr, 0x00, bufferPtrSize);
    
    NSString *key = [NSString stringWithFormat:@"%@%@%@",key1,key2,key3];
    NSString *initVec = @"01234567";
    
    const void *vKey = (const void *)[key UTF8String];
    const void *vinitVec = (const void *)[initVec UTF8String];
    
    uint8_t iv[kCCBlockSize3DES];
    memset((void *)iv, 0x00, (size_t)sizeof(iv));
    
    ccStatus = CCCrypt(kCCEncrypt, kCCAlgorithm3DES, kCCOptionPKCS7Padding | kCCOptionECBMode, vKey, kCCKeySize3DES, vinitVec, vplainText, plainTextBufferSize, (void *)bufferPtr, bufferPtrSize, &movedBytes);
    if (ccStatus != kCCSuccess) {
        free(bufferPtr);
        return nil;
    }
    
    NSData *result = [NSData dataWithBytes:bufferPtr length:movedBytes];
    free(bufferPtr);
    
    return result;
}

-(NSData *)security3DESDecrypt:(NSData *)src key1:(NSString *)key1 key2:(NSString *)key2 key3:(NSString *)key3
{
    if (src == nil || [src length] == 0 ||
        key1 == nil || [key1 length] == 0 ||
        key2 == nil || [key2 length] == 0 ||
        key3 == nil || [key3 length] == 0) {
        
        return nil;
    }
    
    const void *vplainText;
    size_t plainTextBufferSize;
    
    plainTextBufferSize = [src length];
    vplainText = [src bytes];
    
    CCCryptorStatus ccStatus;
    uint8_t *bufferPtr = NULL;
    size_t bufferPtrSize = 0;
    size_t movedBytes = 0;
    
    bufferPtrSize = (plainTextBufferSize + kCCBlockSize3DES) & ~(kCCBlockSize3DES - 1);
    bufferPtr = malloc(bufferPtrSize * sizeof(uint8_t));
    memset((void *)bufferPtr, 0x00, bufferPtrSize);
    
    NSString *key = [NSString stringWithFormat:@"%@%@%@",key1,key2,key3];
    NSString *initVec = @"01234567";
    
    const void *vkey = (const void *)[key UTF8String];
    const void *vinitVec = (const void *)[initVec UTF8String];
    
    uint8_t iv[kCCBlockSize3DES];
    memset((void *)iv, 0x00, (size_t)sizeof(iv));
    
    ccStatus = CCCrypt(kCCDecrypt, kCCAlgorithm3DES, kCCOptionPKCS7Padding | kCCOptionECBMode, vkey, kCCKeySize3DES, vinitVec, vplainText, plainTextBufferSize, (void *)bufferPtr, bufferPtrSize, &movedBytes);
    if (ccStatus != kCCSuccess) {
        free(bufferPtr);
        return nil;
    }
    
    NSData *result = [NSData dataWithBytes:bufferPtr length:movedBytes];
    free(bufferPtr);
    
    return result;
}

-(NSData *)securityBase64Encode:(NSData *)src
{
    if(src == nil || src.length == 0) {
        return nil;
    }
    
    unsigned int iTest;
    
    NSUInteger nSize = [src length];
    
    char *srcChar = (char *)[src bytes];
    char* pOutBuffer = malloc(nSize / 3 * 4 + 5);
    memset(pOutBuffer, 0x00, nSize / 3 * 4 + 5);
    
    for(int i = 0;i < nSize / 3; i++)
    {
        iTest = (unsigned char) *srcChar++;
        iTest = iTest << 8;
        
        iTest = iTest | (unsigned char) *srcChar++;
        iTest = iTest << 8;
        
        iTest = iTest | (unsigned char) *srcChar++;
        
        //以4 byte倒序写入输出缓冲
        pOutBuffer[3] = BASE64_ENCODE_TABLE[iTest & 0x3F];
        iTest = iTest >> 6;
        pOutBuffer[2] = BASE64_ENCODE_TABLE[iTest & 0x3F];
        iTest = iTest >> 6;
        pOutBuffer[1] = BASE64_ENCODE_TABLE[iTest & 0x3F];
        iTest = iTest >> 6;
        pOutBuffer[0] = BASE64_ENCODE_TABLE[iTest];
        pOutBuffer += 4;
    }
    
    //设置尾部
    switch (nSize % 3)
    {
        case 0:
            break;
        case 1:
            iTest = (unsigned char) *srcChar;
            iTest = iTest << 4;
            pOutBuffer[1] = BASE64_ENCODE_TABLE[iTest & 0x3F];
            iTest = iTest >> 6;
            pOutBuffer[0] = BASE64_ENCODE_TABLE[iTest];
            pOutBuffer[2] = '='; //用'='也就是64码填充剩余部分
            pOutBuffer[3] = '=';
            break;
        case 2:
            iTest = (unsigned char) *srcChar++;
            iTest = iTest << 8;
            iTest = iTest | (unsigned char) *srcChar;
            iTest = iTest << 2;
            pOutBuffer[2] = BASE64_ENCODE_TABLE[iTest & 0x3F];
            iTest = iTest >> 6;
            pOutBuffer[1] = BASE64_ENCODE_TABLE[iTest & 0x3F];
            iTest = iTest >> 6;
            pOutBuffer[0] = BASE64_ENCODE_TABLE[iTest];
            pOutBuffer[3] = '='; // Fill remaining byte.
            break;
    }
    pOutBuffer -= nSize / 3 * 4;
    NSData *data = [NSData dataWithBytes:pOutBuffer length:(NSUInteger)((nSize + 2) / 3 * 4)];
    free(pOutBuffer);
    
    return data;
}

-(NSData *)securityBase64Decode:(NSData *)src
{
    if(src == nil || [src length] == 0) {
        return nil;
    }
    
    char *srcChar = (char *)[src bytes];
    const int nSrcCount = (int)[src length];//(int)_tcslen(lpszSrc);
    int nSize = nSrcCount / 4 * 3;
    if(srcChar[nSrcCount - 1] == '=')
        nSize--;
    if(srcChar[nSrcCount - 2] == '=')
        nSize--;
    
    char* pOutBuffer = (char *)malloc(nSize + 3);
    memset(pOutBuffer, 0x00, nSize + 3);
    
    const char* pInBuffer = srcChar;
    int iTest,iPack=0;
    for(int i = 0; i < nSize / 3 ; i++)
    {
        for(int j = 0; j < 4; j++)
        {
            iTest = BASE64_DECODE_TABLE[*pInBuffer]; // Read from InputBuffer.
            pInBuffer++;
            //InPtr++;
            if (iTest == 0xFF)
            {
                j--;
                continue; //读到255非法字符
            }
            iPack = iPack << 6 ;
            iPack = iPack | iTest ;
        }
        pOutBuffer[2] = iPack;
        iPack = iPack >> 8;
        pOutBuffer[1] = iPack;
        iPack = iPack >> 8;
        pOutBuffer[0] = iPack;
        //准备写入后3位
        pOutBuffer+= 3;
        iPack = 0;
    }
    switch(nSize % 3)
    {
        case 1:
            iTest = BASE64_DECODE_TABLE[*pInBuffer]; // Read from InputBuffer.
            pInBuffer++;
            if (iTest != 0xFF)
            {
                iPack = iPack << 6 ;
                iPack = iPack | iTest ;
            }
            iTest = BASE64_DECODE_TABLE[*pInBuffer]; // Read from InputBuffer.
            if (iTest != 0xFF)
            {
                iPack = iPack << 6 ;
                iPack = iPack | iTest ;
            }
            iPack = iPack >> 4;
            pOutBuffer[0] = iPack;
            pOutBuffer++;
            break;
        case 2:
            iTest = BASE64_DECODE_TABLE[*pInBuffer]; // Read from InputBuffer.
            pInBuffer++;
            if (iTest != 0xFF)
            {
                iPack = iPack << 6 ;
                iPack = iPack | iTest ;
            }
            iTest = BASE64_DECODE_TABLE[*pInBuffer]; // Read from InputBuffer.
            pInBuffer++;
            if (iTest != 0xFF)
            {
                iPack = iPack << 6 ;
                iPack = iPack | iTest ;
            }
            iTest = BASE64_DECODE_TABLE[*pInBuffer]; // Read from InputBuffer.
            if (iTest != 0xFF)
            {
                iPack = iPack << 6 ;
                iPack = iPack | iTest ;
            }
            iPack = iPack >> 2;
            pOutBuffer[1] = iPack;
            iPack = iPack >> 8;
            pOutBuffer[0] = iPack;
            pOutBuffer+=2;
            break;
        default:
            break;
    }
    pOutBuffer -= nSize;
    pOutBuffer[nSize] = '\0';
    
    NSData *data = [NSData dataWithBytes:pOutBuffer length:(NSUInteger)nSize];
    free(pOutBuffer);
    
    return data;
}

-(NSData *)securityBCDEncode:(NSData *)src
{
    return nil;
}

-(NSData *)securityBCDDecode:(NSData *)src
{
    return nil;
}

-(NSData *)securityHashByString:(BussinessHashType)type src:(NSString *)src
{
    if ([src isKindOfClass:[NSString class]]) {
        if (src.length) {
            return [self securityHashByBytes:type src:[src dataUsingEncoding:NSUTF8StringEncoding]];
        }
    }
    return nil;
}

#define BUFFER_SIZE 2048
-(NSData *)securityHashByStream:(BussinessHashType)type src:(NSInputStream *)is
{
    if (type == HashTypeMD5)
    {
        //MD5算法
        if (is == nil) {
            return nil;
        }
        
        CC_MD5_CTX md5;
        CC_MD5_Init(&md5);
        
        uint8_t buffer[BUFFER_SIZE];
        [is open];
        BOOL done = NO;
        while (!done) {
            NSInteger bytesRead = [is read:buffer maxLength:BUFFER_SIZE];
            if (bytesRead == -1) {//读取文件流出错
                return nil;
            } else if (bytesRead == 0) {
                done = YES;
            } else {
                CC_MD5_Update(&md5, buffer, (uint32_t)bytesRead);
            }
        }
        [is close];
        unsigned char digest[CC_MD5_DIGEST_LENGTH];
        CC_MD5_Final(digest, &md5);
        
        return [NSData dataWithBytes:digest length:CC_MD5_DIGEST_LENGTH];
    }
    else if (type == HashTypeSHA1)
    {
        //SHA1算法
        if (is == nil) {
            return nil;
        }
        
        CC_SHA1_CTX ctx;
        BOOL done = NO;
        uint8_t buffer[BUFFER_SIZE];
        uint8_t *hashBytes = NULL;
        
        hashBytes = malloc(CC_SHA1_DIGEST_LENGTH * sizeof(uint8_t));
        memset((void *)hashBytes, 0x00, CC_SHA1_DIGEST_LENGTH);
        
        [is open];
        CC_SHA1_Init(&ctx);
        while (!done) {
            NSInteger bytesRead = [is read:buffer maxLength:BUFFER_SIZE];
            if (bytesRead == -1) {//读取文件流出错
                free(hashBytes);
                return nil;
            } else if (bytesRead == 0) {
                done = YES;
            } else {
                CC_SHA1_Update(&ctx, buffer, (unsigned int)bytesRead);
            }
        }
        [is close];
        CC_SHA1_Final(hashBytes, &ctx);
        
        NSData *hash = [NSData dataWithBytes:(const void *)hashBytes length:CC_SHA1_DIGEST_LENGTH];
        free(hashBytes);
        
        return hash;
    }
    
    return nil;
}

-(NSData *)securityHashByBytes:(BussinessHashType)type src:(NSData *)src
{
    if (src.length) {
        if ([GMALGORITHM_OPEN_STATUS isEqualToString:@"1"]) {
            return [[CRSecurityManager securitySM3HexString:src] dataUsingEncoding:NSUTF8StringEncoding];
        } else {
            return [self securityHashByStream:type src:[NSInputStream inputStreamWithData:src]];
        }
    }
    return nil;
}

#pragma mark - 使用公钥字符串加密



#define PADDING kSecPaddingNone
-(NSData *)securityRSAEncrypt:(NSData *)src key:(SecKeyRef)key
{
    if (src == nil || [src length] == 0 || key == NULL) {
        return nil;
    }
    
    NSMutableData *encodedData = [[NSMutableData alloc] init];
    
    OSStatus status;
    size_t cipherBufferSize = 0;
    size_t dataBufferSize = 0;
    
    uint8_t *cipherBuffer = NULL;
    
    cipherBufferSize = SecKeyGetBlockSize(key);
    dataBufferSize = [src length];
    
    cipherBuffer = malloc(cipherBufferSize * sizeof(uint8_t));
    memset((void *)cipherBuffer, 0x00, cipherBufferSize);
    
    //超过长度，分段加密算法开始
    unsigned long blockSize = cipherBufferSize - 12;
    int numBlock = (int)ceil(dataBufferSize / (double)blockSize);
    for (int i = 0; i < numBlock; i++) {
        unsigned long bufferSize = MIN(blockSize, [src length] - i * blockSize);
        NSData *buffer = [src subdataWithRange:NSMakeRange(i * blockSize, bufferSize)];
        status = SecKeyEncrypt(key, PADDING, (const uint8_t *)[buffer bytes], [buffer length], cipherBuffer, &cipherBufferSize);
        if (status == noErr) {
            NSData *encryptedBytes = [[NSData alloc] initWithBytes:(const void *)cipherBuffer length:cipherBufferSize];
            [encodedData appendData:encryptedBytes];
        } else {
            if (cipherBuffer != NULL) {
                free(cipherBuffer);
            }
            return nil;
        }
    }
    //超过长度，分段加密算法结束
    
    if (cipherBuffer != NULL) {
        free(cipherBuffer);
    }
    
    return encodedData;
}

-(NSData *)securityRSADecrypt:(NSData *)src key:(SecKeyRef)key
{
    if (src == nil || [src length] == 0 || key == NULL) {
        return nil;
    }
    
    NSMutableData *decodedData = [[NSMutableData alloc] init];
    size_t plainBufferSize = SecKeyGetBlockSize(key);
    uint8_t *plainBuffer = malloc(plainBufferSize * sizeof(uint8_t));
    double totalLength = [src length];
    size_t blockSize = plainBufferSize;
    size_t numBlock = (size_t)ceil(totalLength / blockSize);
    
    for (int i = 0; i < numBlock; i++) {
        int bufferSize = MIN(blockSize, totalLength - i * blockSize);
        NSData *buffer = [src subdataWithRange:NSMakeRange(i * blockSize, bufferSize)];
        OSStatus status = SecKeyDecrypt(key, PADDING, (const uint8_t *)[buffer bytes], bufferSize, plainBuffer, &plainBufferSize);
        if (status == noErr) {
            NSData *decryptBytes = [[NSData alloc] initWithBytes:(const void *)plainBuffer length:plainBufferSize];
            [decodedData appendData:decryptBytes];
        } else {
            if (plainBuffer != NULL) {
                free(plainBuffer);
            }
            return nil;
        }
    }
    
    if (plainBuffer != NULL) {
        free(plainBuffer);
    }
    
    return decodedData;
}

-(NSData *)securityHEXEncode:(NSData *)src
{
    if(src == nil || [src length] == 0) {
        return nil;
    }
    
    Byte *bytes = (Byte *)[src bytes];
    NSString *result = @"";
    for (int i = 0; i < [src length]; i++) {
        if (i == 0) {
            result = [NSString stringWithFormat:@"%02X", bytes[i]];
        } else {
            result = [result stringByAppendingFormat:@"%02X", bytes[i]];
        }
    }
    
    return [result dataUsingEncoding:NSUTF8StringEncoding];
}

-(NSData *)securityHEXDecode:(NSData *)src
{
    if(src == nil || [src length] == 0) {
        return nil;
    }
    
    NSString *hexString = [[[NSString alloc] initWithData:src encoding:NSUTF8StringEncoding] lowercaseString];
    
    NSMutableData *data = [NSMutableData data];
    unsigned char wholeByte;
    char byteChars[3] = {'\0', '\0', '\0'};
    
    for (int i = 0; i < hexString.length / 2; i++) {
        byteChars[0] = [hexString characterAtIndex:i * 2];
        byteChars[1] = [hexString characterAtIndex:i * 2 + 1];
        wholeByte = strtol(byteChars, NULL, 16);
        [data appendBytes:&wholeByte length:1];
    }
    
    return data;
}

-(SecKeyRef)securityListPublicKey
{
    //得到列表证书路径
    NSString *filepath = [[NSBundle mainBundle] pathForResource:@"issue" ofType:@"crt"];
    
    if (!filepath.length)
    {
        return nil;
    }
    
    SecCertificateRef certificate = nil;
    NSData *certificateData = [[NSData alloc] initWithContentsOfFile:filepath];
    certificate = SecCertificateCreateWithData(kCFAllocatorDefault, (CFDataRef)certificateData);
    SecPolicyRef policy = SecPolicyCreateBasicX509();
    SecTrustRef trust;
    OSStatus status = SecTrustCreateWithCertificates(certificate, policy, &trust);
    SecTrustResultType trustResult;
    if (status == noErr)
    {
        SecTrustEvaluate(trust, &trustResult);

        /*
        status = SecTrustEvaluate(trust, &trustResult);

        //Get time used to verify trust
        CFAbsoluteTime trustTime,currentTime,timeIncrement,newTime;
        CFDateRef newDate;
        
        if (trustResult == kSecTrustResultRecoverableTrustFailure) {
            trustTime = SecTrustGetVerifyTime(trust);
            timeIncrement = 31536000;
            currentTime = CFAbsoluteTimeGetCurrent();
            newTime = currentTime - timeIncrement;
            if (trustTime - newTime){
                newDate = CFDateCreate(NULL, newTime);
                SecTrustSetVerifyDate(trust, newDate);
                status = SecTrustEvaluate(trust, &trustResult);
            }
        }
        
        if (trustResult != kSecTrustResultProceed) {
         
        }
        */
    }

    SecKeyRef publicKey = SecTrustCopyPublicKey(trust);
    CFRelease(certificate);
    CFRelease(policy);
    CFRelease(trust);
    
    return publicKey;
}

- (NSString *)securityGetSM2PublicKey
{
//    NSData *decodeData = [CRSecurityManager securityBase64Decode:[[CRHandShakeManager commonEncryptKey] dataUsingEncoding:NSUTF8StringEncoding]];
////    NSData *encryptPemData = [NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"issue" ofType:@"pem"]];
//    NSData *encryptPemData = [NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"issue公司环境" ofType:@"pem"]];
////    NSData *encryptPemData = [NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"issue公司环境db2" ofType:@"pem"]];
////    NSData *encryptPemData = [NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"issue昆仑银行" ofType:@"pem"]];
////    NSData *encryptPemData = [NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"issue_蔡楚环境" ofType:@"pem"]];
////    NSData *encryptPemData = [NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"issue_cherryother_公司环境" ofType:@"pem"]];
//
//    encryptPemData = [CRSecurityManager securityBase64Decode:encryptPemData];
//    NSData *decrptData = [CRSecurityManager securitySM4CBCDecryptWithSecretKey:decodeData src:encryptPemData];
//    NSString *decryptString = [[NSString alloc] initWithData:decrptData encoding:NSUTF8StringEncoding];
//
//    if (decryptString.length > 128) {
//        decryptString = [decryptString substringFromIndex:2];
//    }
    //decryptString 公钥
//    NSString *decryptString = @"049A038890B6F47E9E31D1118A9E7F1331FD7421352B06076F604726A6395525227CE82071258D11F0E51992E37FFB76FDC162BD2857645807EDBA9F86D9CBCDEA";
    NSString *path = [[NSBundle mainBundle] pathForResource:@"a" ofType:@"puk"];
    if (kIsPro) {
       path = [[NSBundle mainBundle] pathForResource:@"prod" ofType:@"puk"];
    }
    NSData *data = [NSData dataWithContentsOfFile:path];
    NSString *decryptString = [CRSecurityManager securityBytesToHEX:data];
    if (decryptString.length > 128) {
        decryptString = [decryptString substringFromIndex:2];
    }
    return decryptString;
}

- (NSString *)securityGetSM2PrivateKey
{
    NSData *decodeData = [CRSecurityManager securityBase64Decode:[@"XynOfVt9W6ChbaFt2ulEIQ==" dataUsingEncoding:NSUTF8StringEncoding]];
    NSData *encryptPemData = [NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"pri" ofType:@"pem"]];
    NSData *decrptData = [CRSecurityManager securitySM4CBCDecryptWithSecretKey:decodeData src:encryptPemData];
    NSString *decryptString = [[NSString alloc] initWithData:decrptData encoding:NSUTF8StringEncoding];
    
//    if (decryptString.length > 64) {
//        decryptString = [decryptString substringFromIndex:2];
//    }
    
    return decryptString;
}

-(NSString *)securityBytesToURLBase64:(NSData *)src
{
    if (src == nil || src.length == 0) {
        return @"";
    }
    
    NSString *base64 = [[NSString alloc] initWithData:[self securityBase64Encode:src] encoding:NSUTF8StringEncoding];
    NSString *plus = [base64 stringByReplacingOccurrencesOfString:@"+" withString:@"-"];
    NSString *splash = [plus stringByReplacingOccurrencesOfString:@"/" withString:@"_"];
    NSString *append = [splash stringByReplacingOccurrencesOfString:@"=" withString:@"."];
    
    return [NSString stringWithFormat:@"%@", append];
}

-(NSData *)securityURLBase64ToBytes:(NSString *)src
{
    if (src == nil || src.length == 0) {
        return nil;
    }
    
    NSString *plus = [src stringByReplacingOccurrencesOfString:@"-" withString:@"+"];
    NSString *splash = [plus stringByReplacingOccurrencesOfString:@"_" withString:@"/"];
    NSString *append = [splash stringByReplacingOccurrencesOfString:@"." withString:@"="];
    
    return [NSData dataWithData:[self securityBase64Decode:[append dataUsingEncoding:NSUTF8StringEncoding]]];
}

-(NSString *)securityBytesToBase64:(NSData *)src
{
    if (src == nil || [src length] == 0) {
        return @"";
    }
    
    NSData *data = [self securityBase64Encode:src];
    return [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
}

-(NSData *)securityBase64ToBytes:(NSString *)src
{
    if (src == nil  || [src length] == 0) {
        return nil;
    }
    
    return [self securityBase64Decode:[src dataUsingEncoding:NSUTF8StringEncoding]];
}

-(NSString *)securityBytesToBCD:(NSData *)src
{
    if (src == nil || [src length] == 0) {
        return @"";
    }
    
    NSData *data = [self securityBCDEncode:src];
    return [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
}

-(NSData *)securityBCDToBytes:(NSString *)src
{
    if (src == nil || [src length] == 0) {
        return nil;
    }
    
    return [self securityBCDDecode:[src dataUsingEncoding:NSUTF8StringEncoding]];
}

-(NSString *)securityBytesToHEX:(NSData *)src
{
    if (src == nil || [src length] == 0) {
        return @"";
    }
    
    NSData *data = [self securityHEXEncode:src];
    return [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
}

-(NSData *)securityHEXToBytes:(NSString *)src
{
    if (src == nil || [src length] == 0) {
        return nil;
    }
    
    return [self securityHEXDecode:[src dataUsingEncoding:NSUTF8StringEncoding]];
}

- (NSData *)dataFromHexString:(NSString*) hexString {
    if (hexString == nil || [hexString length] == 0) {
        return nil;
    }
    const char *chars = [hexString UTF8String];
    NSInteger i = 0, len = hexString.length;
    
    NSMutableData *data = [NSMutableData dataWithCapacity:len / 2];
    char byteChars[3] = {'\0','\0','\0'};
    unsigned long wholeByte;
    
    while (i < len) {
        byteChars[0] = chars[i++];
        byteChars[1] = chars[i++];
        wholeByte = strtoul(byteChars, NULL, 16);
        [data appendBytes:&wholeByte length:1];
    }
    
    return data;
}

static inline char itoh(int i) {
    if (i > 9) return 'A' + (i - 10);
    return '0' + i;
}

- (NSString *) hexStringFromData:(NSData*) data
{
    if (data == nil || [data length] == 0) {
        return nil;
    }
    NSUInteger i, len;
    unsigned char *buf, *bytes;
    
    len = data.length;
    bytes = (unsigned char*)data.bytes;
    buf = malloc(len*2);
    
    for (i=0; i<len; i++) {
        buf[i*2] = itoh((bytes[i] >> 4) & 0xF);
        buf[i*2+1] = itoh(bytes[i] & 0xF);
    }
    
    return [[NSString alloc] initWithBytesNoCopy:buf
                                          length:len*2
                                        encoding:NSASCIIStringEncoding
                                    freeWhenDone:YES];
}


@end
