//
//  NSData+LBXSM4.h
//  DataHandler
//
//  Created by lbxia on 2017/5/11.
//  Copyright © 2017年 LBX. All rights reserved.
//

#import <Foundation/Foundation.h>

//encrypt operation
typedef NS_ENUM(uint8_t,LBXOperaton)
{
    LBXOperaton_Encrypt = 0,
    LBXOperaton_Decrypt = 1
};

//encrypt option mode
typedef NS_ENUM(NSInteger, LBXOptionMode)
{
    LBXOptionMode_ECB,
    LBXOptionMode_CBC,
    LBXOptionMode_PCBC,
    LBXOptionMode_CFB,
    LBXOptionMode_OFB,
    LBXOptionMode_CTR
};

@interface NSData (LBXSM4)


- (NSData*)sm4WithOp:(LBXOperaton)op
          optionMode:(LBXOptionMode)om
                 key:(NSData*)keyData
                  iv:(NSData*)ivData;

@end
