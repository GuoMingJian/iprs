//
//  RSA.h
//  RSA
//
//  Created by YGJ on 13-7-22.
//  Copyright (c) 2013年 YGJ. All rights reserved.
//

#import <Foundation/Foundation.h>
//RSA加密库
@interface RSAEnc : NSObject

-(void)test;
-(NSString *)parseKey:(NSString*)src rsan:(NSString *)rsan rsae:(NSString *)rsae rsalabe:(NSString *)rsalabe reskeyindex:(NSString *)key;

@end
