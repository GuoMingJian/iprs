//
//  CherryCodeSecurityManager.h
//  MobileOfficing
//
//  Created by ChenZheng on 15/11/24.
//  Copyright © 2015年 ICITIC. All rights reserved.
//

#import <Foundation/Foundation.h>
//HASH算法类型
typedef NS_ENUM (NSUInteger, BussinessHashType){
    HashTypeMD5,
    HashTypeSHA1,
};
@interface CherryCodeSecurityManager : NSObject

+(instancetype)sharedInstance;

/**
 该接口用于SM2加密

 @param src 需要加密的明文
 @param key 公钥
 @return 加密后的密文
 */
- (NSString *)securitySM2Encrypt:(NSString *)src key:(NSString *)key;

/**
 该接口用于SM2解密

 @param src 需要解密的密文
 @param key 私钥
 @return 解密后的明文
 */
- (NSData *)securitySM2Decrypt:(NSData *)src key:(NSString *)key;

/**
 该接口用于SM3的哈希值

 @param src
 @return
 */
- (NSData *)securitySM3:(NSData *)src;

/**
 该接口用于SM3加密

 @param src 加密的对象流
 @return 加密后的哈希值
 */
- (NSString *)securitySM3HexString:(NSData *)src;

/**
 该接口用于SM4加密

 @param secretKey 密钥
 @param iv iv密钥
 @param src 需要加密的流
 @return 加密后的流
 */
- (NSData *)securitySM4CBCEncryptWithSecretKey:(NSData *)secretKey src:(NSData *)src;

/**
 该接口用于SM4解密

 @param secretKey 密钥
 @param iv iv密钥
 @param src 加密的流
 @return 解密后的流
 */
- (NSData *)securitySM4CBCDecryptWithSecretKey:(NSData *)secretKey src:(NSData *)src;

/**
 *  该接口用于为文件进行3DES加密
 *
 *  @param src 文件加密前的明文
 *  @param key 加密、解密的密钥
 *
 *  @return 加密后的字节数据
 */
-(NSData *)security3DESFileEncrypt:(NSData *)src key:(NSString *)key;

/**
 *  该接口用于为文件进行3DES解密
 *
 *  @param src 文件解密前的密文
 *  @param key 加密、解密的密钥
 *
 *  @return 解密后的字节数据
 */
-(NSData *)security3DESFileDecrypt:(NSData *)src key:(NSString *)key;

/**
 *  该接口用于普通数据加密
 *
 *  @param src  文件加密前的明文
 *  @param key1 加密、解密的密钥1
 *  @param key2 加密、解密的密钥2
 *  @param key3 加密、解密的密钥3
 *
 *  @return 加密后的字节数据
 */
-(NSData *)security3DESEncrypt:(NSData *)src key1:(NSString *)key1 key2:(NSString *)key2 key3:(NSString *)key3;

/**
 *  该接口用于普通数据解密
 *
 *  @param src  文件解密前的密文
 *  @param key1 加密、解密的密钥1
 *  @param key2 加密、解密的密钥2
 *  @param key3 加密、解密的密钥3
 *
 *  @return 解密后的字节数据
 */
-(NSData *)security3DESDecrypt:(NSData *)src key1:(NSString *)key1 key2:(NSString *)key2 key3:(NSString *)key3;

/**
 *  该接口用于base64编码
 *
 *  @param src 编码前的字节数据
 *
 *  @return base64编码后的字节数据
 */
-(NSData *)securityBase64Encode:(NSData *)src;

/**
 *  该接口用于base64解码
 *
 *  @param src Base64编码的字节数据
 *
 *  @return base64解码后的字节数据
 */
-(NSData *)securityBase64Decode:(NSData *)src;

/**
 *  该接口用于BCD编码
 *
 *  @param sr BCD编码前的字节数据
 *
 *  @return BCD编码后的字节数据
 */
-(NSData *)securityBCDEncode:(NSData *)src;

/**
 *  该接口用于BCD解码
 *
 *  @param src BCD编码的字节数据
 *
 *  @return BCD解码后的字节数据
 */
-(NSData *)securityBCDDecode:(NSData *)src;

/**
 *  该接口用于计算字符串的hash值
 *
 *  @param type Hash算法类型
 *  @param src  待计算的字符串
 *
 *  @return 计算好的hash字节数据
 */
-(NSData *)securityHashByString:(BussinessHashType)type src:(NSString *)src;

/**
 *  该接口用于计算输入流的hash值
 *
 *  @param type   Hash算法类型
 *  @param stream 待计算的输入流
 *
 *  @return 计算好的hash字节数据
 */
-(NSData *)securityHashByStream:(BussinessHashType)type src:(NSInputStream *)stream;

/**
 *  该接口用于计算字节数据的hash值
 *
 *  @param type Hash算法类型
 *  @param src  待计算的字节数据
 *
 *  @return 计算好的hash字节数据
 */
-(NSData *)securityHashByBytes:( BussinessHashType)type src:(NSData *)src;


/**
 *  该接口用于RSA公钥加密
 *
 *  @param src 数据加密前的明文
 *  @param key 加密、解密的证书公钥
 *
 *  @return 加密后的字节数据
 */
-(NSData *)securityRSAEncrypt:(NSData *)src key:(SecKeyRef)key;

/**
 *  该接口用于RSA公钥解密
 *
 *  @param src 数据解密前的密文
 *  @param key 加密、解密的证书公钥
 *
 *  @return 解密后的字节数据
 */
-(NSData *)securityRSADecrypt:(NSData *)src key:(SecKeyRef)key;

/**
 *  该接口用于将普通字符转换成16进制
 *
 *  @param src 普通字符的字节数据
 *
 *  @return 16进制的字符
 */
-(NSData *)securityHEXEncode:(NSData *)src;

/**
 *  该接口用于将16进制字符转换成普通字符
 *
 *  @param src 16进制字符字节数据
 *
 *  @return 普通字符字节数据
 */
-(NSData *)securityHEXDecode:(NSData *)src;


/**
 *  该接口用于获取列表文件证书中的公钥
 *
 *  @return SecKeyRef，证书的公钥
 */
-(SecKeyRef)securityListPublicKey;

/**
 获取SM2的公钥

 @return 公钥
 */
- (NSString *)securityGetSM2PublicKey;

/**
 *  该接口用于将字节数据转换成URL base64编码的字符串
 *
 *  @param src 普通的字节数据
 *
 *  @return URL Base64编码字符串
 */
-(NSString *)securityBytesToURLBase64:(NSData *)src;

/**
 *  该接口用于将URL base64编码的字符串转换成字节数据
 *
 *  @param src URL Base64编码的字符串
 *
 *  @return URL Base64解码的字节数据
 */
-(NSData *)securityURLBase64ToBytes:(NSString *)src;

/**
 *  该接口用于将字节数据转换成base64编码的字符串
 *
 *  @param src 普通的字节数据
 *
 *  @return Base64编码字符串
 */
-(NSString *)securityBytesToBase64:(NSData *)src;

/**
 *  该接口用于将base64编码的字符串转换成字节数据
 *
 *  @param src Base64编码的字符串
 *
 *  @return Base64解码的字节数据
 */
-(NSData *)securityBase64ToBytes:(NSString *)src;

/**
 *  该接口用于将字节数据转换成BCD编码的字符串
 *
 *  @param src 普通的字节数据
 *
 *  @return BCD编码字符串
 */
-(NSString *)securityBytesToBCD:(NSData *)src;

/**
 *  该接口用于将BCD编码的字符串转换成字节数据
 *
 *  @param src BCD编码的字符串
 *
 *  @return BCD解码的字节数据
 */
-(NSData *)securityBCDToBytes:(NSString *)src;

/**
 *  该接口用于将字节数据转换成HEX编码的字符串
 *
 *  @param src 普通的字节数据
 *
 *  @return HEX编码字符串
 */
-(NSString *)securityBytesToHEX:(NSData *)src;

/**
 *  该接口用于将HEX编码的字符串转换成字节数据
 *
 *  @param src HEX编码的字符串
 *
 *  @return HEX解码的字节数据
 */
-(NSData *)securityHEXToBytes:(NSString *)src;

@end
