
#import "RSAEnc.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <openssl/crypto.h>
#include <openssl/err.h>
#include <openssl/rand.h>
#include <openssl/bn.h>
#include <openssl/rsa.h>


// for SHA1
#include <openssl/evp.h>
#include <openssl/sha.h>

@implementation RSAEnc

// for base64
//  base64encode()中用到
//#include <openssl/pem.h>

//const unsigned char oaep_int_seed[]="\xaa\xfd\x12\xf6\x59\xca\xe6\x34\x89\xb4\x79\xe5\x07\x6d\xde\xc2"
//"\xf0\x6c\xb5\x8f";
unsigned char oaep_int_seed[21];

- (id)init
{
    self =[super init];
    if (self) {
        int i ;
        for (i = 0; i< 20; i++) {
            oaep_int_seed[i] = rand()%0x100;
        }
        oaep_int_seed[20]=0;
    }
    return self;
}
int is_digit(char ch)
{
    if(ch >='0' && ch <='9')
        return 1;
    return 0;
}
char val2hex(int val)
{
    if(val<0xa)
        return val + '0';
    return val - 0xa + 'A';
}

static void dump(int len, const unsigned char* str)
{
    int i;
    for(i = 0; i< len; i++)
    {
        if( (i&0xF) != 0xF)
            printf("%c%c ", val2hex((str[i]>>4)&0xF), val2hex(str[i]&0xF));
        else
            printf("%c%c\n", val2hex((str[i]>>4)&0xF), val2hex(str[i]&0xF));
    }
    
    printf("\n");
}


static void dbg_mem_info(const unsigned char* mem, int len, const char* fmt, ...)
{
    va_list args;
    
    va_start(args,fmt);
    vprintf(fmt, args);
    va_end(args);
    printf("\n");
    dump(len, mem);
    printf("\n");
}

//void sha1(const unsigned char * msg, unsigned int msg_len, unsigned char * digest)
//{
//    SHA_CTX ctx ;
//    
//    SHA_Init( &ctx );
//    SHA_Update( &ctx , (const void *)msg, msg_len );
//    SHA_Final(digest, &ctx);
//    
//}
int sha1(const unsigned char * msg, unsigned int msg_len, unsigned char *digest)
{
    EVP_MD_CTX c;
    EVP_MD_CTX_init(&c);
    EVP_Digest(msg,msg_len,digest,NULL,EVP_sha1(), NULL);
    EVP_MD_CTX_cleanup(&c);
    
    return 0;
}
static int MGF1(unsigned char *mask, long len, const unsigned char *seed,
                long seedlen)
{
    return PKCS1_MGF1(mask, len, seed, seedlen, EVP_sha1());
}

static void MaskData(unsigned char *data, const unsigned char * mask, long len)
{
    int i;
    for(i = 0; i< len; i++)
    {
        data[i] ^= mask[i];
    }
}

static char encoding_table[] = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
    'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
    'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
    'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
    'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
    'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
    'w', 'x', 'y', 'z', '0', '1', '2', '3',
    '4', '5', '6', '7', '8', '9', '+', '/'};

static int mod_table[] = {0, 2, 1};


int base64encode(unsigned char* dest,
                 const unsigned char *data,
                 int input_length)
{
    int i,j;
    int ret;
    
    unsigned int octet_a;
    unsigned int octet_b;
    unsigned int octet_c;
    
    unsigned int triple;
    
    ret = 4 * ((input_length + 2) / 3);
    
    if (dest == NULL) return -1;
    
    for (i = 0, j = 0; i < input_length;) {
        
        octet_a = i < input_length ? data[i++] : 0;
        octet_b = i < input_length ? data[i++] : 0;
        octet_c = i < input_length ? data[i++] : 0;
        
        triple = (octet_a << 0x10) + (octet_b << 0x08) + octet_c;
        
        dest[j++] = encoding_table[(triple >> 3 * 6) & 0x3F];
        dest[j++] = encoding_table[(triple >> 2 * 6) & 0x3F];
        dest[j++] = encoding_table[(triple >> 1 * 6) & 0x3F];
        dest[j++] = encoding_table[(triple >> 0 * 6) & 0x3F];
    }
    
    for (i = 0; i < mod_table[input_length % 3]; i++)
        dest[ret - 1 - i] = '=';
    dest[ret] = 0;
    
    return ret;
}


static const char  base64_symbl_table[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
int isbase64_symbl(char c)
{
    return c && strchr(base64_symbl_table, c) != NULL;
}

char symbl_value(char c)
{
    const char *p = strchr(base64_symbl_table, c);
    if(p) {
        return p-base64_symbl_table;
    } else {
        return -1;
    }
}

int base64decode(unsigned char *dest, const unsigned char *src, int srclen)
{
    *dest = 0;
    if(*src == 0)
    {
        return 0;
    }
    unsigned char *p = dest;
    do
    {
        
        char a = symbl_value(src[0]);
        char b = symbl_value(src[1]);
        char c = symbl_value(src[2]);
        char d = symbl_value(src[3]);
        *p++ = (a << 2) | (b >> 4);
        *p++ = (b << 4) | (c >> 2);
        *p++ = (c << 6) | d;
        if(symbl_value(src[1]) < 0)
        {
            p -= 2;
            break;
        }
        else if(symbl_value(src[2]) < 0)
        {
            p -= 2;
            break;
        }
        else if(symbl_value(src[3]) < 0)
        {
            p--;
            break;
        }
        src += 4;
        while(*src && (*src == 13 || *src == 10)) src++;
    }
    while(srclen -= 4);
    *p = 0;
    return p-dest;
}


-(int)loadKey:(RSA *)p_key path:(const char*)path

{
    const char *key_file_name= path;
    FILE* pf;
    char digit[600];
    int digit_cnt;
    int state = 0;
    char bn;
    char ch;
    
    int n_loaded=0;
    int e_loaded=0;
    int d_loaded=0;
    pf = fopen(key_file_name, "rb");
    if(pf < 0)
    {
        printf("Can't load the key file %s\n",key_file_name);
        return -1;
    }
    
    while(!feof(pf))
    {
        if(1 != fread(&ch, sizeof(ch), 1, pf))
            break;
        switch(state)
        {
            case 0:
                switch(ch)
            {
                case 'n':
                case 'e':
                case 'd':
                    bn = ch;
                    state = 1;
                    break;
                default:
                    break;
            }
                break;
            case 1:
                if(':' == ch)
                {
                    state = 2;
                    digit_cnt = 0;
                }
                else
                    state = 0;
                break;
            case 2:
                if(is_digit(ch))
                {
                    digit[digit_cnt++] = ch;
                }
                else if(digit_cnt > 0)
                {
                    digit[digit_cnt] = 0;
                    switch(bn)
                    {
                        case 'n':
                            BN_dec2bn(&(p_key->n), digit);
                            n_loaded = 1;
                            //printf("N:%s\n",digit);
                            break;
                        case 'd':
                            BN_dec2bn(&(p_key->d), digit);
                            //printf("D:%s\n",digit);
                            d_loaded = 1;
                            break;
                        case 'e':
                            BN_dec2bn(&(p_key->e), digit);
                            //printf("E:%s\n",digit);
                            e_loaded = 1;
                            break;
                    }
                    state = 0;
                }
        }
    }
    
    if(n_loaded && d_loaded && e_loaded)
    {
        return 0;
    }
    else
        return -1;
}


int rsaes_oaep_enc(const unsigned char * msg, unsigned int msg_len, const unsigned char * label, unsigned int label_len, RSA * key, unsigned char* output)
{
    int i;
    int ret;
    
    unsigned char EM[600];
    unsigned char *db = &EM[1 + SHA_DIGEST_LENGTH];
    unsigned char *seed = &EM[1];
    int seed_len;
    unsigned char dbMask[600];
    unsigned char seedMask[600];
    int rsa_n_len = BN_num_bytes(key->n);//返回n的位数 = 128
    
    printf("rsa_n_len is :%d\n",rsa_n_len);
    
    
    dbg_mem_info(msg, msg_len, "the message is:");
    
    
    //Step.1
    if(msg_len > rsa_n_len - 2 * SHA_DIGEST_LENGTH - 1)
    {
        printf("Message is too long\n");
        return -1;  //  要加密字节的长度大于87(即128－2*20－1)时退出加密
    }
    
    //Step.2.a, Step.2.b, Step.2.c
    printf("the label is:\n");
    dump(label_len,label);
    
    sha1(label,label_len, db);
    
    printf("lHash is:\n");
    dump(SHA_DIGEST_LENGTH, db);
    printf("\n");
    
    
    memset(&db[SHA_DIGEST_LENGTH], 0, rsa_n_len - SHA_DIGEST_LENGTH - 1 - msg_len);
    db[rsa_n_len - SHA_DIGEST_LENGTH -1 - msg_len - 1] = 0x01;
    memcpy(&db[rsa_n_len - SHA_DIGEST_LENGTH - 1 - msg_len ], msg, msg_len);
    
    
    printf("DB is:\n");
    dump(rsa_n_len  - SHA_DIGEST_LENGTH - 1 , db);
    printf("\n");
    
    memcpy(seed,oaep_int_seed, sizeof(oaep_int_seed)-1);
    seed_len = sizeof(oaep_int_seed) -1;
    
    
    printf("seed is:\n");
    dump(seed_len, seed);
    printf("\n");
    
    MGF1(dbMask,rsa_n_len - SHA_DIGEST_LENGTH - 1, seed, seed_len);
    
    
    printf("DBMask is:\n");
    dump(rsa_n_len - SHA_DIGEST_LENGTH - 1, dbMask);
    printf("\n");
    
    for(i=0; i<rsa_n_len - SHA_DIGEST_LENGTH - 1 ; i++)
        db[i] ^= dbMask[i];
    
    printf("MaskedDB is:\n");
    dump(rsa_n_len - SHA_DIGEST_LENGTH - 1, db);
    printf("\n");
    
    MGF1(seedMask, SHA_DIGEST_LENGTH , db, rsa_n_len - SHA_DIGEST_LENGTH -1 );
    
    printf("SeedMask is:\n");
    dump(SHA_DIGEST_LENGTH , seedMask);
    printf("\n");
    
    
    for(i = 0; i < SHA_DIGEST_LENGTH; i++)
    {
        seed[i] ^= seedMask[i];
    }
    
    printf("MaskedSeed is:\n");
    dump(SHA_DIGEST_LENGTH , seed);
    printf("\n");
    
    EM[0] = 0;
    
    
    printf("EM is:\n");
    dump(rsa_n_len , EM);
    printf("\n");
    
    
    ret = RSA_public_encrypt(rsa_n_len,EM,output,key,RSA_NO_PADDING );
    
    
    printf("Ciphertext is:\n");
    dump(rsa_n_len , output);
    printf("\n");
    
    return ret;
}


int rsaes_oaep_dec(const unsigned char * cipher, unsigned int cipher_len, const unsigned char * label, unsigned int label_len,
                   RSA * priv_key,
                   unsigned char* output)
{
    int i;
    unsigned char EM[600];
    unsigned char *db = &EM[1 + SHA_DIGEST_LENGTH];
    unsigned char *seed = &EM[1];
    unsigned char lhash[SHA_DIGEST_LENGTH];
    
    unsigned char dbMask[600];
    unsigned char seedMask[SHA_DIGEST_LENGTH];
    unsigned int ret;
    int rsa_n_len = BN_num_bytes(priv_key->n);
    
    
    CRYPTO_dbg_set_options(V_CRYPTO_MDEBUG_ALL);
    
    ret = RSA_private_decrypt(cipher_len, cipher, EM, priv_key, RSA_NO_PADDING);
    
    if(ret > 0)
    {
        printf("EM is:\n");
        dump(ret, EM);
    }else
    {
        printf("ret is %d\n", ret);
    }
    
    printf("the label is:\n");
    dump(label_len,label);
    
    sha1(label,label_len, lhash);
    printf("lHash is:\n");
    dump(SHA_DIGEST_LENGTH, lhash);
    
    
    MGF1(seedMask, SHA_DIGEST_LENGTH , db, rsa_n_len - SHA_DIGEST_LENGTH -1 );
    
    printf("SeedMask is:\n");
    dump(SHA_DIGEST_LENGTH , seedMask);
    printf("\n");
    
    
    for(i = 0; i < SHA_DIGEST_LENGTH; i++)
    {
        seed[i] ^= seedMask[i];
    }
    
    printf("Seed is:\n");
    dump(SHA_DIGEST_LENGTH , seed);
    printf("\n");
    
    
    MGF1(dbMask,rsa_n_len - SHA_DIGEST_LENGTH - 1, seed, SHA_DIGEST_LENGTH);
    
    printf("DBMask is:\n");
    dump(rsa_n_len - SHA_DIGEST_LENGTH - 1, dbMask);
    printf("\n");
    
    for(i=0; i<rsa_n_len - SHA_DIGEST_LENGTH - 1 ; i++)
        db[i] ^= dbMask[i];
    
    printf("DB is:\n");
    dump(rsa_n_len - SHA_DIGEST_LENGTH - 1, db);
    printf("\n");
    
    printf("Lhash from the ciphertext is:\n");
    dump(SHA_DIGEST_LENGTH, db);
    
    if(0!= memcmp(db, lhash, SHA_DIGEST_LENGTH) )
    {
        printf("Decrypting error: lHash doesn't match.\n");
        return -1;
    }
    
    for(i = SHA_DIGEST_LENGTH; i < rsa_n_len - SHA_DIGEST_LENGTH - 1; i++)
    {
        if(db[i] == 0x01)
            break;
    }
    
    if( i < rsa_n_len - SHA_DIGEST_LENGTH - 1)
    {
        memcpy(output, &db[i + 1], rsa_n_len - SHA_DIGEST_LENGTH - 1 - i);
        return rsa_n_len - SHA_DIGEST_LENGTH - 1 - i;
    }
    
    return ret;
    
}
const char * pubkey_msg_file_name ;
const char * usrpwd_msg_file_name ;

void send_pub_key_msg(RSA * key, const char * key_index,const char * label)
{
    const char msg_template[]=
    "{\"MSG_TYPE\":\"ENCRY_SCHM\",\n"
    "\"AUTH_SCHM\":\"USR_PWD_RSAES_OAEP_1\",\n"
    "\"KEY_INDEX\":\"%s\",\n"
    "\"n\":\"%s\",\n"
    "\"e\":\"%s\",\n"
    "\"LABEL\":\"%s\""
    "}\n";
    int bn_bin_len;
    unsigned char bn_bin[600];
    unsigned char base64_n[600];
    unsigned char base64_e[600];
    
    FILE * fp;
    
    bn_bin_len = BN_bn2bin(key->n, bn_bin);
    
    base64encode(base64_n, bn_bin, bn_bin_len);
    dbg_mem_info(bn_bin, bn_bin_len, "N is :");
    
    bn_bin_len = BN_bn2bin(key->e, bn_bin);
    base64encode(base64_e, bn_bin, bn_bin_len);
    dbg_mem_info(bn_bin, bn_bin_len, "E is :");
    
    printf(msg_template, key_index, base64_n, base64_e, label);
    
    fp = fopen(pubkey_msg_file_name, "w+");
    if(fp == NULL)
    {
        printf("Failed to create file pubkey.msg\n");
        return;
    }
    
    fprintf(fp,msg_template, key_index, base64_n, base64_e, label);
    fclose(fp);
}

void send_user_pwd_msg(unsigned char* encrypted_usr_pwd, long encrypted_usr_pwd_len, const char* key_index, const char* label)
{
    unsigned char base64_msg[600];
    
    base64encode(base64_msg, encrypted_usr_pwd, encrypted_usr_pwd_len);

    NSString *tosend = [NSString stringWithFormat:@"{\"MSG_TYPE\":\"AUTH_INFO\",\n\"AUTH_SCHM\":\"USR_PWD_RSAES_OAEP_1\",\n\"KEY_INDEX\":\"%s\",\n\"C\":\"%s\",\n\"LABEL\":\"%s\"}\n",key_index?key_index:"", base64_msg, label];
    NSString *path =[NSString stringWithCString:usrpwd_msg_file_name encoding:NSUTF8StringEncoding];
    [tosend writeToFile:path atomically:YES encoding:NSUTF8StringEncoding error:nil];
    NSLog(@"tosend-->%@",tosend);

}


typedef struct {
    const char * tag_name;
    char * tag_value;
    int tag_value_len;
} json_tag_lst_t;

static int load_json_file(const char * file_name, json_tag_lst_t *tag_list, int tag_list_size)
{
    int state = 0;
    char ch;
    char tag_name[256];
    char tag_value[512];
    int tag_name_len, tag_value_len;
    int i;
    int tags_cnt = 0;
    
    FILE *fp;
    
    fp = fopen(file_name,"rb");
    
    if(fp < 0)
    {
        printf("failed to open file %s\n",file_name);
        return -1;
    }
    
    while(!feof(fp))
    {
        if( 1 != fread(&ch, sizeof(ch),1,fp))
            break;
        
        switch(state)
        {
            case 0:
                if( '{' == ch )
                    state = 1;
                break;
            case 1:
                if( '\"' == ch )
                {
                    state = 2;
                    tag_name_len = 0;
                }
                break;
            case 2:
                if( '\"' != ch)
                {
                    tag_name[tag_name_len++] = ch;
                }
                else
                {
                    tag_name[tag_name_len] = 0;
                    state = 3;
                }
                break;
            case 3:
                if( ':' == ch)
                {
                    state = 4;
                }
                break;
            case 4:
                if( '\"' == ch )
                {
                    state = 5;
                    tag_value_len = 0;
                }
                break;
            case 5:
                if('\"' != ch)
                {
                    tag_value[tag_value_len++] = ch;
                }
                else
                {
                    tag_value[tag_value_len] = 0;
                    state = 7;
                    for(i = 0; i< tag_list_size; i++)
                    {
                        if( 0 == strcmp(tag_list[i].tag_name, tag_name))
                        {
                            strncpy(tag_list[i].tag_value,tag_value, tag_list[i].tag_value_len);
                            tags_cnt++;
                            break;
                        }
                    }
                    //printf("tag:%s, value:%s\n",tag_name, tag_value);
                }
                break;
            case 7:
                if(',' == ch)
                {
                    state = 1;
                }
                break;
        }
    }
    
    fclose(fp);
    
    return tags_cnt;
    
}


void recv_pub_key_msg(RSA * p_key, char *p_key_index, char* p_label)
{
    char key_index[50];
    char label[100];
    char key_n[600];
    char key_e[600];
    json_tag_lst_t pubkey_msg_tags[] =
    {
        {"KEY_INDEX", key_index, sizeof(key_index)},
        {"LABEL",label, sizeof(label)},
        {"n",key_n, sizeof(key_n)},
        {"e",key_e, sizeof(key_e)},
    };
    int bn_bin_len;
    unsigned char bn_bin[600];
    
    if( load_json_file(pubkey_msg_file_name,pubkey_msg_tags, sizeof(pubkey_msg_tags)/sizeof(pubkey_msg_tags[0]))< 0)
    {
        return;
    };
    
    if(p_key_index)
    {
        strcpy(p_key_index, key_index);
    }
    if(p_label)
    {
        strcpy(p_label, label);
    }
    
    printf("base64 decoded N:%s\n",key_n);
    bn_bin_len = base64decode(bn_bin, key_n, strlen(key_n));
    p_key->n = BN_bin2bn(bn_bin, bn_bin_len, p_key->n);
    
    dbg_mem_info(bn_bin,bn_bin_len, "N is :");
    
    
    printf("base64 decoding E:%s\n",key_e);
    bn_bin_len = base64decode(bn_bin, key_e, strlen(key_e));
    p_key->e = BN_bin2bn(bn_bin, bn_bin_len, p_key->e);
    dbg_mem_info(bn_bin,bn_bin_len, "E is :");
    
}


-(NSString *)parseKey:(NSString*)src rsan:(NSString *)rsan rsae:(NSString *)rsae rsalabe:(NSString *)rsalabe reskeyindex:(NSString *)key
{
//    *****************  解析从服务器接收数据 **************************
//    src = @"a3333333";
//    NSString * n = @"jEMNkBeUT8326Kq5/FQAJ2q/5SZZxnMMmC4C8XHl4P/3/PcXztR7SY6wbw+vqmC5HeuwE+XbMi797WWbY9Yww5nwSQSzj8GJg59QmT2zHwr9BEodWW0c0czRILDJ8b2uO8TGQXWlp1QhQKH5Qeixgdqmf7e2SY33gZ0L+JmMdD8=";
//   NSString * e = @"AQAB";
    NSString * n = rsan;
    
    NSString * e= rsae;
    NSString * label = rsalabe;
    NSString * keyindex = key;
    
//    NSString * label = @"for_testing_only";
//    NSString * keyindex = @"1";
    
    
//    从src中解析出公钥
    RSA * pubkey = RSA_new();
    int length ;
    const char * n1 = [n UTF8String];
    NSLog(@"454545454#$$$$%s",n1);
    const char * e1  = [e UTF8String];
     NSLog(@"454545454#$$$$%s",e1);
    unsigned char dest[600]={0} ;
    
    length = base64decode(dest, (unsigned char*)n1, strlen(n1));
    
    pubkey->n = BN_bin2bn(dest, length, pubkey->n);
    NSLog(@"%s\n",dest); 
    length = base64decode(dest, (unsigned char*)e1, strlen(e1));
   
    pubkey->e = BN_bin2bn(dest, length, pubkey->e);
    NSLog(@"%s\n",dest);
//    ******************** 加密帐号密码  ***********************
    
    //NSString * usrpwd = @"user=sha,passwd=1234567p";
    NSString * usrpwd = src;
    NSLog(@"密码%@",src);
    const char * m_str = [usrpwd UTF8String];
    const char * clilabel = [label UTF8String];
    unsigned char ciphertext[600];
    
    int enclen = rsaes_oaep_enc((const unsigned char*)m_str, strlen(m_str),(const unsigned char*)clilabel, strlen(clilabel), pubkey,ciphertext);
    if (enclen){
        NSLog(@"加密成功");
        NSLog(@"加密长度:%i",enclen);
    }else{
        NSLog(@"加密失败");
    }
 
//    ******************** 向服务器发送加密后数据  ***********************
    unsigned char base64_msg[600];
    
    base64encode(base64_msg, ciphertext, enclen);
    
    NSString * temp_str = [NSString stringWithCString:base64_msg encoding:NSUTF8StringEncoding];
    NSString *tosend = [NSString stringWithFormat:@"{\"MSG_TYPE\":\"AUTH_INFO\",\n\"AUTH_SCHM\":\"USR_PWD_RSAES_OAEP_1\",\n\"KEY_INDEX\":\"%@\",\n\"C\":\"%@\",\n\"RSAES_OAEP_Label\":\"%@\"}\n",keyindex?keyindex:@"", temp_str, label];
   
     NSLog(@"temp_str_____:%@",tosend);
    return tosend;
    
}
int recv_user_pwd_msg(unsigned char* encrypted_usr_pwd, char* p_key_index, char* p_label)
{
    int ret;
    char key_index[50];
    char label[100];
    char ciphertext[600];
    json_tag_lst_t pubkey_msg_tags[] =
    {
        {"KEY_INDEX", key_index, sizeof(key_index)},
        {"LABEL",label, sizeof(label)},
        {"C",ciphertext, sizeof(ciphertext)},
    };
    
    if( load_json_file(usrpwd_msg_file_name,pubkey_msg_tags, sizeof(pubkey_msg_tags)/sizeof(pubkey_msg_tags[0]))< 0)
    {
        return -1;
    };
    
    if(p_key_index)
    {
        strcpy(p_key_index, key_index);
    }
    if(p_label)
    {
        strcpy(p_label, label);
    }
    
    printf("base64 decoding ciphertext:%s\n",ciphertext);
    ret = base64decode(encrypted_usr_pwd, ciphertext, strlen(ciphertext));
    
    dbg_mem_info(encrypted_usr_pwd, ret, "Received ciphertext is:");
    return ret;
}

-(void)test
{
    const char* m_str = "1234567p";
    RSA *key = NULL;  //used by server
    RSA *pubkey = NULL; //used by the client.
    int ret;
    const char key_index[]="key_1";
    const char label[]="for_testing_only";
    
    char client_received_key_index[200];
    char client_received_label[200];
    
    unsigned char ciphertext[600];
    unsigned char received_ciphertext[600];
    char server_received_key_index[200];
    char server_received_label[200];
    unsigned char decrypted_msg[600];
    
    key = RSA_new();
    
    NSString * keypath = [[NSBundle mainBundle]pathForResource:@"key" ofType:@"txt"];
    const char * path = [keypath UTF8String];
    [self loadKey:key path:path];

//    recv--- 从服务器收到公钥后存到文件
    NSString * recv=[NSHomeDirectory() stringByAppendingPathComponent:@"Documents/pubkey.msg"];
    
//    send---  客户端向服务器发送帐号密码的数据存到文件
    NSString * send=[NSHomeDirectory() stringByAppendingPathComponent:@"Documents/usrpwd.msg"];
    
    pubkey_msg_file_name = [recv UTF8String];
    usrpwd_msg_file_name = [send UTF8String];
    
    printf("==== Server: Sending the json packet which contains the public key ====\n");
    
    send_pub_key_msg(key,key_index,label);
    
    pubkey = RSA_new();
    
    
    printf("==== Client: Receving the json packet which contains the public key ====\n");
    recv_pub_key_msg(pubkey,client_received_key_index,client_received_label);
    
    
    printf("==== Client: Encrypting the message with the public key ====\n");
    
    ret = rsaes_oaep_enc((const unsigned char*)m_str, strlen(m_str),
                         (const unsigned char*)client_received_label, strlen(client_received_label), pubkey,
                         ciphertext);
    printf("加密后的内容:%s\n",ciphertext);
    
    printf("==== Client: Sending the ciphertext  ====\n");
    send_user_pwd_msg(ciphertext,ret,client_received_key_index,client_received_label);
    
    
    printf("==== Server: Receiving the ciphertext  ====\n");
    recv_user_pwd_msg(received_ciphertext, server_received_key_index, server_received_label);
    
    printf("==== Server: Decrypting the ciphertext ====\n");
    
    ret = rsaes_oaep_dec(received_ciphertext, ret,(const unsigned char*)server_received_label, strlen(server_received_label), key,decrypted_msg);
    printf("解密后的内容: %s\n",(char*)decrypted_msg);
    
    
    RSA_free(key);
    
    if(ret > 0)
    {
        printf("The message is:\n");
        dump(ret, decrypted_msg);
    }
    
}


@end
