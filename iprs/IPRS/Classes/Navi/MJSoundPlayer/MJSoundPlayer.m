//
//  MJSoundPlayer.m
//  鲜特汇收银台
//
//  Created by 郭明健 on 2018/6/2.
//  Copyright © 2018年 GuoMingJian. All rights reserved.
//

#import "MJSoundPlayer.h"
#import <AVFoundation/AVFoundation.h>

@interface MJSoundPlayer ()

@property (nonatomic, strong) AVAudioPlayer *myPlayer;

@end

@implementation MJSoundPlayer

+ (instancetype)allocWithZone:(struct _NSZone *)zone
{
    static MJSoundPlayer *player = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        if (player == nil) {
            player = [super allocWithZone:zone];
            [player setDefaultWithVolume:-1.0 rate:0.4 pitchMultiplier:-1.0];
        }
    });
    return player;
}

+ (instancetype)shareInstance
{
    return [[MJSoundPlayer alloc] init];
}

/**
 *  设置播放的声音参数 如果选择默认请传入 -1.0
 *
 *  @param aVolume          音量（0.0~1.0）默认为1.0
 *  @param aRate            语速（0.0~1.0）
 *  @param aPitchMultiplier 语调 (0.5-2.0)
 */
- (void)setDefaultWithVolume:(float)aVolume
                        rate:(CGFloat)aRate
             pitchMultiplier:(CGFloat)aPitchMultiplier
{
    self.volume = (aVolume == -1.0) ? 1 : aVolume;
    self.rate = (aRate == -1.0) ? 1 : aRate;
    self.pitchMultiplier = (aPitchMultiplier == -1.0) ? 1 : aPitchMultiplier;
}

/**
 开始播放文字
 */
- (void)play:(NSString *)string
{
    if(string && string.length > 0)
    {
        AVSpeechSynthesizer *player  = [[AVSpeechSynthesizer alloc] init];
        AVSpeechUtterance *utterance = [[AVSpeechUtterance alloc] initWithString:string];//设置语音内容
        utterance.voice = [AVSpeechSynthesisVoice voiceWithLanguage:@"zh-CN"];//设置语言
        utterance.rate = self.rate;  //设置语速
        utterance.volume = self.volume;  //设置音量（0.0~1.0）默认为1.0
        utterance.pitchMultiplier = self.pitchMultiplier;  //设置语调 (0.5-2.0)
        utterance.postUtteranceDelay = 1; //目的是让语音合成器播放下一语句前有短暂的暂停
        [player speakUtterance:utterance];
    }
}

/**
 播放制定路径的音频文件
 */
- (void)playAudioWithURL:(NSURL *)fileURL
{
    _myPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:fileURL error:nil];
    [_myPlayer play];
}

@end
