//
//  CordovaListener.h
//  IPRS
//
//  Created by linyingwei on 2018/4/11.
//

#import <Foundation/Foundation.h>
#import <Cordova/CDVPlugin.h>

@interface CordovaListener : CDVPlugin

#pragma mark - 密码键盘弹起
- (void)pwdKeyboard:(CDVInvokedUrlCommand *)command;
#pragma mark - 密码键盘收起
- (void)hideKeyboard:(CDVInvokedUrlCommand *)command;
#pragma mark - Apin Epin加密
- (void)pwdEncrypt:(CDVInvokedUrlCommand *)command;
#pragma mark - 获取pushId
- (void)getPushId:(CDVInvokedUrlCommand *)command;
#pragma mark - 定位
- (void)location:(CDVInvokedUrlCommand *)command;
#pragma mark - 扫码
- (void)scan:(CDVInvokedUrlCommand *)command;
#pragma mark - 相册
- (void)getPhoto:(CDVInvokedUrlCommand *)command;
#pragma mark - 拍照
- (void)takePhoto:(CDVInvokedUrlCommand *)command;
#pragma mark - 保存图片至相册
- (void)saveImageDataToLibrary:(CDVInvokedUrlCommand*)command;
#pragma mark - 获取版本号
- (void)getVersionName:(CDVInvokedUrlCommand*)command;
#pragma mark - 获取设备型号
- (void)getDeviceType:(CDVInvokedUrlCommand*)command;

#pragma mark - 加密
- (void)sm2Encryption:(CDVInvokedUrlCommand *)command;
- (void)sm4Encryption:(CDVInvokedUrlCommand *)command;
- (void)sm4DecryptECB:(CDVInvokedUrlCommand *)command;

@end
