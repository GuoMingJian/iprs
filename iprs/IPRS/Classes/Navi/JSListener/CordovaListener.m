//
//  CordovaListener.m
//  IPRS
//
//  Created by linyingwei on 2018/4/11.
//

#import "CordovaListener.h"
//钥匙串
#import "LKeyChain.h"
//推送
#import "NewsPush.h"
//键盘
#import "NaiveKeyboardController.h"
#import "CommentDefine.h"
//定位
#import "IPRSLocation.h"
//国密
#import "CherryCodeSecurityManager.h"
#import "SecuritySM4.h"
//百度地图
#import <BMKLocationKit/BMKLocation.h>
//扫描
#import "MMScanViewController.h"
//拍照
#import "IPRSTakePhotoes.h"
//导航栏
#import "IPRSNaviViewController.h"
#import <sys/utsname.h>

@interface CordovaListener()

//拍照对象
@property (nonatomic,strong) IPRSTakePhotoes *takePhotoesModel;
//导航栏
@property (nonatomic,strong) IPRSNaviViewController *naviVc;

@property (nonatomic, copy) NSString* callbackId;

@end

@implementation CordovaListener

#pragma mark - 密码键盘弹出
- (void)pwdKeyboard:(CDVInvokedUrlCommand *)command{
    [self.commandDelegate runInBackground:^{
        MAIN(^{
            [NaiveKeyboardController initKeyboard:KeyWindow];
            //normal
            [NaiveKeyboardController showKeyboard:16 numAndCharType:NO andIsNumber:NO];
            //回调
            [[NaiveKeyboardController getKeyboardView]setPressValueBlock:^(NSString *str) {
                NSLog(@"%@",str);
                CDVPluginResult * pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:@{@"value":str}];
                [pluginResult setKeepCallbackAsBool:YES];
                [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
            }];
        })
    }];
}

#pragma mark - 密码键盘收起
- (void)hideKeyboard:(CDVInvokedUrlCommand *)command{
    MAIN(^{
        [NaiveKeyboardController hideKeyboard];
    })
}

#pragma mark - 抑或算法
- (NSData *)xor_encrypt:(NSString *)plain random:(NSString *)random
{
    NSData *plainData = [plain dataUsingEncoding:NSUTF8StringEncoding];
    NSData *randData = [random dataUsingEncoding:NSUTF8StringEncoding];

    NSMutableData *encryptData = [NSMutableData data];

    const Byte *plainByte = plainData.bytes;
    const Byte *randomByte = randData.bytes;
    for (int i = 0; i < plainData.length; i ++)
    {
        char a = randomByte[i % randData.length];
        char c = plainByte[i];
        Byte b = (Byte) (a ^ c);
        [encryptData appendBytes:&b length:1];
    }

    return encryptData.copy;
}

#pragma mark -- Apin Epin加密
- (void)pwdEncrypt:(CDVInvokedUrlCommand *)command
{
    [self.commandDelegate runInBackground:^{
        //密码
        NSString *pwdStr = [NaiveKeyboardController deciphering:command.arguments[0]];
        //随机数
        NSString *randomStr = command.arguments[1];
        //NSLog(@"密码：%@====随机码：%@", pwdStr, randomStr);

        //========================================//
        //随机码跟明文抑或运算
        NSData *encryptData = [self xor_encrypt:pwdStr random:randomStr];
        NSString *encryptStr  = [[NSString alloc] initWithData:encryptData encoding:NSUTF8StringEncoding];
        //公钥
        NSString *sm2pubKey = @"";
        if (kIsPro)
        {//生产环境
            sm2pubKey = @"593754BD89784D9ACE75A384F11A8901E8C058B6F52BE4EFA73D8DDDC2AA688A785DDD4316B0BBFCEA79613C076727DEF5ED955CFDE2B1B3D5FCC3D735697DA9";
        }
        else
        {//开发环境
            sm2pubKey = @"2C92390D6EB6D6A6D41E142BBC87CCCD41D8C92CFDED7C5A49FBBD5BB357E6E7056653A132E97EB45A667E73B1D3DB9EF0027F8E623192BD1D120C7C804717FB";
        }
        //SM2
        NSData *newdata = [[SecuritySM4 alloc] securitySM2Encrypt:encryptStr key:sm2pubKey];
        NSString *apinValue = [[NSString alloc] initWithData:newdata encoding:NSUTF8StringEncoding];
        //NSLog(@"%@\n长度：%ld", apinValue, apinValue.length);
        //========================================//

        CDVPluginResult * pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:@{@"apin":apinValue, @"original":pwdStr}];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }];
}

#pragma mark - 获取pushId
- (void)getPushId:(CDVInvokedUrlCommand *)command{
#if TARGET_IPHONE_SIMULATOR
#else
    [self.commandDelegate runInBackground:^{
        //取uuid
        NSString *uuid = [LKeyChain getDeviceId];
        //取pushid
        NSString *pushId = [NewsPush getID];
        pushId = kStringIsEmpty(pushId) ? @"111" : pushId;
        NSLog(@"uuid=%@ pushId=%@",uuid,pushId);
        CDVPluginResult * pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:@{@"pushId":pushId,@"uuid":uuid}];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }];
#endif
}

//定位
- (void)location:(CDVInvokedUrlCommand *)command{
#if TARGET_IPHONE_SIMULATOR
#else
    //    [self.commandDelegate runInBackground:^{
    //        MAIN((^{
    __weak __typeof(self)weakSelf = self;
    IPRSLocation *ps = [[IPRSLocation alloc]init];
    [ps setLocationBlock:^(BMKLocation *location) {
        __strong __typeof(weakSelf)strongSelf = weakSelf;
        CDVPluginResult * pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:@{
                                                                                                                     @"longtitude":[NSString stringWithFormat:@"%lf",location.location.coordinate.longitude],
                                                                                                                     @"latitude":[NSString stringWithFormat:@"%lf",location.location.coordinate.latitude],
                                                                                                                     @"accode":location.rgcData.adCode,
                                                                                                                     @"country":location.rgcData.country,
                                                                                                                     @"province":location.rgcData.province,
                                                                                                                     @"district":location.rgcData.district,
                                                                                                                     @"street":location.rgcData.street,
                                                                                                                     @"address":[NSString stringWithFormat:@"%@%@%@%@",location.rgcData.province,location.rgcData.city,location.rgcData.district,location.rgcData.street],
                                                                                                                     @"city":location.rgcData.city
                                                                                                                     }];
        [strongSelf.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }];
    [ps location];
    //        }))
    //
    //    }];
#endif
}

#pragma mark - 扫码
- (void)scan:(CDVInvokedUrlCommand *)command{
    //    [self.commandDelegate runInBackground:^{
    //        MAIN((^{
    NSArray *arr = command.arguments;
    NSString *money = @"";
    if (!kArrayIsEmpty(arr)) {
        money = arr[0];
    }
    __weak __typeof(self) weakSelf = self;
    MMScanViewController *scanVc = [[MMScanViewController alloc] initWithQrType:MMScanTypeQrCode onFinish:^(NSString *result, NSError *error) {
        __strong __typeof(self) strongSelf = weakSelf;
        if (error) {
            NSLog(@"error: %@",error);
        } else {
            NSLog(@"扫描结果：%@",result);
            CDVPluginResult * pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:@{@"flag":@"true",@"code":result}];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
            [strongSelf.naviVc dismissViewControllerAnimated:YES completion:nil];
        }
    }];
    [scanVc setPayCordBlock:^(NSString *text) {
        CDVPluginResult * pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:@{@"flag":@"true",@"code":text}];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }];
    [scanVc setChangePayBlock:^(NSDictionary *dic) {
        CDVPluginResult * pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:@{@"flag":@"false",@"code":@""}];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }];
    scanVc.money = money;
    IPRSNaviViewController *navi = [[IPRSNaviViewController alloc]initWithRootViewController:scanVc];
    _naviVc = navi;
    UIWindow * window = [[UIApplication sharedApplication] keyWindow];
    [window.rootViewController presentViewController:navi animated:YES completion:nil];
    //        }))
    //    }];
}

#pragma mark - 相册
- (void)getPhoto:(CDVInvokedUrlCommand *)command{
    __weak __typeof(self) weakSelf = self;
    [self.commandDelegate runInBackground:^{
        __strong __typeof(self) strongSelf = weakSelf;
        MAIN((^{
            //拍照
            [self.takePhotoesModel openPhotoLibrary];
            [strongSelf.takePhotoesModel setPhotoBlock:^(NSString *img) {
                CDVPluginResult * pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:@{@"photobase64":img,@"fileName":@""}];
                [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
            }];
        }))
    }];
}

#pragma mark - 拍照
- (void)takePhoto:(CDVInvokedUrlCommand *)command{
    __weak __typeof(self) weakSelf = self;
    [self.commandDelegate runInBackground:^{
        __strong __typeof(self) strongSelf = weakSelf;
        MAIN((^{
            //拍照
            [self.takePhotoesModel openCamera];
            [strongSelf.takePhotoesModel setPhotoBlock:^(NSString *img) {
                CDVPluginResult * pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:@{@"photobase64":img,@"fileName":@""}];
                [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
            }];
        }))
    }];
}

#pragma mark - 保存图片至相册
- (void)saveImageDataToLibrary:(CDVInvokedUrlCommand*)command
{
    self.callbackId = command.callbackId;
    NSString *base64Str = command.arguments[0];
    NSData* imageData = [[NSData alloc] initWithBase64EncodedString:base64Str options:0];
    UIImage* image = [[UIImage alloc] initWithData:imageData];
    
    if (image)
    {
        UIImageWriteToSavedPhotosAlbum(image, self, @selector(image:didFinishSavingWithError:contextInfo:), nil);
    }
}

- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo
{
    // Was there an error?
    if (error != NULL)
    {
        // Show error message...
        NSLog(@"ERROR: %@",error);
        CDVPluginResult* result = [CDVPluginResult resultWithStatus: CDVCommandStatus_ERROR messageAsString:error.description];
        [self.commandDelegate sendPluginResult:result callbackId:self.callbackId];
    }
    else  // No errors
    {
        // Show message image successfully saved
        NSLog(@"IMAGE SAVED!");
        CDVPluginResult* result = [CDVPluginResult resultWithStatus: CDVCommandStatus_OK messageAsString:@"Image saved"];
        [self.commandDelegate sendPluginResult:result callbackId:self.callbackId];
    }
}

#pragma mark - 获取版本号
- (void)getVersionName:(CDVInvokedUrlCommand*)command
{
    NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];

    CDVPluginResult* result = [CDVPluginResult resultWithStatus: CDVCommandStatus_OK messageAsString:version];
    [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
}

#pragma mark - 获取设备型号
- (void)getDeviceType:(CDVInvokedUrlCommand*)command
{
    NSString *deviceType = [self iPhoneModel];

    CDVPluginResult* result = [CDVPluginResult resultWithStatus: CDVCommandStatus_OK messageAsString:deviceType];
    [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
}

/**
 手机型号
 */
- (NSString *)iPhoneModel
{
    struct utsname systemInfo;
    uname(&systemInfo);
    NSString *platform = [NSString stringWithCString:systemInfo.machine encoding:NSASCIIStringEncoding];

    //------------------------------iPhone---------------------------
    if ([platform isEqualToString:@"iPhone1,1"]) return @"iPhone 2G";
    if ([platform isEqualToString:@"iPhone1,2"]) return @"iPhone 3G";
    if ([platform isEqualToString:@"iPhone2,1"]) return @"iPhone 3GS";
    if ([platform isEqualToString:@"iPhone3,1"] || [platform isEqualToString:@"iPhone3,2"] || [platform isEqualToString:@"iPhone3,3"]) return @"iPhone 4";
    if ([platform isEqualToString:@"iPhone4,1"]) return @"iPhone 4S";
    if ([platform isEqualToString:@"iPhone5,1"] || [platform isEqualToString:@"iPhone5,2"]) return @"iPhone 5";
    if ([platform isEqualToString:@"iPhone5,3"] || [platform isEqualToString:@"iPhone5,4"]) return @"iPhone 5c";
    if ([platform isEqualToString:@"iPhone6,1"] || [platform isEqualToString:@"iPhone6,2"]) return @"iPhone 5s";
    if ([platform isEqualToString:@"iPhone7,2"]) return @"iPhone 6";
    if ([platform isEqualToString:@"iPhone7,1"]) return @"iPhone 6 Plus";
    if ([platform isEqualToString:@"iPhone8,1"]) return @"iPhone 6s";
    if ([platform isEqualToString:@"iPhone8,2"]) return @"iPhone 6s Plus";
    if ([platform isEqualToString:@"iPhone8,4"]) return @"iPhone SE";
    if ([platform isEqualToString:@"iPhone9,1"] || [platform isEqualToString:@"iPhone9,3"]) return @"iPhone 7";
    if ([platform isEqualToString:@"iPhone9,2"] || [platform isEqualToString:@"iPhone9,4"]) return @"iPhone 7 Plus";
    if ([platform isEqualToString:@"iPhone10,1"] || [platform isEqualToString:@"iPhone10,4"]) return @"iPhone 8";
    if ([platform isEqualToString:@"iPhone10,2"] || [platform isEqualToString:@"iPhone10,5"]) return @"iPhone 8 Plus";
    if ([platform isEqualToString:@"iPhone10,3"] || [platform isEqualToString:@"iPhone10,6"]) return @"iPhone X";
    if ([platform isEqualToString:@"iPhone11,8"]) return @"iPhone XR";
    if ([platform isEqualToString:@"iPhone11,2"]) return @"iPhone XS";
    if ([platform isEqualToString:@"iPhone11,4"] || [platform isEqualToString:@"iPhone11,6"]) return @"iPhone XS Max";
    //------------------------------iPad--------------------------
    if ([platform isEqualToString:@"iPad1,1"]) return @"iPad";
    if ([platform isEqualToString:@"iPad2,1"] || [platform isEqualToString:@"iPad2,2"] || [platform isEqualToString:@"iPad2,3"] || [platform isEqualToString:@"iPad2,4"]) return @"iPad 2";
    if ([platform isEqualToString:@"iPad3,1"] || [platform isEqualToString:@"iPad3,2"] || [platform isEqualToString:@"iPad3,3"]) return @"iPad 3";
    if ([platform isEqualToString:@"iPad3,4"] || [platform isEqualToString:@"iPad3,5"] || [platform isEqualToString:@"iPad3,6"]) return @"iPad 4";
    if ([platform isEqualToString:@"iPad4,1"] || [platform isEqualToString:@"iPad4,2"] || [platform isEqualToString:@"iPad4,3"]) return @"iPad Air";
    if ([platform isEqualToString:@"iPad5,3"] || [platform isEqualToString:@"iPad5,4"]) return @"iPad Air 2";
    if ([platform isEqualToString:@"iPad6,3"] || [platform isEqualToString:@"iPad6,4"]) return @"iPad Pro 9.7-inch";
    if ([platform isEqualToString:@"iPad6,7"] || [platform isEqualToString:@"iPad6,8"]) return @"iPad Pro 12.9-inch";
    if ([platform isEqualToString:@"iPad6,11"] || [platform isEqualToString:@"iPad6,12"]) return @"iPad 5";
    if ([platform isEqualToString:@"iPad7,1"] || [platform isEqualToString:@"iPad7,2"]) return @"iPad Pro 12.9-inch 2";
    if ([platform isEqualToString:@"iPad7,3"] || [platform isEqualToString:@"iPad7,4"]) return @"iPad Pro 10.5-inch";
    //------------------------------iPad Mini-----------------------
    if ([platform isEqualToString:@"iPad2,5"] || [platform isEqualToString:@"iPad2,6"] || [platform isEqualToString:@"iPad2,7"]) return @"iPad mini";
    if ([platform isEqualToString:@"iPad4,4"] || [platform isEqualToString:@"iPad4,5"] || [platform isEqualToString:@"iPad4,6"]) return @"iPad mini 2";
    if ([platform isEqualToString:@"iPad4,7"] || [platform isEqualToString:@"iPad4,8"] || [platform isEqualToString:@"iPad4,9"]) return @"iPad mini 3";
    if ([platform isEqualToString:@"iPad5,1"] || [platform isEqualToString:@"iPad5,2"]) return @"iPad mini 4";
    //------------------------------iTouch------------------------
    if ([platform isEqualToString:@"iPod1,1"]) return @"iTouch";
    if ([platform isEqualToString:@"iPod2,1"]) return @"iTouch2";
    if ([platform isEqualToString:@"iPod3,1"]) return @"iTouch3";
    if ([platform isEqualToString:@"iPod4,1"]) return @"iTouch4";
    if ([platform isEqualToString:@"iPod5,1"]) return @"iTouch5";
    if ([platform isEqualToString:@"iPod7,1"]) return @"iTouch6";
    //------------------------------Samulitor-------------------------------------
    if ([platform isEqualToString:@"i386"] || [platform isEqualToString:@"x86_64"])
        return @"iPhone Simulator";

    return @"Unknown";
}

#pragma mark - 懒加载
- (IPRSTakePhotoes *)takePhotoesModel{
    if (!_takePhotoesModel) {
        _takePhotoesModel = [[IPRSTakePhotoes alloc]init];
    }
    return _takePhotoesModel;
}

#pragma mark - ----------------------------- start-------------------------------------
#pragma mark -
- (void)sm2Encryption:(CDVInvokedUrlCommand *)command {
    [self.commandDelegate runInBackground:^{
        
        //获取客户端随机数
        NSString *clientRandom = [self getClientRandom];
        //报文
        NSString *sm2pubKey = [[CherryCodeSecurityManager sharedInstance] securityGetSM2PublicKey];
        //判断参数是否为空
        if(kStringIsEmpty(sm2pubKey) || !(clientRandom && clientRandom.length)){
            NSLog(@"sm2Encrypt参数为空");
            return;
        }
        
        //加密
        NSString *baseStr = [[CherryCodeSecurityManager sharedInstance] securitySM2Encrypt:clientRandom key:sm2pubKey];
        
        //回调结果
        CDVPluginResult *pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:baseStr];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        
    }];
}

- (NSString *)getClientRandom
{
    CFUUIDRef uuidObject = CFUUIDCreate(kCFAllocatorDefault);
    CFStringRef uuidRef = CFUUIDCreateString(kCFAllocatorDefault, uuidObject);
    CFRelease(uuidObject);
    
    NSString *uuid = [NSString stringWithString:(__bridge NSString *)uuidRef];
    CFRelease(uuidRef);
    
    return [[NSString stringWithFormat:@"%@", [uuid stringByReplacingOccurrencesOfString:@"-" withString:@""]] substringToIndex:16];
}

#pragma mark -sm4ecb加密
- (void)sm4Encryption:(CDVInvokedUrlCommand *)command
{
    [self.commandDelegate runInBackground:^{
        NSString *plainText = command.arguments[0];
        
        // sm4key
        NSString *sm4Key = command.arguments[1];
        
        //判断参数是否为空
        if(kStringIsEmpty(plainText) || kStringIsEmpty(sm4Key)){
            NSLog(@"sm4EncryptECB参数为空");
            return;
        }
        
        //明文转data, 直接转utf8,CherryCodeSecurityManager内部会进行base64
        NSData *data = [plainText dataUsingEncoding:NSUTF8StringEncoding];
        //        NSData *data = [[CherryCodeSecurityManager sharedInstance] securityBase64ToBytes:plainText];
        //公钥转data
        NSData *key = [sm4Key dataUsingEncoding:NSUTF8StringEncoding];
        //        NSData *key = [[CherryCodeSecurityManager sharedInstance] securityBase64ToBytes:sm4Key];
        //加密
        NSData *ecbData = [[CherryCodeSecurityManager sharedInstance] securitySM4CBCEncryptWithSecretKey:key src:data];
        //base64,转str
        NSString *ecbStr = [[CherryCodeSecurityManager sharedInstance] securityBytesToBase64:ecbData];
        
        //回调结果
        CDVPluginResult *pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:ecbStr];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        
    }];
    
}

#pragma mark -sm4ecb解密
- (void)sm4DecryptECB:(CDVInvokedUrlCommand *)command
{
    [self.commandDelegate runInBackground:^{
        
        //密文
        id params = command.arguments[0];
        //TODO: 这里应该是要回调错误信息告知js参数有问题
        NSString *encText;
        if ([params isKindOfClass:[NSDictionary class]]) {
            //握手回调数据也会被js传过来, 明文字典, 直接返回即可. 通过:OYANG:判断是否是握手接口数据
            if([params[@"data"] isKindOfClass:[NSString class]]){
                encText = params[@"data"];
                if ([encText containsString:@":OYANG:"]) {
                    CDVPluginResult * pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:params];
                    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
                    return ;
                }
                return;
            }
            return;
        } else if ([params isKindOfClass:[NSString class]]) {
            //sm4密文
            encText = params;
        } else {
            return;
        }
        
        
        //sm4Key
        NSString *sm4Key = command.arguments[1];
        
        //判断参数是否为空
        if(kStringIsEmpty(encText) || kStringIsEmpty(sm4Key)){ return; }
        
        //明文转data
        //            NSData *data = [encText dataUsingEncoding:NSUTF8StringEncoding];
        NSData *data = [[CherryCodeSecurityManager sharedInstance] securityBase64ToBytes:encText];
        //公钥转data
        NSData *key = [sm4Key dataUsingEncoding:NSUTF8StringEncoding];
        //        NSData *key = [[CherryCodeSecurityManager sharedInstance] securityBase64ToBytes:sm4Key];
        //解密
        NSData *ecbData= [[CherryCodeSecurityManager sharedInstance] securitySM4CBCDecryptWithSecretKey:key src:data];
        //转str
        NSString *plainStr = [[NSString alloc] initWithData:ecbData encoding:NSUTF8StringEncoding];
        //回调数据
        CDVPluginResult * pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:plainStr];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }];
}
#pragma mark - ----------------------------- end-------------------------------------

#pragma mark - 转16进制
- (NSString *)convertDataToHexStr:(NSData *)data
{
    if (!data || [data length] == 0) {
        return @"";
    }
    NSMutableString *string = [[NSMutableString alloc] initWithCapacity:[data length]];
    
    [data enumerateByteRangesUsingBlock:^(const void *bytes, NSRange byteRange, BOOL *stop) {
        unsigned char *dataBytes = (unsigned char*)bytes;
        for (NSInteger i = 0; i < byteRange.length; i++) {
            NSString *hexStr = [NSString stringWithFormat:@"%x", (dataBytes[i]) & 0xff];
            if ([hexStr length] == 2) {
                [string appendString:hexStr];
            } else {
                [string appendFormat:@"0%@", hexStr];
            }
        }
    }];
    return string;
}

#pragma mark -sm2解密
- (NSString *)sm2Decrypt:(NSArray *)command{
    //明文
    NSString *source = command[0];
    //私钥
    NSString *pubKeyStr = command[1];
    //判断参数是否为空
    if(kStringIsEmpty(source) || kStringIsEmpty(pubKeyStr)){
        NSLog(@"sm2Decrypt参数为空");
        return @"";
    }
    //明文转data
    NSData *data = [source dataUsingEncoding:NSUTF8StringEncoding];
    //decode base64 data
    data = [[CherryCodeSecurityManager sharedInstance] securityBase64Decode:data];
    
    //解密
    NSData *ecbData= [[CherryCodeSecurityManager sharedInstance] securitySM2Decrypt:data key:pubKeyStr];
    //转utf8
    NSString *ecbStr = [[NSString alloc] initWithData:ecbData encoding:NSUTF8StringEncoding];
    
    return ecbStr;
}

#pragma mark -sm3加签
- (NSString *)sm3Digest:(NSArray *)command{
    //报文
    NSString *source = command[0];
    //判断参数是否为空
    if(kStringIsEmpty(source)){
        NSLog(@"sm3Digest参数为空");
        return @"";
    }
    //sm3加签
    NSString *sm3Str = [[CherryCodeSecurityManager sharedInstance]securitySM3HexString:[source dataUsingEncoding:NSUTF8StringEncoding]];
    NSLog(@"Well sm3加签：%@",sm3Str);
    return sm3Str;
}
@end








