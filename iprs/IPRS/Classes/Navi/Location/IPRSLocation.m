//
//  IPRSLocation.m
//  IPRS
//
//  Created by linyingwei on 2018/4/20.
//  Copyright © 2018年 linyingwei. All rights reserved.
//

#import "IPRSLocation.h"
//定位
#import <BMKLocationkit/BMKLocationComponent.h>
#import <BMKLocationKit/BMKLocationAuth.h>
//地图
#import <BaiduMapAPI_Search/BMKGeocodeSearch.h>
//通用
#import "CommentDefine.h"
@interface IPRSLocation()<BMKGeoCodeSearchDelegate,BMKLocationManagerDelegate,BMKLocationAuthDelegate>
//地理编码
@property (nonatomic,strong) BMKGeoCodeSearch *searcher;
//定位对象
@property (nonatomic,strong) BMKLocationManager *locationManager;
//回调
@property (nonatomic,copy) BMKLocatingCompletionBlock completionBlock;
@end
@implementation IPRSLocation
#pragma mark - 初始化
- (instancetype)init{
    self = [super init];
    if (!self) {
        return self;
    }
    //代理
    [[BMKLocationAuth sharedInstance] checkPermisionWithKey:kBaiduMapKit authDelegate:self];
    return self;
}

#pragma mark - 设置定位
- (void)location{
    [self.locationManager startUpdatingLocation];
    [self.locationManager setLocatingWithReGeocode:YES];
    [self.locationManager startUpdatingLocation];
}

#pragma mark - 懒加载
- (BMKLocationManager *)locationManager{
    if (!_locationManager) {
        //初始化实例
        _locationManager = [[BMKLocationManager alloc] init];
        //设置delegate
        _locationManager.delegate = self;
        //设置返回位置的坐标系类型
        _locationManager.coordinateType = BMKLocationCoordinateTypeBMK09LL;
        //设置距离过滤参数
        _locationManager.distanceFilter = kCLDistanceFilterNone;
        //设置预期精度参数
        _locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
        //设置应用位置类型
        _locationManager.activityType = CLActivityTypeAutomotiveNavigation;
        //设置是否自动停止位置更新
        _locationManager.pausesLocationUpdatesAutomatically = NO;
        //设置是否允许后台定位
        _locationManager.allowsBackgroundLocationUpdates = YES;
        //设置位置获取超时时间
        _locationManager.locationTimeout = 10;
        //设置获取地址信息超时时间
        _locationManager.reGeocodeTimeout = 10;
    }
    return _locationManager;
}

#pragma mark - 定位代理
- (void)BMKLocationManager:(BMKLocationManager * _Nonnull)manager didUpdateLocation:(BMKLocation * _Nullable)location orError:(NSError * _Nullable)error

{
    if (error)
    {
        NSLog(@"locError:{%ld - %@};", (long)error.code, error.localizedDescription);
    }
    if (location) {//得到定位信息，添加annotation
        if (location.location) {
            NSLog(@"LOC = %@",location.location);
//            location.location.coordinate.longitude
//            location.location.coordinate.latitude
        }
        if (!location.rgcData) {
            NSLog(@"rgc = %@",[location.rgcData description]);
//            location.rgcData.country
//            location.rgcData.adCode
//            location.rgcData.country
//            location.rgcData.province
//            location.rgcData.district
//            location.rgcData.street
//            location.rgcData.locationDescribe
            return;
        }
        //回调
        if (_locationBlock) {
            _locationBlock(location);
        }
        [self.locationManager stopUpdatingLocation];
    }
}
@end

