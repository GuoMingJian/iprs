//
//  IPRSLocation.h
//  IPRS
//
//  Created by linyingwei on 2018/4/20.
//  Copyright © 2018年 linyingwei. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <BMKLocationKit/BMKLocation.h>
@interface IPRSLocation : NSObject
//定位回调
@property (nonatomic,copy) void (^locationBlock)(BMKLocation *location);
//定位
- (void)location;
@end
