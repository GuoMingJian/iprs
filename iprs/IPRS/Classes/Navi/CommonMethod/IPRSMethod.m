//
//  IPRSMethod.m
//  IPRS
//
//  Created by linyingwei on 2018/5/23.
//

#import "IPRSMethod.h"

//#define kBundleID @"com.cgbchina.iprs"//中信--bundle id
#define kBundleID @"com.cgbchina.iprs.businessapp"//广发--bundle id

@implementation IPRSMethod

#pragma mark - 判断生产环境
+ (BOOL)judgePro {
    NSString *bundleId = [[NSBundle mainBundle] bundleIdentifier];
    if ([bundleId isEqualToString:kBundleID]) {
        //生产
        return YES;
    }
    return NO;
}

@end
