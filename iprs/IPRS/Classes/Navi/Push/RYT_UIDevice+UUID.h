//
//  RYT_UIDevice+UUID.h
//  Utility
//
//  Created by linjitao on 13-10-12.
//
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface UIDevice (UUID)
/**
    get a unique identifier. this identifier save in keychain.
 */
- (NSString *)deviceIdentifier;
@end
