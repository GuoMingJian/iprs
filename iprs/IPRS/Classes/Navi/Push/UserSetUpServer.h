//
//  UserSetUpServer.h
//  RYTNewsPush
//
//  Created by HelloWorld on 16/3/11.
//  Copyright (c) 2016年 RYTong. All rights reserved.
//

#import <Foundation/Foundation.h>


/**
 *
 *self.apikey = @"ryt";
 *self.serverUrl = @"http://192.168.64.128:8077";
 *self.register_requestUrl = @"/apple/apple/register";
 *self.alias_requestUrl = @"/alias/set_v2";
 *self.feedback_requestUrl = @"/apple/feedback";
 *self.messageGetUrl = @"/message/get";
 *self.offlineMessage = @"/message/offline_message";
 */

@interface UserSetUpServer : NSObject
/**
 * 推送消息服务器
 * 收集到的用户信息,将会发送到此服务器.
 */
@property (nonatomic, copy)NSString *serverUrl;

/**
 * 推送消息服务器的发送注册应用接口,它将与serverUrl
 * 组合生成发送路径.
 */
@property (nonatomic, copy)NSString *register_requestUrl;

/**
 * 推送消息服务器的发送设置别名接口,它将与serverUrl
 * 组合生成发送路径.
 */
@property (nonatomic, copy)NSString *alias_requestUrl;

/**消息反馈
 * 推送消息服务器的发送消息反馈接口,它将与serverUrl
 * 组合生成发送路径.
 */
@property (nonatomic, copy)NSString *feedback_requestUrl;

/**下拉消息*/
@property (nonatomic, copy)NSString *messageGetUrl;

/**离线消息*/
@property(nonatomic , copy)NSString *offlineMessage;

/**
 * 设置盖应用程序的apikey
 */
@property (nonatomic, copy)NSString *apikey;

+ (UserSetUpServer *)sharedNetworkRequest;
@end
