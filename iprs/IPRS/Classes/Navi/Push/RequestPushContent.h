//
//  RequestPushContent.h
//  RYTNewsPush
//
//  Created by 郭瑞朋 on 16/5/13.
//  Copyright © 2016年 RYTong. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PLContent : NSObject

/**内容类型*/
@property(nonatomic,copy)NSString *ctype;
/**具体内容*/
@property(nonatomic,copy)NSString *cContent;

@end

@interface PayLoad : NSObject
/**通知栏展示的简短信息*/
@property (nonatomic, copy)NSString *alert;
/**消息类型(1:文本，2:富文本，3:链接，4:多媒体，其他后续扩展)*/
@property (nonatomic, assign)int type;
/**推送平台：all代表所有，ios代表只推送ios用户,ad代表只推送android用户*/
@property (nonatomic, copy)NSString *platform;
/**消息内容(主要用于点击之后应用内部操作)：
 type=1:{text:xxx}
 type=2:{rtf:xxx}
 type=3:{linkUrl:xxx}
 type=4:{
 data:xxx（base64编码之后的多媒体data数据流）,
*/
@property (nonatomic, retain)PLContent *content;
/**默认角标数字＋1；否则把角标数字改为指定的数字
 此参数暂未实现。目前全部显示1
*/
@property (nonatomic, assign)int badge;
/**应用自己添加的扩展字段*/
@property (nonatomic, retain)NSMutableDictionary *extras;
@end


@interface Options : NSObject

/** APNS是否为生产环境，false表示开发环境，默认为true */
@property (nonatomic, assign) BOOL apnsProduction;

/**消息过期时间（单位：秒）*/
@property (nonatomic, assign) NSNumber *expireTime;

/** 别名和设备的绑定类型，默认为2.
 1:一个别名和多个设备绑定时只给最近一次绑定的设备推送消息；
 2：一个别名和多个设备绑定时给全部绑定的设备推送消息
 */
@property (nonatomic, assign) NSNumber *aliasBindingType;

/** 消息优先级，广播默认优先级为1，单用户或多用户推送的默认优先级为2.
 1：优先级最低；
 2：优先级中等；
 3：优先级最高。
 */
@property (nonatomic, assign) NSNumber *priorityLevel;
@end


@interface RequestPushContent : NSObject

/**应用ak*/
@property (nonatomic, copy)NSString *ak;

/**指定需要推送的注册ID*/
@property (nonatomic, retain)NSMutableArray *regId;

/**指定需要推送的别名*/
@property (nonatomic, retain)NSMutableArray *alias;

/***/
@property (nonatomic, retain)PayLoad *payload;

/***/
@property (nonatomic, retain)Options *options;

/**@return RequestPushContent 单例对象*/
+ (RequestPushContent *)sharedRequestPushContent;
@end
