//
//  NewsPushDataBaseManage.h
//  RYTNewsPush
//
//  Created by 郭瑞朋 on 16/5/17.
//  Copyright © 2016年 RYTong. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>

@interface NewsPushDataBaseManage : NSObject{
    sqlite3             *database_;
    NSString            *databasePath_;
    BOOL                bFirstCreate_;
}
+ (NewsPushDataBaseManage *)sharedNewsPushDataBaseManage;

/**打开数据库*/
- (BOOL)open;

/**依据表名插入数据库*/
- (BOOL) insertData:(NSDictionary *)dataDict toTable:(NSString *)tableName;

/**依据表名和regId，更新数据库表中的alias*/
//- (BOOL) updateData:(NSString *)value toTable:(NSString *)tableName whererRegId:(NSString *)regId byKey:(NSString *)key;

/**查询数据库*/
- (NSMutableArray *)getDataListFromTable:(NSString *) tableName byApiKey:(NSString *)ak;

/**总共多少条数据*/
//- (int)getDataCount;
@end
