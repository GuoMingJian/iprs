//
//  NewsPush.h
//  RYTNewsPush
//
//  Created by HelloWorld on 16/1/22.
//  Copyright (c) 2016年 RYTong. All rights reserved.
//

#import <UIKit/UIKit.h>

#define APIKEY @"dev_test"

@interface NewsPush : NSObject

//@property (nonatomic, strong)NSDictionary *remoteNotification;

/**
 初始化,
 */
//+ (void)setupWithOption:(NSDictionary *)launchingOption;
/**
 注册APNS类型
 */
+ (void)registerForRemoteNotificationTypes:(NSUInteger)types
                                categories:(NSSet *)categories;
/**
 APP从苹果APNS获取device token后,需要将此token上送给推送服务器.本接口负责上传token到推送服务器.
 @param deviceToken 从苹果APNS获取device token值
 */
+ (void)registerDeviceToken:(NSData *)deviceToken;

/**
 设置当前用户的别名,并且此别名信息上送给服务器,由服务器保存.
 @para alias
 - nil 此次调用不设置此值
 - 空字符串(@"")表示取消之前的设置
 - 每次调用设置有效的别名,覆盖之前的设置
 - 有效的别名组成:字母(区分大小写),数字.下划线,汉字
 
 @para callbackSelector
 - nil 此次调用不需要 Callback
 - 用于回调返回对应的状态码:0为成功
 - 回调函数请参考SDK 实现
 - 例:-(void)callback:(int)iResCode{ }
 @para target 实现callbackSelector的实例对象.
 - nil 此次调用不需要Callback.
 */
+ (void)setAlias:(NSString *)alias callback:(SEL)callback object:(id)target;

/**消息反馈
 设置推送内容,将APP获取到的推送内容保存到推送服务中
 @para userInfo
 - 推送信息
 */
+ (void)handleRemoteNotification:(NSDictionary *)userInfo;

/**下拉消息，返回服务器*/
+ (void)getMessage:(NSDictionary *)info callback:(SEL)callback object:(id)target;

/**获取离线消息*/
+ (void)getOfflineMessagesWithCallback:(SEL)callback object:(id)target;

/**获取注册ID
 在从苹果服务器获取到token后,会通过这个token将推送服务器注册ID.
 */
+ (NSString *)getID;

@end
