//
//  SecuritySM4.m
//  firstapp
//
//  Created by 岑永宏 on 2018/6/5.
//  Copyright © 2018年 岑永宏. All rights reserved.
//

#import "SecuritySM4.h"
#import "NSData+LBXPadding.h"
#import "NSData+LBXSM4.h"
#import "sm2.h"
#define MP_OKAY       0
@implementation SecuritySM4
- (NSData *)securitySM4CBCEncryptWithSecretKey:(NSData *)secretKey src:(NSData *)src
{
    if (src == nil || [src length] == 0 || [secretKey length] == 0 || secretKey == nil) {
        return nil;
    }
    NSData *plainData = [src LBXPaddingWithMode:LBXPaddingMode_PKCS5 blockSize:16];
    NSData *cryptData = [plainData sm4WithOp:LBXOperaton_Encrypt optionMode:LBXOptionMode_CBC key:secretKey iv:secretKey];
    //    cryptData = [CRSecurityManager securityBase64Encode:cryptData];
    return cryptData;
}
- (NSData *)securitySM4CBCDecryptWithSecretKey:(NSData *)secretKey src:(NSData *)src
{
    if (src == nil || [src length] == 0 || [secretKey length] == 0 || secretKey == nil) {
        return nil;
    }
    //    src = [CRSecurityManager securityBase64Decode:src];
    NSData *decryptData = [src sm4WithOp:LBXOperaton_Decrypt optionMode:LBXOptionMode_CBC key:secretKey iv:secretKey];
    //解密后除去补位数据
    decryptData = [decryptData LBXUnPaddingWithMode:LBXPaddingMode_PKCS5];
    return decryptData;
}

#pragma mark - public methods
- (NSData *)securitySM2Encrypt:(NSString *)power key:(NSString *)key
{
    if ([power length] == 0 || [key length] == 0) {
        return nil;
    }
    
    
    NSString *powerSc=[NSString stringWithFormat:@"%02d%@",(int)power.length,power];
    NSData *src=[powerSc dataUsingEncoding:NSUTF8StringEncoding];
    
    unsigned char result[1024] = {0};
    unsigned long outlen = 1024;
    const char *encryptData = [src bytes];
    NSData *keyData =  [self dataFromHexString:key];
    
    int ret = GM_SM2Encrypt(result,&outlen,(unsigned char *)encryptData,src.length,(unsigned char *)keyData.bytes,keyData.length);
    
    if (outlen < 2 || ret != MP_OKAY) {
        //加密出错了
        return [NSData data];
    }
    NSData *data = [NSData dataWithBytes:result length:outlen];
    
    //    [self SM2DersectRectPower:[self securityHEXEncode:data]];
    
    
    return [self SM2DersectRectPower:[self securityHEXEncode:data]];
}
#pragma mark SM2加密der的处理，，具体处理要求 只满足聚合支付

-(NSData *)SM2DersectRectPower:(NSData *)Sdata{
    
    NSString *OlderStr=[[NSString alloc]initWithData:Sdata encoding:NSUTF8StringEncoding];
    
    //获取x1 y1 c2 c3 其中x1 x2 c3是64个字节
    NSString *x1=[OlderStr substringWithRange:NSMakeRange(2, 64)];
    NSString *y1=[OlderStr substringWithRange:NSMakeRange(66, 64)];
    NSString *C3=[OlderStr substringWithRange:NSMakeRange(OlderStr.length-64, 64)];
    NSString *C2=[OlderStr substringWithRange:NSMakeRange(130, OlderStr.length-194)];
    
    //开始转der格式
    
    NSString *der=[NSString stringWithFormat:@"02%02x%@02%02x%@04%02x%@04%02x%@",(int)(x1.length/2),x1,(int)(y1.length/2),y1,(int)(C3.length/2),C3,(int)(C2.length/2),C2];
    NSString *newDer=[NSString stringWithFormat:@"30%02x%@",(int)(der.length/2),der];
    
    NSLog(@"oldx1=%@,oldy1=%@,oldC3=%@,oldC2=%@,newDer=%@",x1,y1,C3,C2,newDer);
    return [newDer dataUsingEncoding:NSUTF8StringEncoding];
    
}

//#pragma mark 抑或运算
//// 加密
//- (NSData *)xor_encrypt:(NSString *)power addKey:(NSString *)key
//{
//    // 获取key的长度
//    NSInteger length = key.length;
//    
//    // 将OC字符串转换为C字符串
//    const char *keys = [key cStringUsingEncoding:NSASCIIStringEncoding];
//    
//    unsigned char cKey[length];
//    
//    memcpy(cKey, keys, length);
//    
//    // 数据初始化，空间未分配 配合使用 appendBytes
//    NSMutableData *encryptData = [[NSMutableData alloc] initWithCapacity:length];
//    
//    // 获取字节指针
//    const Byte *point = power.bytes;
//    
//    for (int i = 0; i < self.length; i++) {
//        int l = i % length;                     // 算出当前位置字节，要和密钥的异或运算的密钥字节
//        char c = cKey[l];
//        Byte b = (Byte) ((point[i]) ^ c);       // 异或运算
//        [encryptData appendBytes:&b length:1];  // 追加字节
//    }
//    return encryptData.copy;
//}




- (NSData *)securitySM2Decrypt:(NSData *)data key:(NSString *)key
{
    //密文长度至少也需要64+32位
    if ([data length] < 64 + 32 || [key length] == 0) {
        return nil;
    }
    unsigned char result[1024 * 8] = {0};
    data = [self securityHEXDecode:data];
    NSData *keyData =  [self dataFromHexString:key];
    unsigned long outlen = 1024;
    
    int ret = GM_SM2Decrypt((unsigned char *)result, &outlen, (unsigned char *)data.bytes, data.length, (unsigned char *)keyData.bytes, keyData.length);
    
    if (outlen == 0 || ret != MP_OKAY) {
        //解密出错了
        return [NSData data];
    }
    NSData *resultData = [NSData dataWithBytes:result length:outlen];
    return resultData;
}
-(NSData *)securityHEXEncode:(NSData *)src
{
    if(src == nil || [src length] == 0) {
        return nil;
    }
    
    Byte *bytes = (Byte *)[src bytes];
    NSString *result = @"";
    for (int i = 0; i < [src length]; i++) {
        if (i == 0) {
            result = [NSString stringWithFormat:@"%02X", bytes[i]];
        } else {
            result = [result stringByAppendingFormat:@"%02X", bytes[i]];
        }
    }
    
    return [result dataUsingEncoding:NSUTF8StringEncoding];
}
-(NSData *)securityHEXDecode:(NSData *)src
{
    if(src == nil || [src length] == 0) {
        return nil;
    }
    
    NSString *hexString = [[[NSString alloc] initWithData:src encoding:NSUTF8StringEncoding] lowercaseString];
    
    NSMutableData *data = [NSMutableData data];
    unsigned char wholeByte;
    char byteChars[3] = {'\0', '\0', '\0'};
    
    for (int i = 0; i < hexString.length / 2; i++) {
        byteChars[0] = [hexString characterAtIndex:i * 2];
        byteChars[1] = [hexString characterAtIndex:i * 2 + 1];
        wholeByte = strtol(byteChars, NULL, 16);
        [data appendBytes:&wholeByte length:1];
    }
    
    return data;
}

- (NSData *)dataFromHexString:(NSString*) hexString {
    if (hexString == nil || [hexString length] == 0) {
        return nil;
    }
    const char *chars = [hexString UTF8String];
    NSInteger i = 0, len = hexString.length;
    
    NSMutableData *data = [NSMutableData dataWithCapacity:len / 2];
    char byteChars[3] = {'\0','\0','\0'};
    unsigned long wholeByte;
    
    while (i < len) {
        byteChars[0] = chars[i++];
        byteChars[1] = chars[i++];
        wholeByte = strtoul(byteChars, NULL, 16);
        [data appendBytes:&wholeByte length:1];
    }
    
    return data;
}


-(NSString *)securityBytesToURLBase64:(NSData *)src
{
    if (src == nil || src.length == 0) {
        return @"";
    }
    
    NSString *base64 = [[NSString alloc] initWithData:[self securityBase64Encode:src] encoding:NSUTF8StringEncoding];
    
    NSString *plus = [base64 stringByReplacingOccurrencesOfString:@"+" withString:@"-"];
    NSString *splash = [plus stringByReplacingOccurrencesOfString:@"/" withString:@"_"];
    NSString *append = [splash stringByReplacingOccurrencesOfString:@"=" withString:@"."];
    
    return [NSString stringWithFormat:@"%@", append];
}

-(NSData *)securityURLBase64ToBytes:(NSString *)src
{
    if (src == nil || src.length == 0) {
        return nil;
    }
    
    NSString *plus = [src stringByReplacingOccurrencesOfString:@"-" withString:@"+"];
    NSString *splash = [plus stringByReplacingOccurrencesOfString:@"_" withString:@"/"];
    NSString *append = [splash stringByReplacingOccurrencesOfString:@"." withString:@"="];
    
    return [NSData dataWithData:[self securityBase64Decode:[append dataUsingEncoding:NSUTF8StringEncoding]]];
}

-(NSData *)securityBase64Encode:(NSData *)src
{
    if(src == nil || src.length == 0) {
        return nil;
    }
    
    unsigned int iTest;
    
    NSUInteger nSize = [src length];
    
    char *srcChar = (char *)[src bytes];
    char* pOutBuffer = malloc(nSize / 3 * 4 + 5);
    memset(pOutBuffer, 0x00, nSize / 3 * 4 + 5);
    
    for(int i = 0;i < nSize / 3; i++)
    {
        iTest = (unsigned char) *srcChar++;
        iTest = iTest << 8;
        
        iTest = iTest | (unsigned char) *srcChar++;
        iTest = iTest << 8;
        
        iTest = iTest | (unsigned char) *srcChar++;
        
        //以4 byte倒序写入输出缓冲
        pOutBuffer[3] = BASE64_ENCODE_TABLE[iTest & 0x3F];
        iTest = iTest >> 6;
        pOutBuffer[2] = BASE64_ENCODE_TABLE[iTest & 0x3F];
        iTest = iTest >> 6;
        pOutBuffer[1] = BASE64_ENCODE_TABLE[iTest & 0x3F];
        iTest = iTest >> 6;
        pOutBuffer[0] = BASE64_ENCODE_TABLE[iTest];
        pOutBuffer += 4;
    }
    
    //设置尾部
    switch (nSize % 3)
    {
        case 0:
            break;
        case 1:
            iTest = (unsigned char) *srcChar;
            iTest = iTest << 4;
            pOutBuffer[1] = BASE64_ENCODE_TABLE[iTest & 0x3F];
            iTest = iTest >> 6;
            pOutBuffer[0] = BASE64_ENCODE_TABLE[iTest];
            pOutBuffer[2] = '='; //用'='也就是64码填充剩余部分
            pOutBuffer[3] = '=';
            break;
        case 2:
            iTest = (unsigned char) *srcChar++;
            iTest = iTest << 8;
            iTest = iTest | (unsigned char) *srcChar;
            iTest = iTest << 2;
            pOutBuffer[2] = BASE64_ENCODE_TABLE[iTest & 0x3F];
            iTest = iTest >> 6;
            pOutBuffer[1] = BASE64_ENCODE_TABLE[iTest & 0x3F];
            iTest = iTest >> 6;
            pOutBuffer[0] = BASE64_ENCODE_TABLE[iTest];
            pOutBuffer[3] = '='; // Fill remaining byte.
            break;
    }
    pOutBuffer -= nSize / 3 * 4;
    NSData *data = [NSData dataWithBytes:pOutBuffer length:(NSUInteger)((nSize + 2) / 3 * 4)];
    free(pOutBuffer);
    
    return data;
}
-(NSData *)securityBase64Decode:(NSData *)src
{
    if(src == nil || [src length] == 0) {
        return nil;
    }
    
    char *srcChar = (char *)[src bytes];
    const int nSrcCount = (int)[src length];//(int)_tcslen(lpszSrc);
    int nSize = nSrcCount / 4 * 3;
    if(srcChar[nSrcCount - 1] == '=')
        nSize--;
    if(srcChar[nSrcCount - 2] == '=')
        nSize--;
    
    char* pOutBuffer = (char *)malloc(nSize + 3);
    memset(pOutBuffer, 0x00, nSize + 3);
    
    const char* pInBuffer = srcChar;
    int iTest,iPack=0;
    for(int i = 0; i < nSize / 3 ; i++)
    {
        for(int j = 0; j < 4; j++)
        {
            iTest = BASE64_DECODE_TABLE[*pInBuffer]; // Read from InputBuffer.
            pInBuffer++;
            //InPtr++;
            if (iTest == 0xFF)
            {
                j--;
                continue; //读到255非法字符
            }
            iPack = iPack << 6 ;
            iPack = iPack | iTest ;
        }
        pOutBuffer[2] = iPack;
        iPack = iPack >> 8;
        pOutBuffer[1] = iPack;
        iPack = iPack >> 8;
        pOutBuffer[0] = iPack;
        //准备写入后3位
        pOutBuffer+= 3;
        iPack = 0;
    }
    switch(nSize % 3)
    {
        case 1:
            iTest = BASE64_DECODE_TABLE[*pInBuffer]; // Read from InputBuffer.
            pInBuffer++;
            if (iTest != 0xFF)
            {
                iPack = iPack << 6 ;
                iPack = iPack | iTest ;
            }
            iTest = BASE64_DECODE_TABLE[*pInBuffer]; // Read from InputBuffer.
            if (iTest != 0xFF)
            {
                iPack = iPack << 6 ;
                iPack = iPack | iTest ;
            }
            iPack = iPack >> 4;
            pOutBuffer[0] = iPack;
            pOutBuffer++;
            break;
        case 2:
            iTest = BASE64_DECODE_TABLE[*pInBuffer]; // Read from InputBuffer.
            pInBuffer++;
            if (iTest != 0xFF)
            {
                iPack = iPack << 6 ;
                iPack = iPack | iTest ;
            }
            iTest = BASE64_DECODE_TABLE[*pInBuffer]; // Read from InputBuffer.
            pInBuffer++;
            if (iTest != 0xFF)
            {
                iPack = iPack << 6 ;
                iPack = iPack | iTest ;
            }
            iTest = BASE64_DECODE_TABLE[*pInBuffer]; // Read from InputBuffer.
            if (iTest != 0xFF)
            {
                iPack = iPack << 6 ;
                iPack = iPack | iTest ;
            }
            iPack = iPack >> 2;
            pOutBuffer[1] = iPack;
            iPack = iPack >> 8;
            pOutBuffer[0] = iPack;
            pOutBuffer+=2;
            break;
        default:
            break;
    }
    pOutBuffer -= nSize;
    pOutBuffer[nSize] = '\0';
    
    NSData *data = [NSData dataWithBytes:pOutBuffer length:(NSUInteger)nSize];
    free(pOutBuffer);
    
    return data;
}



@end
