//
//  CommentDefine.h
//  khqyKhqyIphone
//
//  Created by 林英伟 on 15/11/3.
//
//

#ifndef CommentDefine_h
#define CommentDefine_h
#import "IPRSMethod.h"
//判断是生产还是测试环境
#define kIsPro [IPRSMethod judgePro]
//百度地图key
#define kBaiduMapKit (kIsPro ? @"qS9dIEd7IDi1lZxoj8aWG6VNfHE9wdou" : @"3dbOpTg4I3udSpX6MlkhbIXI9tCmuas4")
//推送环境ip
#define kPushUrl (kIsPro ? @"https://8.95508.com/push" : @"https://218.13.4.194:8077")
//release打印
#ifdef DEBUG
#define NSLog(...) NSLog(@"%s %@", __PRETTY_FUNCTION__, [NSString stringWithFormat:__VA_ARGS__])
#else
#define NSLog(format, ...)
#endif
//状态栏高度
#define StatusBarHeight [UIApplication sharedApplication].statusBarFrame.size.height
//开热点
#define kHotSpot (StatusBarHeight > 20 ? YES : NO)
//导航栏高度
#define NaviHeight (44 + StatusBarHeight)
//屏幕宽高
#define ScreenWidth [[UIScreen mainScreen] bounds].size.width
#define ScreenHeight [[UIScreen mainScreen] bounds].size.height
//颜色
#define kColorWithRGB(r, g, b) [UIColor colorWithRed:(r) / 255.f green:(g) / 255.f blue:(b) / 255.f alpha:1.f]
#define kColorWithRGBAlpha(r, g, b, a) [UIColor colorWithRed:(r) / 255.f green:(g) / 255.f blue:(b) / 255.f alpha:(a)]
//图片
#define kIconImage(image)  [UIImage imageNamed:(image)]
//网络繁忙提示
#define ERRORTIPS @"服务器异常，请稍后再试！"
//获取版本号
#define SystemVersion [[UIDevice currentDevice] systemVersion]
//状态栏白色
#define ChangeBarWhite [[UIApplication sharedApplication]setStatusBarStyle:UIStatusBarStyleLightContent];
//状态栏黑色
#define ChangeBarBlack [[UIApplication sharedApplication]setStatusBarStyle:UIStatusBarStyleDefault];
//keyWindow
#define KeyWindow [UIApplication sharedApplication].keyWindow

//GCD - 一次性执行
#define ONCE(onceBlock) static dispatch_once_t onceToken; dispatch_once(&onceToken, onceBlock);
//G－C－D
#define BACK(block) dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), block);
#define MAIN(block) dispatch_async(dispatch_get_main_queue(),block);
// 不堵塞线程并在主线程的延迟执行 timer:延迟时间，单位：秒；与主线程同步
#define MainDelay(timer,block) dispatch_after(dispatch_time(DISPATCH_TIME_NOW, INT64_C(timer) * NSEC_PER_SEC), dispatch_get_main_queue(), block);

/** 获取沙盒 Document 路径*/
#define kDocumentPath  [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject]

/** 字符串是否为空*/
#define kStringIsEmpty(str)     ([str isKindOfClass:[NSNull class]] || str == nil || [str length] < 1 ? YES : NO )
/** 数组是否为空*/
#define kArrayIsEmpty(array)    (array == nil || [array isKindOfClass:[NSNull class]] || array.count == 0)
/** 字典是否为空*/
#define kDictIsEmpty(dic)       (dic == nil || [dic isKindOfClass:[NSNull class]] || dic.allKeys == 0)
/** 是否是空对象*/
#define kObjectIsEmpty(_object) (_object == nil \
|| [_object isKindOfClass:[NSNull class]] \
|| ([_object respondsToSelector:@selector(length)] && [(NSData *)_object length] == 0) \
|| ([_object respondsToSelector:@selector(count)] && [(NSArray *)_object count] == 0))

/** 滚动视图适配*/
#define adjustsScrollViewInsets(scrollView)\
do {\
_Pragma("clang diagnostic push")\
_Pragma("clang diagnostic ignored \"-Warc-performSelector-leaks\"")\
if ([scrollView respondsToSelector:NSSelectorFromString(@"setContentInsetAdjustmentBehavior:")]) {\
NSMethodSignature *signature = [UIScrollView instanceMethodSignatureForSelector:@selector(setContentInsetAdjustmentBehavior:)];\
NSInvocation *invocation = [NSInvocation invocationWithMethodSignature:signature];\
NSInteger argument = 2;\
invocation.target = scrollView;\
invocation.selector = @selector(setContentInsetAdjustmentBehavior:);\
[invocation setArgument:&argument atIndex:2];\
[invocation retainArguments];\
[invocation invoke];\
}\
_Pragma("clang diagnostic pop")\
} while (0)

/** 是否ios7*/
#define IOS7 ([[[UIDevice currentDevice] systemVersion] doubleValue] >= 7.0 ? YES : NO)
/** 是否ios8*/
#define IOS8 ([[[UIDevice currentDevice] systemVersion] doubleValue] >= 8.0 ? YES : NO)
/** 是否ios11*/
#define IOS11 ([[[UIDevice currentDevice] systemVersion] doubleValue] >= 11.0 ? YES : NO)
//iphone x
#define RealX ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1125, 2436), [[UIScreen mainScreen] currentMode].size) : NO)
//根控制器
#define kRootVc [[UIApplication sharedApplication] keyWindow].rootViewController
#endif /* CommentDefine_h */
