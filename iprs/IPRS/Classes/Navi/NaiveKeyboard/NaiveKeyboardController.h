//
//  NaiveKeyboardController.h
//  mobilePlatformMobilePlatformIphone
//
//  Created by huangjundong on 14-12-9.
//
//

#import <Foundation/Foundation.h>
@interface KeyboardView : UIView{
    int                 NumberArr_[10];
    UIView              *NumberPadKeyboard_;
    
    int                 CharacterArr_[26];
    UIView              *ASCIICapableKeyboard_;
    
    int                 SymbolArr_[32];
    UIView              *SymbolPadKeyboard_;
    
    UIView              *tipView_;
    
    int                 maxLenth;
    BOOL                isShiftOn_;
    BOOL                isSpeicalPassWord_;
    BOOL                isLogin_;
    //判断是否正序排列
    BOOL                isNormalSort_;
    //当前的键盘类型，0代表的是数字键盘，1代表的是字母键盘，2代表的是符号键盘
    int                 currentKeyboardType_;
    
    UIImageView         *backgroundImageView_;
    
//    id                  delegate_;
    
    NSString            *value_;
    
    NSMutableArray      *randomKey_;
    
    BOOL                isNumOnly_;
    
    BOOL                isNumAndChar_;
    
    BOOL                isNumber_;//纯数字输入
}

@property (nonatomic, retain) UIView *NumberPadKeyboard;
@property (nonatomic, retain) UIView *ASCIICapableKeyboard;
@property (nonatomic, retain) UIView *SymbolPadKeyboard;
@property (nonatomic, retain) UIView *tipView;
@property (nonatomic, assign) int maxLenth;
@property (nonatomic, retain) UILabel *passWordLabel;
@property (nonatomic, assign) int currentCharCount;
@property (nonatomic, assign) BOOL isSpeicalPassWord_;
@property (nonatomic, assign) BOOL isLogin_;
@property (nonatomic, assign) id delegate;
@property (nonatomic, retain) NSMutableArray *randomKey;
@property (nonatomic, assign) BOOL isNumOnly;
@property (nonatomic, assign) BOOL isNumAndChar;
@property (nonatomic, assign) BOOL isNumber;
@property (nonatomic,copy) void (^pressValueBlock)(NSString *str);
//@property (nonatomic,weak) id<KeyboardViewDelegate *> *delegate;

-(UIView *)initWithFrame:(CGRect)rect;
-(void)hideKeyboard;
-(void)cleanValue;
- (void)setOrigin:(CGPoint)origin;
- (void) createAndShowNumberPadKeyboardWithSwitchButtonStatus:(BOOL)status;//refresh

@end

@protocol KeyboardViewDelegate <NSObject>
@required
-(void)keyboardChange:(NSString *)value;
//zzm
@end

@interface NaiveKeyboardController : NSObject{
    id  <KeyboardViewDelegate>delegate_;
}

@property(nonatomic, assign)KeyboardView *keyboardView;

+(void)setKeyboardIndex:(UIWindow*)currentWindow;

+ (void)initKeyboard:(UIWindow *)window;

+ (void)showKeyboard:(int)maxlength numType:(BOOL)flag andIsNumber:(BOOL)numFlag;
+ (void)showKeyboard:(int)maxlength numAndCharType:(BOOL)flag andIsNumber:(BOOL)numFlag;

+ (void)hideKeyboard;


+ (NSMutableArray *)getRanomKey;
+ (NSString *)deciphering:(NSString *)src;
+ (NSData *) doEncyrptValue:(NSString *)body mode:(NSString *) encryptmode accounts:(NSString*) account;
+ (KeyboardView *)getKeyboardView;
@end



