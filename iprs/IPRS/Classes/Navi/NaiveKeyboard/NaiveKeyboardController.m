//
//  NaiveKeyboardController.m
//  mobilePlatformMobilePlatformIphone
//
//  Created by huangjundong on 14-12-9.
//
//

#import "NaiveKeyboardController.h"
#import "hsmcliCGB.h"
#define ZERO_BUTTON_TIME 0.2
#define SYMBOLS @"~`!@#$%^&*()_-+={}[]|\\:;\"'<,>.?/"
#define CHARACTERS @"abcdefghijklmnopqrstuvwxyz"
#define CHARACTERS_C @"ABCDEFGHIJKLMNOPQRSTUVWXYZ"
#define NUMBER @"0123456789"

//#define KEYBOARDBACKGROUND @"c_keyboardBackground.png"
//#define KEYBOARDCANCELTITLE @"keyboardcanceltitle.png"
#define kXHeight 10
#define CancelBtnHeight 30
#define KEYBOARD_DEFAULT_FONT_SIZE 25

#define TOP_MARGIN_CHA 30
#define LEFT_MARGIN_CHA 2.0
#define V_SPACE 2
#define H_SPACE 4

#define KeyboardHeight (ScreenHeight * .32 > 181 ? ScreenHeight * .32 : 181)
#define NUMKEYBOARD_WEIGHT 105
#define NUMKEYBOARD_HEIGHT 40
#define CHAKEYBOARD_WEIGHT 31
#define CHAKEYBOARD_HEIGHT 41
#define SYMBOLKEYBOARD_WEIGHT 31
#define SYMBOLKEYBOARD_HEIGHT 41
#define KEYBOARD_BAR_HEIGHT 13

#define RANDOM_NUMBER @"number"
#define RANDOM_CHARACTE @"character"
#define RANDOM_SYMBOL @"symbol"
#define RANDMO_NONE @"none"

//FinanceBank.h 关于版本号 设定超时时间
#define TARGERVERSION 7.0
#define MAX_TIME 300
#define MIN_TIME 60
//MainUserGuide
#define STATUS_BAR_HEIGHT 20.0


typedef NS_ENUM(NSInteger, KeyboardType){
    KeyboardNumberType,
    KeyboardCharacterType,
    KeyboardSymbolType,
    KeyboardSwitchType
};

@implementation NaiveKeyboardController

@synthesize keyboardView = keyboardView_;

static KeyboardView *keyboardView_ = nil;

+(void)setKeyboardIndex:(UIWindow*)currentWindow{
    MAIN(^{
        if (keyboardView_) {
            [currentWindow bringSubviewToFront:keyboardView_];
        }
    })
}

+ (void)initKeyboard:(UIWindow *)window{
    if (keyboardView_ == nil){
        NSInteger kHeight = 0;
        if(RealX){
            kHeight = 10;
        }
        keyboardView_ = [[KeyboardView alloc] initWithFrame:CGRectMake(0.0, ScreenHeight, ScreenWidth, KeyboardHeight + CancelBtnHeight + kHeight)];
        keyboardView_.tag = 9090;
        keyboardView_.delegate = self;
        [window addSubview:keyboardView_];
        
        for (int i = 0; i<PIN_MAX_LENGTH; i++)
        {
            NSTimeInterval interval = [[NSDate date] timeIntervalSince1970];
            NSNumber *number = [[NSNumber alloc] initWithDouble:interval];
            int timeValue = [number intValue];
            int _random = arc4random() % (2147483647 - timeValue);
            int ch = (timeValue + _random) % 80;
            if (ch < 2){
                ch = 2-ch;
            }
            [keyboardView_.randomKey addObject:[NSNumber numberWithInt:ch]];
            
            //            [number release];
        }
        return;
    }
}

+ (void)showKeyboard:(int)maxlength numType:(BOOL)flag andIsNumber:(BOOL)numFlag
{
    [keyboardView_ cleanValue];
    keyboardView_.isNumOnly = flag;
    keyboardView_.isNumAndChar = NO;
    keyboardView_.maxLenth = maxlength > 0 ? maxlength : 8;
    keyboardView_.isNumber = numFlag;
    [keyboardView_ createAndShowNumberPadKeyboardWithSwitchButtonStatus:YES];
    
    [UIView animateWithDuration:.4 animations:^{
        CGPoint origin = CGPointZero;
        NSInteger kHeight = 0;
        if(RealX){
            kHeight = 10;
        }
        origin = CGPointMake(0.0, ScreenHeight - keyboardView_.frame.size.height - kHeight);
        [keyboardView_ setOrigin:origin];
    }];
}

+ (void)showKeyboard:(int)maxlength numAndCharType:(BOOL)flag andIsNumber:(BOOL)numFlag
{
    [keyboardView_ cleanValue];
    keyboardView_.isNumAndChar = flag;
    keyboardView_.maxLenth = maxlength > 0 ? maxlength : 8;
    keyboardView_.isNumber = numFlag;
    [keyboardView_ createAndShowNumberPadKeyboardWithSwitchButtonStatus:YES];
    
    [UIView animateWithDuration:.4 animations:^{
        CGPoint origin = CGPointZero;
        NSInteger kHeight = 0;
        if(RealX){
            kHeight = 10;
        }
        origin = CGPointMake(0.0, ScreenHeight - keyboardView_.frame.size.height - kHeight);
        [keyboardView_ setOrigin:origin];
    }];
    
}

+ (void)hideKeyboard{
    [UIView animateWithDuration:.4 animations:^{
        CGPoint origin = CGPointZero;
        NSInteger kHeight = 0;
        if(RealX){
            kHeight = 10;
        }
        origin = CGPointMake(0.0, ScreenHeight - kHeight);
        [keyboardView_ setOrigin:origin];
        [keyboardView_ hideKeyboard];
    }];
}

+ (NSMutableArray *)getRanomKey
{
    return keyboardView_.randomKey;
}

+ (KeyboardView *)getKeyboardView{
    return keyboardView_;
}
//判断简单密码复杂度
//+(void)returnEncyrp:(NSString*)value
//{
//    NSString *str = nil;
//    NSRange range = [value rangeOfString:@"05"];
//    NSRange range1 = [value rangeOfString:@"04"];
//    //不包含
//    if (range.location == NSNotFound && range1.location == NSNotFound)
//    {
//        str = @"0";
//    }
//    else
//    {
//        str = @"1";
//    }
//    [[WL sharedInstance] sendActionToJS:@"password" withData:@{@"value":str}];
//
//}


//** 根据不同加密模式加密密码
+ (NSData *) doEncyrptValue:(NSString *)body mode:(NSString *) encryptmode accounts:(NSString*) account {
    NSString *keyPath = [[NSBundle mainBundle]pathForResource:@"passwordkey" ofType:@"plist"];
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithContentsOfFile:keyPath];
    NSString *key = dict[@"sit"];
    if(kIsPro){
       key = dict[@"pro"];
    }
    NSString *value = body;
    Boolean needEncryptWitAes = FALSE;
    if ([encryptmode isEqualToString:@"01"]) {
        needEncryptWitAes = TRUE;
    } else if ([encryptmode isEqualToString:@"A0"]) {
        value = [hsmcliCGB PkEncryptAPin:body crePublickeyData:key accounts:account];
    } else if ([encryptmode isEqualToString:@"A1"]) {
        value = [hsmcliCGB PkEncryptAPin:body crePublickeyData:key accounts:account];
        needEncryptWitAes = TRUE;
    } else if ([encryptmode isEqualToString:@"E0"]) {
        value = [hsmcliCGB PkEncryptEPin:body crePublickeyData:key accounts:account];
    } else if ([encryptmode isEqualToString:@"E1"]) {
        value = [hsmcliCGB PkEncryptEPin:body crePublickeyData:key accounts:account];
        needEncryptWitAes = TRUE;
    } else if ([encryptmode isEqualToString:@"AE0"]) {
        NSMutableString *temstr = [[NSMutableString alloc] init];
        [temstr appendFormat:@"%@", [hsmcliCGB PkEncryptAPin:body crePublickeyData:key accounts:account]];
        [temstr appendFormat:@"%@", [hsmcliCGB PkEncryptEPin:body crePublickeyData:key accounts:account]];
        value = [NSString stringWithString:temstr];
        //        [temstr release];
    } else if ([encryptmode isEqualToString:@"AE1"]) {
        NSMutableString *temstr = [[NSMutableString alloc] init];
        [temstr appendFormat:@"%@", [hsmcliCGB PkEncryptAPin:body crePublickeyData:key accounts:account]];
        [temstr appendFormat:@"%@", [hsmcliCGB PkEncryptEPin:body crePublickeyData:key accounts:account]];
        value = [NSString stringWithString:temstr];
        //        [temstr release];
    }
    //简单密码
    //[self returnEncyrp:value];
    NSData *data = [value dataUsingEncoding:NSUTF8StringEncoding];
    return data;
}

+ (NSString *)deciphering:(NSString *)src{
    
    NSString *unicodeString = @"";
    
    for (int i =0; i<src.length; i++) {
        int key = [[[NaiveKeyboardController getRanomKey] objectAtIndex:i]integerValue];
        NSString *temp = [src substringWithRange:NSMakeRange(i, 1)];
        NSString *type = @"";
        if ([SYMBOLS rangeOfString:temp].length > 0) {
            type = SYMBOLS;
        }else if ([CHARACTERS rangeOfString:temp].length>0){
            type = CHARACTERS;
        }else if ([CHARACTERS_C rangeOfString:temp].length>0){
            type = CHARACTERS_C;
        }else{
            type = NUMBER;
        }
        int index = key%type.length - [type rangeOfString:temp].location+1 <= 0 ? [type rangeOfString:temp].location - key%type.length : ([type rangeOfString:temp].location + type.length - key%type.length)%type.length  ;
        NSString *value = [type substringWithRange:NSMakeRange(index, 1)];
        unicodeString = [unicodeString stringByAppendingString:[NSString stringWithFormat:@"%@",value]];
    }
    return unicodeString;
}

@end

@interface KeyboardView ()
@property(nonatomic,copy)NSString *back_N;
@property(nonatomic,copy)NSString *back_S;
@property(nonatomic) KeyboardType kType;
@end


@implementation KeyboardView

@synthesize NumberPadKeyboard = NumberPadKeyboard_;
@synthesize ASCIICapableKeyboard = ASCIICapableKeyboard_;
@synthesize SymbolPadKeyboard = SymbolPadKeyboard_;
@synthesize tipView = tipView_;
@synthesize maxLenth;
@synthesize passWordLabel;
@synthesize currentCharCount;
@synthesize isSpeicalPassWord_;
@synthesize isLogin_;
@synthesize delegate = delegate_;
@synthesize randomKey = randomKey_;
@synthesize isNumOnly = isNumOnly_;
@synthesize isNumAndChar = isNumAndChar_;
@synthesize isNumber = isNumber_;


-(UIView *)initWithFrame:(CGRect)rect{
    if (self = [super initWithFrame:rect]) {
        //初始化属性
        isShiftOn_ = NO;
        isNormalSort_ = YES;
        currentKeyboardType_ = 0;
        maxLenth = 20;
        value_ = @"";
        randomKey_ = [[NSMutableArray alloc]init];
#pragma mark-//////////键盘
        //创建cancel bar
        UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, CancelBtnHeight)];
        view.backgroundColor = kColorWithRGB(224, 224, 224);
        //        UIImage *cancelBarImage = [UIImage imageNamed:KEYBOARDCANCELTITLE];
        UILabel *texLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 5, 200, 20)];
        texLabel.text = @"正在使用广发银行密码键盘";
        texLabel.textAlignment = NSTextAlignmentLeft;
        texLabel.font = [self getSwitchFont:12.0f];
        texLabel.textColor = [self getBuleColor];
        [view addSubview:texLabel];
        
        UIButton *cancelBarButton = [UIButton buttonWithType:UIButtonTypeCustom];
        cancelBarButton.frame = view.bounds;
        cancelBarButton.tag = 200;
        [cancelBarButton addTarget:self action:@selector(hideKeyboard:) forControlEvents:UIControlEventTouchUpInside];
        [view addSubview:cancelBarButton];
        
        [self addSubview:view];
        //创建背景
        //        UIImage *backgroundImage = [UIImage imageNamed:KEYBOARDBACKGROUND];
        backgroundImageView_ = [[UIImageView alloc]init];
        backgroundImageView_.backgroundColor = [UIColor whiteColor];
        NSInteger kHeight = 0;
        if(RealX){
            kHeight = 20;
        }
        backgroundImageView_.frame = CGRectMake(0, view.frame.size.height, ScreenWidth, KeyboardHeight + kHeight);
        [self addSubview:backgroundImageView_];
        [self bringSubviewToFront:backgroundImageView_];
        
        //创建键盘view
        UIView *numView = [[UIView alloc] initWithFrame:backgroundImageView_.frame];
        numView.backgroundColor = [UIColor clearColor];
        self.NumberPadKeyboard = numView;
        //        [numView release];
        
        UIView *chaView = [[UIView alloc] initWithFrame:backgroundImageView_.frame];
        chaView.backgroundColor = [UIColor clearColor];
        self.ASCIICapableKeyboard = chaView;
        //        [chaView release];
        
        UIView *symbolView = [[UIView alloc] initWithFrame:backgroundImageView_.frame];
        symbolView.backgroundColor = [UIColor clearColor];
        self.SymbolPadKeyboard = symbolView;
        //        [symbolView release];
        
        UIView *tipView = [[UIView alloc]initWithFrame:backgroundImageView_.frame];
        [self addSubview:tipView];
        tipView.backgroundColor = [UIColor clearColor];
        tipView.hidden = YES;
        self.tipView = tipView;
        //        [tipView release];
    }
    
    [self createAndShowNumberPadKeyboardWithSwitchButtonStatus:YES];
    
    return self;
}

-(void)cleanValue{
    value_ = @"";
//    if (_pressValueBlock) {
//        _pressValueBlock(@"");
//    }
//    [[WL sharedInstance]sendActionToJS:@"keyboardChange" withData:@{@"value": value_}];
}

- (UIFont *)getFont:(int)size {
    return [UIFont fontWithName:@"Helvetica-Light" size:18.f];
    //@"KohinoorTelugu-Light"
}

-(UIFont *)getSwitchFont:(int)size{
    return [UIFont fontWithName:@"Helvetica-Light" size:size];
}

- (UIColor *)getBuleColor {
    return [UIColor blackColor];
    //    return  [checkEveryThings hexStringToColor:@"#3f3f3f"];
}

- (void) createNumberNormalOrder{
    int numberOriginalOrder[10] = {0,1,2,3,4,5,6,7,8,9};
    int count = 10;
    
    NSMutableArray *arr = [NSMutableArray array];
    for (int i = 0; i < count; i++) {
        [arr addObject:[NSString stringWithFormat:@"%d",numberOriginalOrder[i]]];
    }
    
    NSMutableArray *arr2 = [NSMutableArray array];
    int count2 = count;
    for (int i = 0; i < count2; i++) {
        int rangdomNum = arc4random() % count;
        [arr2 addObject:arr[rangdomNum]];
        int x = [arr[rangdomNum] intValue];
        numberOriginalOrder[i] = x;
        [arr removeObjectAtIndex:rangdomNum];
        count--;
    }
    
    int tempIndex = 0;
    int chosenNumber = 0;
    for (int i = 0; i < 10; i ++) {
        tempIndex =  i ;
        chosenNumber = numberOriginalOrder[tempIndex];
        NumberArr_[i] = chosenNumber;
    }
}

- (void) createCharacterNormalOrder{
    //0a,1b,2c,3d,4e,5f,6g,7h,8i,9j,10k,11l,12m,13n,14o,15p,16q,17r,18s,19t,20u,21v,22w,23x,24y,25z
    //正序排列，根据平常使用的键盘排列和a~z的字母顺序加入到数组
    int characterOriginalOrder[26] = {16,22,4,17,19,24,20,8,14,15,0,18,3,5,6,7,9,10,11,25,23,2,21,1,13,12};
    
    int count = 26;
    
    NSMutableArray *arr = [NSMutableArray array];
    for (int i = 0; i < count; i++) {
        [arr addObject:[NSString stringWithFormat:@"%d",characterOriginalOrder[i]]];
    }
    
    //    NSMutableArray *arr2 = [NSMutableArray array];
    //    int count2 = count;
    //    for (int i = 0; i < count2; i++) {
    //        int rangdomNum = arc4random() % count;
    //        [arr2 addObject:arr[rangdomNum]];
    //        int x = [arr[rangdomNum] intValue];
    //        characterOriginalOrder[i] = x;
    //        [arr removeObjectAtIndex:rangdomNum];
    //        count--;
    //    }
    
    int tempIndex = 0;
    int chosenNumber = 0;
    for (int i = 0; i < 26; i ++) {
        tempIndex =  i;
        chosenNumber = characterOriginalOrder[tempIndex];
        CharacterArr_[i] = chosenNumber;
    }
}

- (void) createSymbolNormalOrder
{
    int characterOriginalOrder[32] = {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31};
    
    int count = 32;
    
    NSMutableArray *arr = [NSMutableArray array];
    for (int i = 0; i < count; i++) {
        [arr addObject:[NSString stringWithFormat:@"%d",characterOriginalOrder[i]]];
    }
    
    NSMutableArray *arr2 = [NSMutableArray array];
    int count2 = count;
    for (int i = 0; i < count2; i++) {
        int rangdomNum = arc4random() % count;
        [arr2 addObject:arr[rangdomNum]];
        int x = [arr[rangdomNum] intValue];
        characterOriginalOrder[i] = x;
        [arr removeObjectAtIndex:rangdomNum];
        count--;
    }
    
    
    int tempIndex = 0;
    int chosenNumber = 0;
    for (int i = 0; i < 32; i ++) {
        tempIndex =  i;
        chosenNumber = characterOriginalOrder[tempIndex];
        SymbolArr_[i] = chosenNumber;
    }
}

- (void) createDeletelButtonToView:(UIView *)view frame:(CGRect)rect type:(NSString *)type {
    NSString *backN;
    //    NSString *backS;
    if (isLogin_ && ([type isEqualToString:@"charcter"] || [type isEqualToString:@"number"])) {
        backN = [NSString stringWithFormat:@"login_c_%@_keyboardDeleteButton.png", type];
        //        backS = [NSString stringWithFormat:@"login_c_%@_keyboardDeleteButtonSel.png", type];
    }else{
        backN = [NSString stringWithFormat:@"c_%@_keyboardDeleteButton.png", type];
        //        backS = [NSString stringWithFormat:@"c_%@_keyboardDeleteButtonSel.png", type];
    }
    
    UIButton *cBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    cBtn.frame = rect;
    UIImage *backImgN = [UIImage imageNamed:backN];
    cBtn.layer.masksToBounds = YES;
    cBtn.layer.cornerRadius = 5.f;
    cBtn.layer.borderWidth = .5f;
    cBtn.layer.borderColor = kColorWithRGB(204, 204, 204).CGColor;
    cBtn.adjustsImageWhenHighlighted = NO;
    [cBtn setBackgroundImage:backImgN forState:UIControlStateNormal];
    [cBtn addTarget:self action:@selector(deleteButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:cBtn];
}

- (void) createSwitchButtonToView:(UIView *)view frame:(CGRect)rect type:(NSString *)type curType:(NSString *)curType
{
    int tag = 0;
    NSString *text = nil;
    
    int fontSize = 15;
    if ([type isEqualToString:RANDOM_CHARACTE]) {
        tag = 0;
        text = @"ABC";
    } else if ([type isEqualToString:RANDOM_SYMBOL]) {
        tag = 1;
        text = @"符";
    } else if ([type isEqualToString:RANDOM_NUMBER]) {
        tag = 2;
        text = @"123";
        fontSize = 15;
    } else if ([type isEqualToString:RANDMO_NONE]){
        tag = 3;
        text = @" ";
        fontSize = 15;
    }
    
    NSString *backN = [NSString stringWithFormat:@"c_%@_keyboardSwitchButton.png", curType];
    //    NSString *backS = [NSString stringWithFormat:@"c_%@_keyboardSwitchButtonSel.png", curType];
    self.back_N = backN;
    //    self.back_S = backS;
    //Switch键
    UIButton *switchBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    switchBtn.tag = tag;
    switchBtn.frame = rect;
    
    UIImage *backImgN = [UIImage imageNamed:backN];
    //    UIImage *backImgS = [UIImage imageNamed:backS];
    //    [switchBtn setBackgroundImage:backImgS forState:UIControlStateHighlighted];
    [switchBtn setBackgroundImage:backImgN forState:UIControlStateNormal];
    [switchBtn setTitleColor:[self getBuleColor] forState:UIControlStateNormal];
    [switchBtn setTitle:text forState:UIControlStateNormal];
    switchBtn.layer.masksToBounds = YES;
    switchBtn.layer.cornerRadius = 5.f;
    switchBtn.layer.borderWidth = .5f;
    switchBtn.layer.borderColor = kColorWithRGB(204, 204, 204).CGColor;
    switchBtn.adjustsImageWhenHighlighted = NO;
    [switchBtn addTarget:self action:@selector(switchButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    [switchBtn.titleLabel setFont:[self getSwitchFont:fontSize]];
    [view addSubview:switchBtn];
    
    if (3 == tag)
    {
        [switchBtn setEnabled:NO];
    }
}


//登陆按钮
//- (void)createLoginButtonToView:(UIView *)view frame:(CGRect)rect type:(NSString *)type{
//    //
//    //    NSString *backN = [NSString stringWithFormat:@"login_c_%@_keyboardLoginButton.png", type];
//    //    NSString *backS = [NSString stringWithFormat:@"login_c_%@_keyboardLoginButtonSel.png", type];
//
//    NSString *backN = [NSString stringWithFormat:@"login_c_%@_keyboardLoginButton.png", type];
//    //    NSString *backS = [NSString stringWithFormat:@"login_c_%@_keyboardLoginButtonSel.png", type];
//
//    UIButton *loginBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//    loginBtn.frame = rect;
//    UIImage *backImgN = [UIImage imageNamed:backN];
//    [loginBtn setBackgroundImage:backImgN forState:UIControlStateNormal];
//    //    UIImage *backImgS = [UIImage imageNamed:backS];
//    //    [loginBtn setBackgroundImage:backImgS forState:UIControlStateHighlighted];
//    [loginBtn setTitle:@"登录" forState:UIControlStateNormal];
//    [loginBtn.titleLabel setFont:[self getFont:15]];
//    [loginBtn addTarget:self action:@selector(loginButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
//    loginBtn.adjustsImageWhenHighlighted = NO;
//    [view addSubview:loginBtn];
//}


//-(void)createNoneButtonToView
//{
//
//}

- (void) createAndShowNumberPadKeyboardWithSwitchButtonStatus:(BOOL)status{
    if (isNormalSort_){
        [self createNumberNormalOrder];
    }else {
        //        [self createNumberRandomOrder];
    }
    [[ASCIICapableKeyboard_ subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
    [[SymbolPadKeyboard_ subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
    //清空数字键盘上的控件
    [[NumberPadKeyboard_ subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    //按钮数
    NSInteger count = 10;
    //横间距
    CGFloat widthSpace = 5.f;
    //纵间距
    CGFloat heightSpace = 5.f;
    //按钮宽
    CGFloat buttonWidth = (ScreenWidth - 20) / 3;
    //按钮高
    CGFloat buttonHeight = (KeyboardHeight - 25) / 4;
    //第一个按钮的X坐标
    CGFloat startX = 5.f;
    //第一个按钮的Y坐标
    CGFloat startY = 5.f;
    //最后一个按钮的y轴
    CGFloat lastBtnY = 0.f;
    //最后一个按钮的x轴
    CGFloat lastBtnX = 0.f;
    for (int i = 0; i < count; i++) {
        NSInteger index = i % 3;
        NSInteger page = i / 3;
        
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        if (i == count - 1) {
            lastBtnY = page*(buttonHeight+heightSpace) + startY;
            lastBtnX = (index + 1)*(buttonWidth+widthSpace)+startX;
            btn.frame = CGRectMake(lastBtnX,lastBtnY,buttonWidth,buttonHeight);
        }else{
            btn.frame = CGRectMake(index*(buttonWidth+widthSpace)+startX,page*(buttonHeight+heightSpace) + startY,buttonWidth,buttonHeight);
        }
        [btn setBackgroundColor:[UIColor whiteColor]];
        btn.layer.masksToBounds = YES;
        btn.layer.borderWidth = .5f;
        btn.layer.borderColor = kColorWithRGBAlpha(204, 204, 204, 1).CGColor;
        btn.layer.cornerRadius = 5.f;
        [btn.titleLabel setFont:[self getFont:KEYBOARD_DEFAULT_FONT_SIZE]];
        [btn setTitleColor:[self getBuleColor] forState:UIControlStateNormal];
        [btn setTitle:[NSString stringWithFormat:@"%d",NumberArr_[i]] forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(numberButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        btn.tag = NumberArr_[i];
        [NumberPadKeyboard_ addSubview:btn];
    }
    if (isNumber_){
        //纯数字输入键盘
    }else{
        //符号切换键
        CGFloat btnWidth = (buttonWidth - startX) * .5;
        [self createSwitchButtonToView:NumberPadKeyboard_ frame:CGRectMake(startX, lastBtnY, btnWidth, buttonHeight) type:RANDOM_SYMBOL curType:RANDOM_NUMBER];
        
        //字母切换键
        [self createSwitchButtonToView:NumberPadKeyboard_ frame:CGRectMake(startX * 2 + btnWidth, lastBtnY, btnWidth, buttonHeight) type:RANDOM_CHARACTE curType:RANDOM_NUMBER];
    }
    
    //添加登陆按钮
    if (isLogin_) {
        //删除键
    }else{
        //删除键
        [self createDeletelButtonToView:NumberPadKeyboard_ frame:CGRectMake(lastBtnX + buttonWidth + startX, lastBtnY, buttonWidth, buttonHeight) type:RANDOM_NUMBER];
    }
    
    [self addSubview:NumberPadKeyboard_];
    
    //    [pool drain];
    
}

- (void)createAndShowASCIICapableKeyboardWithSwitchButtonStatus:(BOOL)status
{
    //重置shiftOn状态
    isShiftOn_ = NO;
    if (isNormalSort_) {
        [self createCharacterNormalOrder];
    }else {
        //        [self createCharacterRandomOrder];
    }
    //清空数字键盘上的控件
    [[ASCIICapableKeyboard_ subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    static NSString *characters = @"abcdefghijklmnopqrstuvwxyz";
    //按钮数
    CGFloat count = 26;
    //第一个按钮的X坐标
    CGFloat startX = 2.f;
    //第一个按钮的Y坐标
    CGFloat startY = 5.f;
    //横间距
    CGFloat widthSpace = 2.f;
    //纵间距
    CGFloat heightSpace = 5.f;
    //按钮宽
    CGFloat buttonWidth = (ScreenWidth - widthSpace * 11) / 10;
    //按钮高
    CGFloat buttonHeight = (KeyboardHeight - 25) / 4;
    //shift键宽
    CGFloat shiftBtnW = (ScreenWidth - buttonWidth * 7 - widthSpace * 10) * .5;
    //第三排按钮的y轴
    CGFloat threeBtnY = 0.f;
    //第三排最后一个按钮的x轴
    CGFloat threeLBtnX = 0.f;
    //最后两个大按钮宽
    CGFloat bigBtnW = (ScreenWidth - startX * 3) * .5;
    for (int i = 0; i < count; i++) {
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        NSInteger index = i % 10;
        NSInteger page = i / 10;
        btn.frame = CGRectMake(index*(buttonWidth+widthSpace)+startX,page*(buttonHeight+heightSpace) + startY,buttonWidth,buttonHeight);
        if (i >= 10 && i < 19) {
            index = i % 10;
            page = i / 10;
            btn.frame = CGRectMake(index*(buttonWidth+widthSpace)+startX+buttonWidth * .5,page*(buttonHeight+heightSpace) + startY,buttonWidth,buttonHeight);
        }else if (i >= 19){
            index = i % 9;
            page = i / 9;
            threeBtnY = page*(buttonHeight+heightSpace) + startY;
            if (i == count - 1) {
                threeLBtnX = index*(buttonWidth+widthSpace)+startX+buttonWidth * .5 + buttonWidth + startX;
            }
            btn.frame = CGRectMake(index*(buttonWidth+widthSpace)+startX+buttonWidth * .5,threeBtnY,buttonWidth,buttonHeight);
        }
        btn.tag = 020202;
        [btn setBackgroundColor:[UIColor whiteColor]];
        btn.layer.masksToBounds = YES;
        btn.layer.borderWidth = .5f;
        btn.layer.borderColor = kColorWithRGBAlpha(204, 204, 204, 1).CGColor;
        btn.layer.cornerRadius = 5.f;
        [btn setTitleColor:[self getBuleColor] forState:UIControlStateNormal];
        [btn.titleLabel setFont:[self getFont:KEYBOARD_DEFAULT_FONT_SIZE]];
        NSString *ch = [characters substringWithRange:NSMakeRange(CharacterArr_[i], 1)];
        [btn setTitle:ch forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(characterButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        btn.tag = CharacterArr_[i];
        [ASCIICapableKeyboard_ addSubview:btn];
    }
    
    if (isLogin_) {
        //        int width_ ;
        //        //shift键
        //        UIButton *shiftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        //        shiftBtn.frame = CGRectMake(startX, threeBtnY, shiftBtnW, buttonHeight);
        //        UIImage *backImgN = [UIImage imageNamed:@"c_chaKeyboardShiftButton.png"];
        //        [shiftBtn setBackgroundImage:backImgN forState:UIControlStateNormal];
        //        [shiftBtn addTarget:self action:@selector(shiftButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        //        [ASCIICapableKeyboard_ addSubview:shiftBtn];
        //
        //
        //        //////////////
        //        //删除键
        //        oringinX = secondLeftMarginNum + (8 % 10) * (CHAKEYBOARD_WEIGHT + H_SPACE);
        //        oringinY = topMarginNum + 2 * (CHAKEYBOARD_HEIGHT + V_SPACE);
        //        [self createDeletelButtonToView:ASCIICapableKeyboard_ frame:CGRectMake(oringinX, oringinY, CHAKEYBOARD_WEIGHT + (CHAKEYBOARD_WEIGHT)/2, CHAKEYBOARD_HEIGHT) type:RANDOM_CHARACTE];
        //
        //        //数字切换键
        //        int buttonInedex_;
        //        buttonInedex_ = 1;
        //        int currentLineNum_ = 2;
        //        //计算这一行中按键个数为currentLineNum_时，每个按键的宽度
        //        width_ = (((self.frame.size.width - 2 * (V_SPACE + CHAKEYBOARD_WEIGHT)) - (currentLineNum_ * h_space))/currentLineNum_);
        //
        //        oringinX = leftMarginNum + ((h_space + width_) * (buttonInedex_ - 1));
        //        oringinY = topMarginNum + (4-1) * (CHAKEYBOARD_HEIGHT + V_SPACE);
        //        [self createSwitchButtonToView:ASCIICapableKeyboard_ frame:CGRectMake(oringinX, oringinY, width_, CHAKEYBOARD_HEIGHT) type:RANDOM_NUMBER curType:RANDOM_CHARACTE];
        //
        //        //字符切换键
        //        buttonInedex_ = 2;
        //        oringinX = leftMarginNum + ((h_space + width_) * (buttonInedex_ - 1));
        //        oringinY = topMarginNum + (4 - 1) * (CHAKEYBOARD_HEIGHT + V_SPACE);
        //        [self createSwitchButtonToView:ASCIICapableKeyboard_ frame:CGRectMake(oringinX, oringinY, width_ , CHAKEYBOARD_HEIGHT) type:RANDOM_SYMBOL curType:RANDOM_CHARACTE];
        //
        //        //登录健
        //        buttonInedex_ = 3;
        //        oringinX = leftMarginNum + ((h_space + width_) * (buttonInedex_ - 1));
        //        oringinY = topMarginNum + (4 - 1) * (CHAKEYBOARD_HEIGHT + V_SPACE);
        //        [self createLoginButtonToView:ASCIICapableKeyboard_ frame:CGRectMake(oringinX, oringinY, 2 * CHAKEYBOARD_WEIGHT  , CHAKEYBOARD_HEIGHT) type:RANDOM_SYMBOL];
        
    }else{
        //shift键
        UIButton *shiftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        shiftBtn.frame = CGRectMake(startX, threeBtnY, shiftBtnW, buttonHeight);
        UIImage *backImgN = [UIImage imageNamed:@"c_chaKeyboardShiftButton.png"];
        [shiftBtn setBackgroundImage:backImgN forState:UIControlStateNormal];
        shiftBtn.layer.masksToBounds = YES;
        shiftBtn.layer.cornerRadius = 5.f;
        shiftBtn.layer.borderWidth = .5f;
        shiftBtn.layer.borderColor = kColorWithRGB(204, 204, 204).CGColor;
        shiftBtn.adjustsImageWhenHighlighted = NO;
        [shiftBtn addTarget:self action:@selector(shiftButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        [ASCIICapableKeyboard_ addSubview:shiftBtn];
        //删除键
        [self createDeletelButtonToView:ASCIICapableKeyboard_ frame:CGRectMake(threeLBtnX, threeBtnY, shiftBtnW, buttonHeight) type:RANDOM_CHARACTE];
        
        //大按钮Y轴
        CGFloat bigBtnY = threeBtnY + buttonHeight + startY;
        //数字切换键
        [self createSwitchButtonToView:ASCIICapableKeyboard_ frame:CGRectMake(startX, bigBtnY, bigBtnW, buttonHeight) type:RANDOM_NUMBER curType:RANDOM_CHARACTE];
        
        //字符切换键
        [self createSwitchButtonToView:ASCIICapableKeyboard_ frame:CGRectMake(startX * 2 + bigBtnW, bigBtnY, bigBtnW , buttonHeight) type:RANDOM_SYMBOL curType:RANDOM_CHARACTE];
    }
    
    
    
    [self addSubview:ASCIICapableKeyboard_];
    
    //    [pool drain];
}

- (void)setOrigin:(CGPoint)origin
{
    CGRect frame = self.frame;
    frame.origin = origin;
    self.frame = frame;
}


- (void) createAndShowSymbolPadKeyboardWithSwitchButtonStatus:(BOOL)status
{
    if (isNormalSort_) {
        [self createSymbolNormalOrder];
    }else {
        //        [self createSymbolRandomOrder];
    }
    //清空数字键盘上的控件
    [[SymbolPadKeyboard_ subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
    static NSString *characters = SYMBOLS;
    //按钮数
    CGFloat count = 32;
    //第一个按钮的X坐标
    CGFloat startX = 2.f;
    //第一个按钮的Y坐标
    CGFloat startY = 5.f;
    //横间距
    CGFloat widthSpace = 2.f;
    //纵间距
    CGFloat heightSpace = 5.f;
    //按钮宽
    CGFloat buttonWidth = (ScreenWidth - widthSpace * 11) / 10;
    //按钮高
    CGFloat buttonHeight = (KeyboardHeight - 25) / 4;
    //最后一排按钮y轴
    CGFloat lastBtnY = 0.f;
    for (int i = 0; i < count; i++) {
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        NSInteger index = i % 10;
        NSInteger page = i / 10;
        if (i == count - 1) {
            lastBtnY = page*(buttonHeight+heightSpace) + startY;
        }
        btn.frame = CGRectMake(index*(buttonWidth+widthSpace)+startX,page*(buttonHeight+heightSpace) + startY,buttonWidth,buttonHeight);
        btn.tag = 030303;
        [btn setBackgroundColor:[UIColor whiteColor]];
        btn.layer.masksToBounds = YES;
        btn.layer.borderWidth = .5f;
        btn.layer.borderColor = kColorWithRGBAlpha(204, 204, 204, 1).CGColor;
        btn.layer.cornerRadius = 5.f;
        [btn setTitleColor:[self getBuleColor] forState:UIControlStateNormal];
        [btn.titleLabel setFont:[self getFont:KEYBOARD_DEFAULT_FONT_SIZE]];
        NSString *ch = [characters substringWithRange:NSMakeRange(SymbolArr_[i], 1)];
        [btn setTitle:ch forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(symbolButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        btn.tag = SymbolArr_[i];
        [SymbolPadKeyboard_ addSubview:btn];
    }
    
    if (isLogin_) {
        //        //数字切换键
        //        ii = 2;
        //        oringinX = leftMarginNum + (totalNum % lineNum) * (SYMBOLKEYBOARD_WEIGHT + h_space);
        //        oringinY = topMarginNum + (totalNum / lineNum) * (SYMBOLKEYBOARD_HEIGHT + V_SPACE);
        //        [self createSwitchButtonToView:SymbolPadKeyboard_ frame:CGRectMake(oringinX, oringinY, SYMBOLKEYBOARD_WEIGHT*ii+h_space*(ii-1), SYMBOLKEYBOARD_HEIGHT) type:RANDOM_NUMBER curType:RANDOM_SYMBOL];
        //
        //        //字母切换键
        //        ii = 2;
        //        oringinX = leftMarginNum + ((totalNum+2) % lineNum) * (SYMBOLKEYBOARD_WEIGHT + h_space);
        //        oringinY = topMarginNum + ((totalNum+2) / lineNum) * (SYMBOLKEYBOARD_HEIGHT + V_SPACE);
        //        [self createSwitchButtonToView:SymbolPadKeyboard_ frame:CGRectMake(oringinX, oringinY, SYMBOLKEYBOARD_WEIGHT*ii+h_space*(ii-1), SYMBOLKEYBOARD_HEIGHT) type:RANDOM_CHARACTE curType:RANDOM_SYMBOL];
        //
        //        //删除键
        //        ii = 2;
        //        oringinX = leftMarginNum + ((totalNum+4) % lineNum) * (SYMBOLKEYBOARD_WEIGHT + h_space);
        //        oringinY = topMarginNum + ((totalNum+4) / lineNum) * (SYMBOLKEYBOARD_HEIGHT + V_SPACE);
        //        [self createDeletelButtonToView:SymbolPadKeyboard_ frame:CGRectMake(oringinX, oringinY, SYMBOLKEYBOARD_WEIGHT*ii+h_space*(ii-1), SYMBOLKEYBOARD_HEIGHT) type:RANDOM_SYMBOL];
        //
        //        //登录键
        //        ii = 2;
        //        oringinX = leftMarginNum + ((totalNum+6) % lineNum) * (SYMBOLKEYBOARD_WEIGHT + h_space);
        //        oringinY = topMarginNum + ((totalNum+6) / lineNum) * (SYMBOLKEYBOARD_HEIGHT + V_SPACE);
        //        [self createLoginButtonToView:SymbolPadKeyboard_ frame:CGRectMake(oringinX, oringinY, SYMBOLKEYBOARD_WEIGHT*ii+h_space*(ii-1), SYMBOLKEYBOARD_HEIGHT) type:RANDOM_SYMBOL];
        
    }else{
        //数字切换键
        CGFloat numX = startX * 3 + buttonWidth * 2;
        CGFloat numW = buttonWidth * 3 + startX * 2;
        [self createSwitchButtonToView:SymbolPadKeyboard_ frame:CGRectMake(numX, lastBtnY, numW, buttonHeight) type:RANDOM_NUMBER curType:RANDOM_SYMBOL];
        
        //字母切换键
        [self createSwitchButtonToView:SymbolPadKeyboard_ frame:CGRectMake(numX + numW + startX, lastBtnY, numW, buttonHeight) type:RANDOM_CHARACTE curType:RANDOM_SYMBOL];
        
        //删除键
        [self createDeletelButtonToView:SymbolPadKeyboard_ frame:CGRectMake(numX + numW + startX * 2 + numW, lastBtnY, buttonWidth * 2 + startX, buttonHeight) type:RANDOM_SYMBOL];
    }
    
    [self addSubview:SymbolPadKeyboard_];
}


#pragma mark - Action
- (void)showCharactersTipView:(id)sender{
    //    if (self.tipView.hidden) {
    //        self.tipView.hidden = NO;
    //    }
    //    [self.tipView removeAllSubviews];
    //    UIButton *button = (UIButton *)sender;
    //    UIImage *img = [UIImage imageNamed:@"key_new.png"];
    //    UIImageView *imgView = [[UIImageView alloc]initWithImage:img];
    //    CGRect rect = button.frame;
    //    CGSize imgSize = img.size;
    //    rect.origin.x = rect.origin.x + (rect.size.width - img.size.width)/2;
    //    rect.origin.y = rect.origin.y  - img.size.height + backgroundImageView_.frame.origin.y;
    //    rect.size = imgSize;
    //    img = [UIImage imageNamed:@"key_new.png"];
    //    [imgView setImage:img];
    //    [self.tipView setFrame:rect];
    //    [self.tipView addSubview:imgView];
    //
    //    NSString *text = button.titleLabel.text;
    //    if (isShiftOn_) {
    //        text = [text uppercaseString];
    //    }
    //    CGRect labelRect = imgView.frame;
    //    labelRect.size.height = labelRect.size.height/2 + 15;
    //    labelRect.origin.y = labelRect.origin.y + 5;
    //    UILabel *label = [[UILabel alloc]initWithFrame:labelRect];
    //    label.text = text;
    //    label.font = [UIFont fontWithName:@"Helvetica-Light" size:20];
    //    //    label.font = [self getFont:35.0];
    //    label.textAlignment = NSTextAlignmentCenter;
    //    label.backgroundColor = [UIColor clearColor];
    //    label.textColor = [self getBuleColor];
    //    [self.tipView addSubview:label];
    //    [self bringSubviewToFront:self.tipView];
    //    [label release];
    //    [imgView release];
}

- (void)showSymbolTipView:(id)sender{
    //    if (self.tipView.hidden) {
    //        self.tipView.hidden = NO;
    //    }
    //    [self.tipView removeAllSubviews];
    //    UIButton *button = (UIButton *)sender;
    //    UIImage *img = [UIImage imageNamed:@"key_new.png"];
    //    UIImageView *imgView = [[UIImageView alloc]initWithImage:img];
    //    CGRect rect = button.frame;
    //    CGSize imgSize = img.size;
    //    rect.origin.x = rect.origin.x + (rect.size.width - img.size.width)/2;
    //    if (rect.origin.x<0) {
    //        rect.origin.x = button.frame.origin.x;
    //        //        img = [UIImage imageNamed:@"key_left.png"];
    //        img = [UIImage imageNamed:@"key_new.png"];
    //        [imgView setImage:img];
    //    }else if (rect.origin.x + img.size.width > [[UIScreen mainScreen]bounds].size.width){
    //        rect.origin.x = button.frame.origin.x + button.frame.size.width - img.size.width;
    //        //        img = [UIImage imageNamed:@"key_right.png"];
    //        img = [UIImage imageNamed:@"key_new.png"];
    //        [imgView setImage:img];
    //    }
    //
    //    rect.origin.y = rect.origin.y  - img.size.height + backgroundImageView_.frame.origin.y - 1;
    //    rect.size = imgSize;
    //    [self.tipView setFrame:rect];
    //    [self.tipView addSubview:imgView];
    //
    //    //    NSUInteger num = ((UIButton *)sender).tag;
    //    //    static NSString *characters = SYMBOLS;
    //    //    NSString *text = [characters substringWithRange:NSMakeRange(num, 1)];
    //
    //    NSString *text = button.titleLabel.text;
    //    CGRect labelRect = imgView.frame;
    //    labelRect.size.height = labelRect.size.height/2 + 7;
    //
    //    UILabel *label = [[UILabel alloc]initWithFrame:labelRect];
    //    label.text = text;
    //    label.font = [UIFont fontWithName:@"Helvetica-Light" size:20];
    //    //    label.font = [self getFont:36.0];
    //    label.textAlignment = NSTextAlignmentCenter;
    //    label.backgroundColor = [UIColor clearColor];
    //    label.textColor = [self getBuleColor];
    //
    //    [self.tipView addSubview:label];
    //    [self bringSubviewToFront:self.tipView];
    //
    //    [label release];
    //    [imgView release];
}

- (void)hideTipView:(id)sender{
    if (!self.tipView.hidden) {
        self.tipView.hidden = YES;
    }
}

- (void)numberButtonPressed:(id)sender{
    //    if ([sender isKindOfClass:[UIButton class]]){
    //        UIButton *zeroBtn = (UIButton*)sender;
    //        if (0 == ((UIButton*)sender).tag){
    //            [self zeroBtnClick:zeroBtn andKeyboardType:KeyboardNumberType];
    //        }
    //    }
    int num;
    if (isNumber_){
        num = ((UIButton *)sender).tag;
    }else{
        num = (((UIButton *)sender).tag + [[randomKey_ objectAtIndex:value_.length]integerValue]%10)%10;
    }
    NSString *t = [NSString stringWithFormat:@"%d", num];
    [self press:t];
}

- (void)characterButtonPressed:(id)sender {
    if (!self.tipView.hidden) {
        self.tipView.hidden = YES;
    }
    //    if ([sender isKindOfClass:[UIButton class]]){
    //        UIButton *pBtn = (UIButton*)sender;
    //        if ([[pBtn.titleLabel.text lowercaseString] isEqualToString:@"q"]){
    //            [self zeroBtnClick:pBtn andKeyboardType:KeyboardCharacterType];
    //        }
    //    }
    static NSString *characters = CHARACTERS;
    NSUInteger num = (((UIButton *)sender).tag + [[randomKey_ objectAtIndex:value_.length]integerValue]%characters.length)%characters.length;
    
    NSString *ch = [characters substringWithRange:NSMakeRange(num, 1)];
    
    if (isShiftOn_) {
        ch = [ch uppercaseString];
    }
    [self press:ch];
    
}

- (void)symbolButtonPressed:(id)sender {
    if (!self.tipView.hidden) {
        self.tipView.hidden = YES;
    }
    //    UIButton *button = ((UIButton *)sender);
    //    [self zeroBtnClick:button andKeyboardType:KeyboardSymbolType];
    
    static NSString *characters = SYMBOLS;
    NSUInteger num = (((UIButton *)sender).tag + [[randomKey_ objectAtIndex:value_.length]integerValue]%characters.length)%characters.length;
    NSString *ch = [characters substringWithRange:NSMakeRange(num, 1)];
    [self press:ch];
}

- (void)switchButtonPressed:(id)sender
{
    UIButton *button = ((UIButton *)sender);
    NSInteger num = button.tag;
    [self zeroBtnClick:button andKeyboardType:KeyboardSwitchType];
    
    if (num == 1 && (isNumOnly_ || isNumAndChar_))
    {
        return;
    }else if ((num == 0) && isNumOnly_){
        return;
    }
    if (NumberPadKeyboard_) {
        [NumberPadKeyboard_ removeFromSuperview];
    }
    if (ASCIICapableKeyboard_) {
        [ASCIICapableKeyboard_ removeFromSuperview];
    }
    if (SymbolPadKeyboard_) {
        [SymbolPadKeyboard_ removeFromSuperview];
    }
    switch (num) {
        case 0://切换到字母键盘
        [self createAndShowASCIICapableKeyboardWithSwitchButtonStatus:YES];
        currentKeyboardType_ = 1;
        break;
        case 1://切换到符号键盘
        [self createAndShowSymbolPadKeyboardWithSwitchButtonStatus:YES];
        currentKeyboardType_ = 2;
        break;
        case 2://切换到数字键盘
        [self createAndShowNumberPadKeyboardWithSwitchButtonStatus:YES];
        currentKeyboardType_ = 0;
        break;
        default:
        break;
    }
}


-(void)zeroBtnClick:(UIButton*)currentBtn andKeyboardType:(KeyboardType)type{
    UIImage *typeImg = nil;
    _kType = type;
    switch (type) {
        case KeyboardNumberType:
        typeImg = [UIImage imageNamed:@"c_numKeyboardButton.png"];
        break;
        case KeyboardCharacterType:
        typeImg = [UIImage imageNamed:@"c_chaKeyboardButton.png"];
        [self showCharactersTipView:currentBtn];
        break;
        case KeyboardSymbolType:
        typeImg = [UIImage imageNamed:@"c_symbolKeyboardButton.png"];
        [self showSymbolTipView:currentBtn];
        break;
        case KeyboardSwitchType:
        //            typeImg = [UIImage imageNamed:self.back_S];
        break;
        default:
        break;
    }
    [currentBtn setBackgroundImage:typeImg forState:UIControlStateNormal];
    [self performSelector:@selector(changeColorWithZeroButton:) withObject:currentBtn afterDelay:ZERO_BUTTON_TIME];
}

-(void)changeColorWithZeroButton:(UIButton*)currentBtn{
    UIImage *typeImg = nil;
    switch (_kType) {
        case KeyboardNumberType:
        typeImg = [UIImage imageNamed:@"c_numKeyboardButton.png"];break;
        case KeyboardCharacterType:
        typeImg = [UIImage imageNamed:@"c_chaKeyboardButton.png"];break;
        case KeyboardSymbolType:
        typeImg = [UIImage imageNamed:@"c_symbolKeyboardButton.png"];break;
        case KeyboardSwitchType:
        typeImg = [UIImage imageNamed:self.back_N];break;
        default:
        break;
    }
    
    if (self.tipView && ![self.tipView isHidden]) {
        [self.tipView setHidden:YES];
    }
    [currentBtn setBackgroundImage:typeImg forState:UIControlStateNormal];
}




////乱序排列
//- (void)sortButtonPressed:(id)sender{
//    [self vibrate];
//    [NumberPadKeyboard_ removeFromSuperview];
//    [ASCIICapableKeyboard_ removeFromSuperview];
//    [SymbolPadKeyboard_ removeFromSuperview];
//
//    isNormalSort_ = isNormalSort_ ? NO : YES;
//
//    switch (currentKeyboardType_) {
//        case 0://数字键盘的正、乱序切换
//            [self createAndShowNumberPadKeyboardWithSwitchButtonStatus:YES];
//            break;
//        case 1://字母键盘的正、乱序切换
//            [self createAndShowASCIICapableKeyboardWithSwitchButtonStatus:YES];
//            break;
//        case 2://符号键盘的正、乱序切换
//            [self createAndShowSymbolPadKeyboardWithSwitchButtonStatus:YES];
//            break;
//        default:
//            break;
//    }
//
//}

- (void)shiftButtonPressed:(id)sender
{
    isShiftOn_ = isShiftOn_ ? NO : YES;
    UIButton *sb = (UIButton *)sender;
    sb.selected = sb.selected ? NO : YES;
    
    NSArray *array = ASCIICapableKeyboard_.subviews;
    for (UIView *view in array) {
        if ([view isKindOfClass:[UIButton class]]) {
            UIButton *btn = (UIButton *)view;
            if (isShiftOn_) {
                [btn setTitle:[btn.titleLabel.text uppercaseString] forState:UIControlStateNormal];
            } else {
                [btn setTitle:[btn.titleLabel.text lowercaseString] forState:UIControlStateNormal];
            }
        }
    }
}

- (void)deleteButtonPressed:(id)sender {
    if (value_.length > 0) {
        value_ = [value_ substringToIndex:value_.length-1];
    }
    if (_pressValueBlock) {
        _pressValueBlock(value_);
    }
//    [[WL sharedInstance]sendActionToJS:@"keyboardChange" withData:@{@"value": value_}];
}

- (void)press:(NSString *)text {
    //限制密码输入的长度，并回传值；
    if(value_.length > maxLenth-1){
        return ;
    }
    value_ = [value_ stringByAppendingString:text];
    if (_pressValueBlock) {
        _pressValueBlock(value_);
    }
//    [[WL sharedInstance]sendActionToJS:@"keyboardChange" withData:@{@"value": value_}];
}

- (void)hideKeyboard:(UIButton*)btn{
    if (200 == btn.tag) {
//        [[WL sharedInstance] sendActionToJS:@"hideKeyboard"];
    }
    [UIView animateWithDuration:.4 animations:^{
        CGPoint origin = CGPointMake(0.0, ScreenHeight);
        [self setOrigin:origin];
        [self createAndShowNumberPadKeyboardWithSwitchButtonStatus:YES];
        currentKeyboardType_ = 0;
    }];
}

-(void)hideKeyboard{
    [UIView animateWithDuration:.4 animations:^{
        CGPoint origin = CGPointMake(0.0, ScreenHeight);
        [self setOrigin:origin];
        [self createAndShowNumberPadKeyboardWithSwitchButtonStatus:YES];
        currentKeyboardType_ = 0;
    }];
}

@end

