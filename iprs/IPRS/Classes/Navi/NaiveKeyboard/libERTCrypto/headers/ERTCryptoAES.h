///////////////////////////////////////////////////////////
// ERTCryptoAES.h
// ERTCryptoAES
// Created by wu.linchen on 2014-11-26.
// Copyright (c) 2014 rytong. All rights reserved.
///////////////////////////////////////////////////////////


#ifndef _ERTCRYPTO_AES_H_
#define _ERTCRYPTO_AES_H_

#include <openssl/aes.h> 
#include "ERTUserData.h"

class ERTCryptoAES 
{
public:
	enum ERTAESTYPE
	{
		AES,		// encrypto/ decrypto use the same key, different function init
		ECB,		// encrypto/ decrypto use the same key, different function init
		CBC,		// encrypto/ decrypto use the same key, different function init
		CTR128,		// encrypto/ decrypto use the same key, the same function init(AES_set_encrypt_key)
		IGE,		// encrypto/ decrypto use the same key, different function init
	};
public:
	ERTCryptoAES();
    virtual ~ERTCryptoAES();

    /**
    *  设置AES类型
    *  @param type 类型
    *  @param length 密钥实际长度
    *  @discussion 设置AES加解密算法类型，其中CBC与5.2兼容
    */
	void setAesType(ERTAESTYPE type);

    /**
    *  设置密钥
    *  @param key 密钥(二进制),
    *  @param length 密钥实际长度
    *  @discussion AES、ECB、CBC、CTR128、IGE、模式加密和解密均使用相同密钥,key全部填充0到32字节，超过则截取
    */
    virtual void setKey(unsigned char *key, unsigned int length);

    /**
    *  设置初始向量，用于加密和解密运算（IGE、BI_IGE、CTR、CFB、CBC）
    *  @param iv 初始向量(二进制),
    *  @param length 初始向量长度
    *  @discussion CTR、CFB、CBC为AES_BLOCK_SIZE、 IGE为AES_BLOCK_SIZE*2、 BI_IGE为AES_BLOCK_SIZE*4
    */
	void setIv(unsigned char *iv, unsigned int len = AES_BLOCK_SIZE*4);

    /**
    *  加密数据
    *  @param stringIn 要进行加密的数据,
    *  @param length 要进行加密的数据长度
    *  @param encrypedData  加密后的数据
    *  @return 加密结果是否成功
    *  @discussion 加密数据块采用PKCS7方式，获得的加密结果外部不能进行内存释放，由内部管理
    */
    virtual bool encryptData(const unsigned char *stringIn, const unsigned int length, ERTUserData &encrypedData);
	
    /**
    *  加密数据
    *  @param stringIn 要进行解密的数据,
    *  @param length 要进行解密的数据长度
    *  @param decryptedData 解密后的数据
    *  @return 解密结果是否成功
    *  @discussion 数据块采用PKCS7方式，获得的解密结果外部不能进行内存释放，由内部管理
    */
    virtual bool decryptData(const unsigned char *stringIn, const unsigned int length, ERTUserData &decryptedData);
protected:
    /**
    *  清理KEY
    */
    void clearEncryptKey();

    /**
    *  准备加密数据
    *  @param length 要进行解密的数据长度,
    *  @param stringIn 要进行解密的数据
    *  @param temp 获得的填充完毕的数据
    *  @return 填充完毕后的数据长度
    *  @discussion 数据块采用PKCS7方式
    */
	long prepareEncrypt(const unsigned int length, const unsigned char *stringIn, unsigned char *&temp);

    /**
    *  结束加密
    *  @param temp 由prepareEncrypt产生的填充后的块数据
    *  @discussion 数据块采用PKCS7方式
    */
	void endEncrypt(unsigned char *&temp);

    /**
    *  解密函数
    *  @param stringIn 要进行解密的数据
     *  @param length 要进行解密的数据长度,
     *  @param decryptdData 解密后的数据
    *  @discussion 数据块采用PKCS7方式
    */
	void decryptAES(const unsigned char *stringIn, const unsigned int length, ERTUserData &decryptdData);
	void decryptECB(const unsigned char *stringIn, const unsigned int length, ERTUserData &decryptdData);
	void decryptCBC(const unsigned char *stringIn, const unsigned int length, ERTUserData &decryptdData);
	void decryptCTR(const unsigned char *stringIn, const unsigned int length, ERTUserData &decryptdData);
	void decryptIGE(const unsigned char *stringIn, const unsigned int length, ERTUserData &decryptdData);
    bool deprocPKCS7(ERTUserData &data);
protected:
	// 密钥
	unsigned char *encryptKey_;
	// 加密密钥长度
	unsigned int encryptKeyLen_;

	// 加解密算法类型
	ERTAESTYPE type_;
	// 初始向量，某些算法使用
	unsigned char iv_[AES_BLOCK_SIZE*4];//
};
class ERTCryptoAES2 : private ERTCryptoAES
{
public:
    enum ERTAESTYPE2
	{
		CFB128,		// encrypto/ decrypto use different key, different function init(note: be the same order)
		CFB1,		// encrypto/ decrypto use different key, different function init(note: be the same order)
		CFB8,		// encrypto/ decrypto use different key, different function init(note: be the same order)
		OFB128,		// encrypto/ decrypto use different key, different function init(note: be the same order)
		BI_IGE		// encrypto/ decrypto use the same two keys, setEncryptKey/setDecryptKey set two keys
	};
public:
	ERTCryptoAES2();
	~ERTCryptoAES2();
    /**
    *  设置AES类型
    *  @param type 类型
    *  @param length 密钥实际长度
    *  @discussion 设置AES加解密算法类型，其中CBC与5.2兼容
    */
    void setAesType(ERTAESTYPE2 type);

    /**
    *  设置初始向量，用于加密和解密运算（IGE、BI_IGE、CTR、CFB、CBC）
    *  @param iv 初始向量(二进制),
    *  @param length 初始向量长度
    *  @discussion CTR、CFB、CBC为AES_BLOCK_SIZE、 IGE为AES_BLOCK_SIZE*2、 BI_IGE为AES_BLOCK_SIZE*4
    */
    void setIv(unsigned char *iv, unsigned int len = AES_BLOCK_SIZE * 4);

	// CFB128、CFB1、CFB8 使用加密和解密两个密钥进行加密和解密，分别使用setEncryptKey、setDecryptKey设置密钥
	// length: key leng,must be 16 or 24 or 32 --- (mean 128bits, 192bits, 256 bits;)
	// the two funtion must be called befor encrypto and decrypt Data;

    /**
    *  设置加密密钥
    *  @param key 密钥(二进制),
    *  @param length 密钥实际长度
    *  @discussion key全部填充0到32字节，超过则截取
    */
    void setEncryptKey(unsigned char *key, unsigned int length);

    /**
    *  设置解密密钥
    *  @param key 密钥(二进制),
    *  @param length 密钥实际长度
    *  @discussion key全部填充0到32字节，超过则截取
    */
	void setDecryptKey(unsigned char *key, unsigned int length);

    /**
    *  加密数据
    *  @param stringIn 要进行加密的数据,
    *  @param length 要进行加密的数据长度
    *  @param encryptedData  加密后的数据
    *  @return 加密结果是否成功
    *  @discussion 加密数据块采用PKCS7方式，获得的加密结果外部不能进行内存释放，由内部管理
    */
    virtual bool encryptData(const unsigned char *stringIn, const unsigned int length, ERTUserData &encryptedData);

    /**
    *  加密数据
    *  @param stringIn 要进行解密的数据,
    *  @param length 要进行解密的数据长度
    *  @param decryptedData 解密后的数据
    *  @return 解密结果是否成功
    *  @discussion 数据块采用PKCS7方式，获得的解密结果外部不能进行内存释放，由内部管理
    */
    virtual bool decryptData(const unsigned char *stringIn, const unsigned int length, ERTUserData &decryptedData);
private:
	void decryptCFB(const unsigned char *stringIn, const unsigned int length, ERTUserData &data, ERTAESTYPE2 type);
	void decryptOFB(const unsigned char *stringIn, const unsigned int length, ERTUserData &data);
	void decryptBIIGE(const unsigned char *stringIn, const unsigned int length, ERTUserData &data);
	void clearDecryptKey();
private:
	// 加解密算法类型
	ERTAESTYPE2 type_;
	// 解密密钥
	unsigned char *decryptKey_;
	// 解密密钥长度
	unsigned int decryptKeyLen_;
};

#endif //_ERTCRYPTO_AES_H_