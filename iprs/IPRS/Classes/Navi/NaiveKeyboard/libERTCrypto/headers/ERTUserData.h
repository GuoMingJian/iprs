/*
 * ERTUserData.h
 *
 *  Created on: 2015年1月6日
 *      Author: root
 */

#ifndef ERT_NET_ERTUSERDATA_H_
#define ERT_NET_ERTUSERDATA_H_

#include "vector_data.h"
#include <string>


class ERTUserData:public vector_data<unsigned char>
{
	typedef vector_data<unsigned char> _Base;
public:
	using vector_data<unsigned char>::append;
	ERTUserData();
	ERTUserData(std::string);
    ERTUserData(const vector_data<unsigned char> &data);
    
#ifndef _UNSUPPORT_C11
    ERTUserData(const vector_data<unsigned char> &&data);
#endif
    
    ERTUserData(const unsigned char *data, _Base::num_type length);
	virtual ~ERTUserData();

public:
    static void setDebugMode(bool debug = true);
	ERTUserData mid(_Base::num_type begin, _Base::num_type count) const;
    void outPutVal() const;
    void outPut16Val() const;
    bool outPutFile(const char* filePath) const;
    void fromFile(const char* filePath);
    void append(const char *data, _Base::num_type length);
	void append(const std::string &str);
	operator std::string();
    operator std::string() const;
private :
    static bool debugMode_;
};


#endif /* ERT_NET_ERTUSERDATA_H_ */
