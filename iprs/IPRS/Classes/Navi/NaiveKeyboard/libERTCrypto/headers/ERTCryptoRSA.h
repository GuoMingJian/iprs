///////////////////////////////////////////////////////////
// ERTCryptoRSA.h
// ERTCryptoRSA
// Created by wu.linchen on 2014-11-26.
// Copyright (c) 2014 rytong. All rights reserved.
///////////////////////////////////////////////////////////


#ifndef _ERTCRYPTO_RSA_H_
#define _ERTCRYPTO_RSA_H_

#include <openssl/rsa.h>

#include <string>
#include "ERTUserData.h"


#define RSA_PKCS1_PADDING_SIZE	11

class ERTCryptoRSA 
{
public:
	ERTCryptoRSA();
	~ERTCryptoRSA();

    enum RSA_KEY_LENGTH
    {
        RSA_KEY_LENGTH_1024 = 1024,
        RSA_KEY_LENGTH_2048 = 2048
    };
    
    enum RSA_KEY_TYPE
    {
        RSA_KEY_PUB,
        RSA_KEY_PRIV
    };
    
    enum RSA_PADDING_TYPE
    {
        
        RSA_PADDING_PKCS1 = RSA_PKCS1_PADDING,
        RSA_PADDING_SSLV23 = RSA_SSLV23_PADDING,
        RSA_PADDING_NO = RSA_NO_PADDING,
        RSA_PADDING_PKCS1_OAEP = RSA_PKCS1_OAEP_PADDING,
        RSA_PADDING_X931 = RSA_X931_PADDING,
        RSA_PADDING_PKCS1V1_5
        /* EVP_PKEY_ only */
        //RSA_PKCS1_PSS_PADDING	= 6,

        //RSA_PKCS1_PADDING_SIZE =	11
    };

    /**
    *  生成密钥的pem文件，包括公钥和私钥
    *  @param file 文件名,
    *  @param length 密钥长度,
    *  @return 是否成功
    */
    bool generateKey(const char* file, RSA_KEY_LENGTH length);
    
    /**
     *  生成模指数，公钥指数，私钥指数
     *  @param bits 以多少位方式生成指数
     *  @param n，e，d 定义参见RSA指数定义
     *  @return 是否成功
     */
    bool generateKeyCAne(const RSA_KEY_LENGTH bits, ERTUserData &n, ERTUserData &e, ERTUserData &d);
    
    /**
     *  RSA key
     *  @param bits 以多少位方式生成指数
     *  @param n，e，d 定义参见RSA指数定义
     *  @return 是否成功
     */
    RSA* generateKey(const RSA_KEY_LENGTH bits);
    
    /**
    *  加载pem文件方式获得私钥和公钥
    *  @param file 文件名,
    *  @return 是否成功
    */
    bool loadKey(const char* file);
    /**
    *  加载pem文件方式获得私钥
    *  @param file 文件名,
    *  @return 是否成功
    */
    bool loadPriKey(const char* file);
    
    
    /**
     *  从DER编码的X509整数中读取私钥
     *  @param data 内存数据,
     *  @param length 数据长度,
     *  @return 是否成功
     */
    bool loadPriKey(const unsigned char* data, long length);
    
    
    bool loadPriKey(ERTUserData& n, ERTUserData& d);
    
    void setKey(RSA* key, RSA_KEY_TYPE type );
    
    /**
    *  加载pem文件方式获得公钥
    *  @param file 文件名,
    *  @return 是否成功
    */
    bool loadPubKey(const char* file);
    
    /**
     *  从DER编码的X509整数中读取公钥
     *  @param data 内存数据,
     *  @param length 数据长度,
     *  @return 是否成功
     */
    bool loadPubKey(const unsigned char* data, long length);

    
    bool loadPubKey(ERTUserData& n, ERTUserData& e);
    
    /**
     *  获取秘钥长度
     *  @param type 密钥类型,
     *  @return 长度
     */
    int keySize(RSA_KEY_TYPE type);
    
    /**
    *  私钥加密
    *  @param stringIn 要进行加密的数据,
    *  @param length 要进行加密的数据长度
    *  @param encryptedData 加密后的数据
    *  @return 加密结果是否成功
    */
    int prikeyEncrypt(const unsigned char *in, int in_len, ERTUserData &encryptedData,RSA_PADDING_TYPE padding = RSA_PADDING_NO);

    /**
    *  私钥解密
    *  @param stringIn 要进行解密的数据,
    *  @param length 要进行解密的数据长度
    *  @param decryptedData 加解密的数据
    *  @return 解密结果是否成功
    */
	int prikeyDecrypt(const unsigned char *in, int in_len, ERTUserData &decryptedData,RSA_PADDING_TYPE padding = RSA_PADDING_NO);

    /**
    *  公钥加密
    *  @param stringIn 要进行加密的数据,
    *  @param length 要进行加密的数据长度
    *  @param encryptedData 加密后的数据
    *  @return 加密结果是否成功
    */
	int pubkeyEncrypt(const unsigned char *in, int in_len,ERTUserData &encryptedData, RSA_PADDING_TYPE padding = RSA_PADDING_NO);


    /**
    *  公钥解密
    *  @param stringIn 要进行解密的数据,
    *  @param length 要进行解密的数据长度
    *  @param decryptedData 加解密的数据
    *  @return 解密结果是否成功
    */
    int pubkeyDecrypt(const unsigned char *in, int in_len, ERTUserData &decryptedData, RSA_PADDING_TYPE padding = RSA_PADDING_NO);
    
    /**
     *  从指数获得秘钥
     *  @param n 模指数
     *  @param e 公钥指数
     *  @param d 私钥指数
     *  @return 秘钥
     */
    static RSA *fromNED(const ERTUserData& n,const ERTUserData& e,const ERTUserData& d);
    
    /**
     *  rsa公钥加密和签名前进行数据的PKCS1.5安全编码
     *  @param to 新数据块地址
     *  @param tlen 目标大小
     *  @param from 原数据
     *  @param flen 愿数据长度
     *  @return 函数执行是否成功
     */
    static bool rsaPaddingAddPKCS1Type1Enc(unsigned char *to, int tlen, const unsigned char *from, int flen);
    
    
    /**
     *  关闭公钥、私钥
     */
    void closeKey();
protected:

    void closeKey(RSA *&key);

    void clearData(unsigned char *&data);

private:
	RSA *pubKey_;	// 公钥
	RSA *priKey_;	// 私钥

};

#endif // _ERTCRYPTO_RSA_H_
