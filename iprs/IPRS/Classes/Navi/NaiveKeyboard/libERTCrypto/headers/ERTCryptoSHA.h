///////////////////////////////////////////////////////////
// ERTCryptoSHA.h
// ERTCryptoSHA; ERTCryptoSHA1; ERTCryptoSHA224; ERTCryptoSHA256; ERTCryptoSHA384; ERTCryptoSHA512
// Created by wu.linchen on 2014-11-26.
// Copyright (c) 2014 rytong. All rights reserved.
///////////////////////////////////////////////////////////


#ifndef _ERTCRYPTO_SHA_H_
#define _ERTCRYPTO_SHA_H_

#include "ERTCryptoDigest.h"
class ERTCryptoSHA :
	public ERTCryptoDigest
{
public:
	ERTCryptoSHA();
	~ERTCryptoSHA();
public:

    /**
    *  从字符串获取消息摘要（二进制数据，非可读字符串）
    *  @param data 二进制数据,
    *  @param datalength 二进制数据长度
    *  @return 返回摘要串
    *  @discussion 摘要长度与算法有关，SHA为160位（20字节）
    */
	virtual ERTUserData digestFromData(void* data, long length);
};


class ERTCryptoSHA1 :
	public ERTCryptoDigest
{
public:
	ERTCryptoSHA1();
	~ERTCryptoSHA1();
public:
    /**
    *  从字符串获取消息摘要（二进制数据，非可读字符串）
    *  @param data 二进制数据,
    *  @param datalength 二进制数据长度
    *  @return 返回摘要串
    *  @discussion 摘要长度与算法有关，SHA1为160位（20字节）
    */
	virtual ERTUserData digestFromData(void* data, long length);
};


class ERTCryptoSHA224 :
	public ERTCryptoDigest
{
public:
	ERTCryptoSHA224();
	~ERTCryptoSHA224();
public:
    /**
    *  从字符串获取消息摘要（二进制数据，非可读字符串）
    *  @param data 二进制数据,
    *  @param datalength 二进制数据长度
    *  @return 返回摘要串
    *  @discussion 摘要长度与算法有关，SHA224为224位（28字节）
    */
	virtual ERTUserData digestFromData(void* data, long length);
};


class ERTCryptoSHA256 :
	public ERTCryptoDigest
{
public:
	ERTCryptoSHA256();
	~ERTCryptoSHA256();
public:
    /**
    *  从字符串获取消息摘要（二进制数据，非可读字符串）
    *  @param data 二进制数据,
    *  @param datalength 二进制数据长度
    *  @return 返回摘要串
    *  @discussion 摘要长度与算法有关，SHA256为256位（32字节）
    */
	virtual ERTUserData digestFromData(void* data, long length);
};



class ERTCryptoSHA384 :
	public ERTCryptoDigest
{
public:
	ERTCryptoSHA384();
	~ERTCryptoSHA384();
public:
    /**
    *  从字符串获取消息摘要（二进制数据，非可读字符串）
    *  @param data 二进制数据,
    *  @param datalength 二进制数据长度
    *  @return 返回摘要串
    *  @discussion 摘要长度与算法有关，SHA384为384位（48字节）
    */
	virtual ERTUserData digestFromData(void* data, long length);
};


class ERTCryptoSHA512 :
	public ERTCryptoDigest
{
public:
	ERTCryptoSHA512();
	~ERTCryptoSHA512();
public:
    /**
    *  从字符串获取消息摘要（二进制数据，非可读字符串）
    *  @param data 二进制数据,
    *  @param datalength 二进制数据长度
    *  @return 返回摘要串
    *  @discussion 摘要长度与算法有关，SHA512为512位（64字节）
    */
	virtual ERTUserData digestFromData(void* data, long length);
};

#endif // _ERTCRYPTO_SHA_H_