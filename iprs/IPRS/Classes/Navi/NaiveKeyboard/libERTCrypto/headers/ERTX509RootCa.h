//
//  ERTX509RootCa.h
//  ERTCore
//
//  Created by rytong on 15/3/26.
//  Copyright (c) 2015年 rytong. All rights reserved.
//

#ifndef __ERTCore__ERTX509RootCa__
#define __ERTCore__ERTX509RootCa__

#include <stdio.h>
#include "ERTCryptoX509.h"
#include "ERTCryptoSM2.h"

class ERTX509RootCa
{
public:
    ERTX509RootCa();
    ~ERTX509RootCa();
    
    /**
     *  从文件读取CA机构的密钥（私钥）信息
     *  @param pfilePath 密钥信息文件路径
     *  @return 密钥读取加载是否成功
     *  @discussion 密钥用于对请求签名的证书进行签名，调用signX509之前先执行readCaKey操作，否则不能签名
     */
    bool readCaKey(const char *pfilePath);
    
    /**
     *  从数据读取CA机构的密钥（私钥）信息
     *  @param key 密钥信息数据
     *  @return 密钥读取加载是否成功
     *  @discussion 密钥用于对请求签名的证书进行签名，调用signX509之前先执行readCaKey操作，否则不能签名
     */
    bool readCaKey(ERTUserData key);
    
    /**
     *  读取ca 公钥根证书
     *  @param pfilePath 公钥证书文件
     *  @return 读取公钥是否成功
     *  @discussion DER编码的X509证书，CA的公钥证书，用于验证其他证书是否为CA签名的合法证书，此函数由客户端调用，读取内置的root.ca公钥
     */
    bool readCaCer(const char *pfilePath);
    /**
     *  读取ca 公钥根证书
     *  @param key 公钥证书数据
     *  @return 读取公钥是否成功
     *  @discussion DER编码的X509证书，CA的公钥证书，用于验证其他证书是否为CA签名的合法证书，此函数由客户端调用，读取内置的root.ca公钥
     */
    bool readCaCer(ERTUserData key);
    
    /**
     *  对DER编码的X509证书进行私钥签名
     *  @param cer DER编码的X509证书 数据
     *  @param ID_A SM2签名算法使用的ID标识
     *  @return 签名结果
     *  @discussion 此函数级readCaKey由签发机构使用
     */
    bool signX509(ERTUserData &cer, ERTUserData ID_A);
    
    /**
     *  对DER编码的X509证书进行私钥签名
     *  @param cer DER编码的X509证书 数据
     *  @return 签名结果
     *  @discussion 此函数级readCaKey由签发机构使用， SM2签名算法的ID标识将会被内部代码计算出来，然后调用 bool signX509(ERTUserData &cer, ERTUserData ID_A);
     */
    bool signX509(ERTUserData &cer);
    
    /**
     *  验证DER编码的X509公钥证书是否为CA机构签发
     *  @param cer DER编码的X509证书 数据
     *  @param ID_A SM2签名算法使用的ID标识
     *  @return 验证结果
     *  @discussion 此函数及readCaCer由证书验证处是否为机构签发处使用
     */
    bool verifyX509(ERTUserData &cer, ERTUserData ID_A);
    
    /**
     *  验证DER编码的X509公钥证书是否为CA机构签发
     *  @param cer DER编码的X509证书 数据
     *  @return 验证结果
     *  @discussion 此函数及readCaCer由证书验证处是否为机构签发处使用，然后调用 bool verifyX509(ERTUserData &cer, ERTUserData ID_A);
     */
    bool verifyX509(ERTUserData &cer);
private:
    ERTX509RootCa &operator = (const ERTX509RootCa &ca);
    ERTX509RootCa(const ERTX509RootCa &sm2);
private:
    ERTUserData getActualData(const ERTUserData &cer);
private:
    ERTCryptoSM2* caKey_;
};

#endif /* defined(__ERTCore__ERTX509RootCa__) */
