///////////////////////////////////////////////////////////
// ERTCryptoDigest.h
// ERTCryptoDigest
// Created by wu.linchen on 2014-11-26.
// Copyright (c) 2014 rytong. All rights reserved.
///////////////////////////////////////////////////////////


#ifndef _ERTCRYPTO_DIGEST_H_
#define _ERTCRYPTO_DIGEST_H_
#include "ERTUserData.h"

class ERTCryptoDigest
{
public:
	ERTCryptoDigest();
	virtual ~ERTCryptoDigest();
public:
    /**
    *  从文件获取摘要字符串（即MD5或sha工具输出的字符“AB6CD982CD70A98CBFEEDCA....”）
    *  @param file 文件名,
    *  @return 摘要转换后的字符
    */
    ERTUserData fromFile(char *file);

    /**
    *  从字符串获取摘要字符串（即MD5或sha工具输出的字符“AB6CD982CD70A98CBFEEDCA....”）
    *  @param str 字符串,
    *  @return 摘要转换后的字符
    */
    ERTUserData fromString(char *str);


    /**
    *  从二进制数据获取摘要字符串（即MD5或sha工具输出的字符“AB6CD982CD70A98CBFEEDCA....”）
    *  @param data 二进制数据
    *  @param datalength：二进制数据长度（byte），
    *  @return 摘要转换后的字符
    */
	ERTUserData fromData(void *data, long datalength);

    /**
    *  从字符串获取消息摘要（二进制数据，非可读字符串）
    *  @param strIn 输入字符串,
    *  @return 返回摘要串
    *  @discussion 摘要长度与算法有关，均为固定长度如MD5为128位（16字节）
    */
    ERTUserData digestFromString(const char *strIn);

    /**
    *  从字符串获取消息摘要（二进制数据，非可读字符串）
    *  @param file 文件名,
    *  @return 返回摘要串
    *  @discussion 摘要长度与算法有关，均为固定长度如MD5为128位（16字节）
    */
    ERTUserData digestFromFile(char *file);

    /**
    *  从字符串获取消息摘要（二进制数据，非可读字符串）
    *  @param data 二进制数据,
    *  @param datalength 二进制数据长度
    *  @return 返回摘要串
    *  @discussion 摘要长度与算法有关，均为固定长度如MD5为128位（16字节）
    */
	virtual ERTUserData digestFromData(void *data, long datalength) = 0;
private:

    /**
    *  消息摘要转换成输出字符
    */
	ERTUserData digestToString(const ERTUserData &digest);

};

#endif //_ERTCRYPTO_DIGEST_H_
