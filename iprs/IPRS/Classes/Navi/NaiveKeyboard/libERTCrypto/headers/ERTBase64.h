﻿/******************************************************************
 * ERTBase64.h
 * ERTEncrypt
 *
 * Created by 黄东鹏 on 2014-08-29.
 * Copyright (c) 2014 rytong. All rights reserved.
 *****************************************************************/

#ifndef _ERT_BASE64_H_  
#define _ERT_BASE64_H_  

#include <string>  

using namespace std;

class ERTBase64
{
public:
	/**
	 *brief  Base64 endcode
	 *param  bytes_to_encode   输入的byte流
	 *param  in_len            字节长度[in]
	 *return                   编码后的string
	 */
	static string encode(unsigned char const* bytesToEncode, unsigned int len);

	/**
	 *brief  Base64 decode
	 *param  encoded_string    输入的byte流
	 *return                   解码后的string
	 */
	static string decode(string const& encodedString);

private:
	static inline bool isBase64(unsigned char c)
	{
		return (isalnum(c) || (c == '+') || (c == '/'));
	}

private:
	static const string base64Chars;
};

#endif //_ERT_BASE64_H_ 