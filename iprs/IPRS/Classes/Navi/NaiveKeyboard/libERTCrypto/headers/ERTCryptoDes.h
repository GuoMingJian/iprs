///////////////////////////////////////////////////////////
// ERTCrypto3Des.h
// ERTCrypto3Des
// Created by wu.linchen on 2014-12-15.
// Copyright (c) 2014 rytong. All rights reserved.
///////////////////////////////////////////////////////////


#ifndef _ERTCRYPTO_DES_H_
#define _ERTCRYPTO_DES_H_
#include <openssl/des.h>
#include "ERTUserData.h"

class ERTCrypto3Des
{
public:
    ERTCrypto3Des();
    ~ERTCrypto3Des();

    /**
    *  设置密钥
    *  @param key 密钥(二进制),
    *  @param length 密钥实际长度
    *  @discussion 如果密钥不足24字节将进行填充0，如果多于24则后续的密钥值不能起作用
    */
    void setKey(unsigned char *key, unsigned int length);

    /**
    *  加密数据
    *  @param stringIn 要进行加密的数据,
    *  @param length 要进行加密的数据长度
    *  @param stringOut 加密后的数据
    *  @param lengthOut 加密后的数据长度
    *  @return 加密结果是否成功
    *  @discussion 加密数据块采用PKCS7方式，获得的加密结果外部不能进行内存释放，由内部管理
    */
    virtual bool encryptData(unsigned char *stringIn, unsigned int length, ERTUserData &encryptData);

    /**
    *  加密数据
    *  @param stringIn 要进行解密的数据,
    *  @param length 要进行解密的数据长度
    *  @param stringOut 解密后的数据
    *  @param lengthOut 解密后的数据长度
    *  @return 解密结果是否成功
    *  @discussion 数据块采用PKCS7方式，获得的解密结果外部不能进行内存释放，由内部管理
    */
    virtual bool decryptData(unsigned char *stringIn, unsigned int length, ERTUserData &decryptData);
protected:
    /**
    *  清理KEY
    */
    void clearEncryptKey();

    /**
    *  准备加密数据
    *  @param length 要进行解密的数据长度,
    *  @param stringIn 要进行解密的数据
    *  @param temp 获得的填充完毕的数据
    *  @return 填充完毕后的数据长度
    *  @discussion 数据块采用PKCS7方式
    */
    long prepareEncrypt(const unsigned int length, const unsigned char *stringIn, unsigned char *&temp);
    bool deprocPKCS7(ERTUserData &data);
protected:
    // 密钥
    unsigned char *encryptKey_;
    // 加密密钥长度
    unsigned int encryptKeyLen_;
};

#endif //_ERTCRYPTO_DES_H_