//
//  ERTCryptoSM2.h
//  ERTCore
//
//  Created by rytong on 15/2/5.
//  Copyright (c) 2015年 rytong. All rights reserved.
//

#ifndef __ERTCore__ERTCryptoSM2__
#define __ERTCore__ERTCryptoSM2__

#include <openssl/ec.h>
#include <openssl/evp.h>
#include "ERTUserData.h"
#include "ERTCryptoX509.h"


// sm2规范用户ID标识默认值
static const unsigned char defaultIDA[] =
{
    0x31,0x32,0x33,0x34,    0x35,0x36,0x37,0x38,
    0x31,0x32,0x33,0x34,    0x35,0x36,0x37,0x38
};

enum HashType
{
    HashType_MD5,
    HashType_SHA,
    HashType_SHA1,
    HashType_SHA224,
    HashType_SHA256,
    HashType_SHA384,
    HashType_SHA512,
    HashType_SM3
};

// KDF函数 （密钥派生函数）
/**
 *  KDF函数 （密钥派生函数）
 *  @param type 曲线函数计算方式
 *  @param Z 输入的 共享密钥比特数据
 *  @param klen 要获得的密钥数据的比特长度
 *  @return 长度为klen的密钥比特数据K
 */
ERTUserData KDF(ERTUserData Z, int klen, HashType hashType);


class ERTCryptoSM2
{
public:
    enum Type
    {
        TYPE_GFp,
        TYPE_GF2m
    };
    
    enum ECType
    {
        ECType_Recommendation               = 0,  // 国密sm2推荐参数
#ifndef OPENSSL_NO_EC2M
        ECType_NID_sect113r1                = NID_sect113r1,
        ECType_NID_sect113r2                = NID_sect113r2,
        ECType_NID_sect131r1                = NID_sect131r1,
        ECType_NID_sect131r2                = NID_sect131r2,
        ECType_NID_sect163k1                = NID_sect163k1,
        ECType_NID_sect163r1                = NID_sect163r1,
        ECType_NID_sect163r2                = NID_sect163r2,
        ECType_NID_sect193r1                = NID_sect193r1,
        ECType_NID_sect193r2                = NID_sect193r2,
        ECType_NID_sect233k1                = NID_sect233k1,
        ECType_NID_sect233r1                = NID_sect233r1,
        ECType_NID_sect239k1                = NID_sect239k1,
        ECType_NID_sect283k1                = NID_sect283k1,
        ECType_NID_sect283r1                = NID_sect283r1,
        ECType_NID_sect409k1                = NID_sect409k1,
        ECType_NID_sect409r1                = NID_sect409r1,
        ECType_NID_sect571k1                = NID_sect571k1,
        ECType_NID_sect571r1                = NID_sect571r1,
        ECType_NID_X9_62_c2pnb163v1         = NID_X9_62_c2pnb163v1,
        ECType_NID_X9_62_c2pnb163v2         = NID_X9_62_c2pnb163v2,
        ECType_NID_X9_62_c2pnb163v3         = NID_X9_62_c2pnb163v3,
        ECType_NID_X9_62_c2pnb176v1         = NID_X9_62_c2pnb176v1,
        ECType_NID_X9_62_c2tnb191v1         = NID_X9_62_c2tnb191v1,
        ECType_NID_X9_62_c2tnb191v2         = NID_X9_62_c2tnb191v2,
        ECType_NID_X9_62_c2tnb191v3         = NID_X9_62_c2tnb191v3,
        ECType_NID_X9_62_c2pnb208w1         = NID_X9_62_c2pnb208w1,
        ECType_NID_X9_62_c2tnb239v1         = NID_X9_62_c2tnb239v1,
        ECType_NID_X9_62_c2tnb239v2         = NID_X9_62_c2tnb239v2,
        ECType_NID_X9_62_c2tnb239v3         = NID_X9_62_c2tnb239v3,
        ECType_NID_X9_62_c2pnb272w1         = NID_X9_62_c2pnb272w1,
        ECType_NID_X9_62_c2pnb304w1         = NID_X9_62_c2pnb304w1,
        ECType_NID_X9_62_c2tnb359v1         = NID_X9_62_c2tnb359v1,
        ECType_NID_X9_62_c2pnb368w1         = NID_X9_62_c2pnb368w1,
        ECType_NID_X9_62_c2tnb431r1         = NID_X9_62_c2tnb431r1,
        ECType_NID_wap_wsg_idm_ecid_wtls1   = NID_wap_wsg_idm_ecid_wtls1,
        ECType_NID_wap_wsg_idm_ecid_wtls3   = NID_wap_wsg_idm_ecid_wtls3,
        ECType_NID_wap_wsg_idm_ecid_wtls4   = NID_wap_wsg_idm_ecid_wtls4,
        ECType_NID_wap_wsg_idm_ecid_wtls5   = NID_wap_wsg_idm_ecid_wtls5,
        ECType_NID_wap_wsg_idm_ecid_wtls10  = NID_wap_wsg_idm_ecid_wtls10,
        ECType_NID_wap_wsg_idm_ecid_wtls11  = NID_wap_wsg_idm_ecid_wtls11,
        ECType_NID_ipsec3                   = NID_ipsec3,
        ECType_NID_ipsec4                   = NID_ipsec4,
#endif
        ECType_NID_secp112r1                = NID_secp112r1,
        ECType_NID_secp112r2                = NID_secp112r2,
        ECType_NID_secp128r1                = NID_secp128r1,
        ECType_NID_secp128r2                = NID_secp128r2,
        ECType_NID_secp160k1                = NID_secp160k1,
        ECType_NID_secp160r1                = NID_secp160r1,
        ECType_NID_secp160r2                = NID_secp160r2,
        ECType_NID_secp192k1                = NID_secp192k1,
        ECType_NID_secp224k1                = NID_secp224k1,
        ECType_NID_secp224r1                = NID_secp224r1,
        ECType_NID_secp256k1                = NID_secp256k1,
        ECType_NID_secp384r1                = NID_secp384r1,
        ECType_NID_secp521r1                = NID_secp521r1,
        ECType_NID_X9_62_prime192v1         = NID_X9_62_prime192v1,
        ECType_NID_X9_62_prime192v2         = NID_X9_62_prime192v2,
        ECType_NID_X9_62_prime192v3         = NID_X9_62_prime192v3,
        ECType_NID_X9_62_prime239v1         = NID_X9_62_prime239v1,
        ECType_NID_X9_62_prime239v2         = NID_X9_62_prime239v2,
        ECType_NID_X9_62_prime239v3         = NID_X9_62_prime239v3,
        ECType_NID_X9_62_prime256v1         = NID_X9_62_prime256v1,
        ECType_NID_wap_wsg_idm_ecid_wtls6   = NID_wap_wsg_idm_ecid_wtls6,
        ECType_NID_wap_wsg_idm_ecid_wtls7   = NID_wap_wsg_idm_ecid_wtls7,
        ECType_NID_wap_wsg_idm_ecid_wtls8   = NID_wap_wsg_idm_ecid_wtls8,
        ECType_NID_wap_wsg_idm_ecid_wtls9   = NID_wap_wsg_idm_ecid_wtls9,
        ECType_NID_wap_wsg_idm_ecid_wtls12  = NID_wap_wsg_idm_ecid_wtls12
    };
    
    class ERTEccKey
    {
    public:
        
#ifndef _UNSUPPORT_C11
        ERTEccKey() = default;
#else
        ERTEccKey();
#endif
        
        ~ERTEccKey();
        
        ERTUserData p;
        ERTUserData a;
        ERTUserData b;
        ERTUserData n;
        
        ERTUserData Gx;
        ERTUserData Gy;
        ERTUserData k;
        ERTUserData Kx;
        ERTUserData Ky;
        void clear();
        
        // 所有信息保存成连续内存数据
        ERTUserData save();
        
        // 从连续内存数据中读取到所有信息
        bool read(ERTUserData data);
    private:
        ERTUserData colOne(const ERTUserData &pra);
        bool UnColOne(ERTUserData &res, ERTUserData &pra);
    };
public:
    ERTCryptoSM2();
    ERTCryptoSM2(const ERTCryptoSM2 &sm2);
#ifndef _UNSUPPORT_C11
    ERTCryptoSM2(ERTCryptoSM2 &&sm2);
#endif
    
    ~ERTCryptoSM2();
    
    // 设置椭圆参数
    bool setEcc(ERTUserData p, ERTUserData a, ERTUserData b, ERTUserData Gx, ERTUserData Gy, ERTUserData n, Type type);
    
    bool generateKey();

    
    /**
     *  设置私钥
     *  @param k 私钥字节值，需先生成椭圆否则生产国密推荐曲线椭圆
     *  @param M 原始消息M
     *  @param ID_A 用户ID标识
     *  @return 签名结果数据，数据为空则签名失败
     */
    void setPrikey(ERTUserData k);
    
    /**
     *  设置公钥
     *  @param Kx 公钥x坐标字节值，需先生成椭圆否则生产国密推荐曲线椭圆
     *  @param Ky 公钥y坐标字节值，需先生成椭圆否则生产国密推荐曲线椭圆
     */
    void setPublicKey(ERTUserData Kx, ERTUserData Ky);
    
    /**
     *  获取EC key的 基点和私钥, 公钥值
     *  @param ertKey key大数信息
     *  @return 执行结果是否成功
     */
    bool getGD(ERTEccKey &ertKey);
    
    /**
     *  签名函数        (私钥签名， 必须有私钥，和完整椭圆参数)
     *  @param M 消息M
     *  @param ID_A 用户ID标识
     *  @return 签名结果数据，数据为空则签名失败
     */
    ERTUserData sign(const ERTUserData &M, const ERTUserData &ID_A=ERTUserData(defaultIDA, 16));
    
    /**
     *  验签函数        (公钥验签， 必须有公钥钥， 一般此处不存在私钥)
     *  @param sign 消息签名
     *  @param M 原始消息M
     *  @param ID_A 用户ID标识
     *  @return 签名结果数据，数据为空则签名失败
     */
    bool verify(const ERTUserData &sign, const ERTUserData &M, const ERTUserData &ID_A=ERTUserData(defaultIDA, 16));
    
    // 加解密函数（公钥加密，私钥解密）
    /**
     *  公钥加密
     *  @param in 要进行加密的数据,
     *  @param in_len 要进行加密的数据长度
     *  @param encryptedData 加密后的数据
     *  @return 加密结果是否成功
     */
    bool pubKeyEncrypt(const unsigned char *in, int in_len, ERTUserData &encryptedData);
    
    /**
     *  私钥解密
     *  @param in 要进行解密的数据,
     *  @param in_len 要进行解密的数据长度
     *  @param decryptedData 加解密的数据
     *  @return 解密结果是否成功
     */
    bool privDecrypt(const unsigned char *in, int in_len, ERTUserData &decryptedData);

    // 拷贝函数
    ERTCryptoSM2 &operator = (const ERTCryptoSM2 &sm2);
    // 右值拷贝函数 TODO
    
    /**
     *  转换der编码的x509证书格式数据
     *  @param x509 设置好证书信息的x509结构（有效期，颁发着，授权者....）
     *  @discussion 如果x509种设置了私钥则使用其中的私钥签发此证书，否则制作自签名证书
     *  @return 证书数据
     */
    ERTUserData toDerData(ERTCryptoX509 *x509);
   
    /**
     *  转换成EVP_KEY
     */
    EVP_PKEY *toEVPKey();
    const EC_KEY *getKey();
    
    // 从der编码的标准x509证书数据读取密钥信息（公钥）
    bool fromDer(const ERTUserData &der);
    
    bool fromKey(ERTEccKey &ertKey);
    
    bool setECKey(EC_KEY *key);
public:
    // ASN1 BIT STRING 格式 04 xxxkey SM2规范格式（仅试用国密推荐规范）
    
    /**
     *  获取公钥数据 参见SM2密码算法使用规范7.1
     */
    ERTUserData getASN1PublicKey();
    
    /**
     *  设置公钥数据 参见SM2密码算法使用规范7.1
     *  @param key getASN1PublicKey获取公钥得到的数据
     */
    bool setASN1PublicKey(const ERTUserData &key);
    
    /**
     *  获取私钥数据 参见SM2密码算法使用规范7.1
     */
    ERTUserData getASN1PrivateKey();
    
    /**
     *  设置私钥数据 参见SM2密码算法使用规范7.1
     *  @param key getASN1PrivateKey获取公钥得到的数据
     */
    bool setASN1PrivateKey(const ERTUserData &key);
    
    /**
     *  获取密钥对保护数据 参见SM2密码算法使用规范7.4
     *  @param keyOut 外部SM2
     *  @param keySM4 对称密码算法密钥
     */
    ERTUserData getEncryptKey(ERTCryptoSM2 *keyOut, unsigned char keySM4[16]);
    
    
    /**
     *  获取密钥对保护数据 参见SM2密码算法使用规范7.4
     *  @param keyData getEncryptKey 得到的密钥对保护数据
     *  @param keyOut 外部SM2
     */
    bool fromEncryptKey(const ERTUserData &keyData, ERTCryptoSM2 *keyOut);
    
public:
    // 04 xxxkey SM2规范格式（仅试用国密推荐规范）
    /**
     *  获取公钥数据
     */
    ERTUserData getPublicKey();
    
    /**
     *  设置公钥数据
     *  @param key getPublicKey获取公钥得到的数据
     */
    bool setPublicKey(const ERTUserData &key);
    
public:
    /**
     *  从规范加密结果中转换C1X，C1Y,C2,C3数据
     *  @param enData 公钥加密的结果（输入）
     *  @param C1X C1的X坐标32字节（输出）
     *  @param C1Y C1的Y坐标32字节（输出）
     *  @param C2 C2数据（输出）
     *  @param C3 C3数据（输出）
     *  @return 是否转换成功
     */
    static bool sm2EncryptedDataToC1C2C3(const ERTUserData &enData, ERTUserData &C1X, ERTUserData &C1Y, ERTUserData &C2, ERTUserData &C3);
    // 生成EC
    static ERTCryptoSM2 *generate(ECType type);
    static ERTCryptoSM2* free(ERTCryptoSM2 *&sm2);
    
private:
    // 加解密函数（公钥加密，私钥解密）(sm2规范)
    bool pubKeyEncryptStandard(const unsigned char *in, int in_len, ERTUserData &encryptedData);
    bool privDecryptStandard(const unsigned char *in, int in_len, ERTUserData &decryptedData);
    
    // 签名函数(sm2规范)
    ERTUserData signStandard(const ERTUserData &M, const ERTUserData &ID_A=ERTUserData(defaultIDA, 16));
    bool verifyStandard(const ERTUserData &sign, const ERTUserData &M, const ERTUserData &ID_A=ERTUserData(defaultIDA, 16));
    
private:
    typedef EC_GROUP *(*EC_GROUP_new_curve_Func)(const BIGNUM *p, const BIGNUM *a, const BIGNUM *b, BN_CTX *ctx);
    
    typedef int (*EC_POINT_set_compressed_coordinates_Func)(const EC_GROUP *group, EC_POINT *p, const BIGNUM *x, int y_bit, BN_CTX *ctx);
    
    typedef int (*EC_POINT_get_affine_coordinates_Func)(const EC_GROUP *group, const EC_POINT *p, BIGNUM *x, BIGNUM *y, BN_CTX *ctx);
    
    typedef int (*EC_POINT_set_affine_coordinates_Func)(const EC_GROUP *group, EC_POINT *p, const BIGNUM *x, const BIGNUM *y, BN_CTX *ctx);
    
    typedef int (*EC_GROUP_get_curve_GFp_Func)(const EC_GROUP *group, BIGNUM *p, BIGNUM *a, BIGNUM *b, BN_CTX *ctx);
    
private:
    bool creatEcc(Type type, int bit);
    BIGNUM *getRandom();
    void setType(Type type);
    static void freeBigNum(BIGNUM *&num);
    static void freeEcPoint(EC_POINT *&point);
    static void outPutBigNumVal16(BIGNUM *num);
    void getXYfromPoint(const EC_POINT *point, BIGNUM *X, BIGNUM *Y);
    // 计算ZA
    ERTUserData calZA(const ERTUserData &ID_A);
    
    void outPutPoint16Val(EC_POINT *point);
    bool pramFromGroup();
private:
    // ecc 曲线参数
    BN_CTX *ctx_;
    // 参数abp确定曲线
    BIGNUM *p_;  // 参数p （mod p）
    BIGNUM *a_;  // 参数a
    BIGNUM *b_;  // 参数b
    BIGNUM *n_;  // n为p的阶
    BIGNUM *Gx_;  // 基点G
    BIGNUM *Gy_;  // 基点G
    EC_GROUP *group_;    // group 离散椭圆
    
    Type type_;
    
    EC_KEY *key_;
    EC_GROUP_new_curve_Func ecGroupNew;
    EC_POINT_set_compressed_coordinates_Func ecPointSetCompressedCoordinates;
    EC_POINT_get_affine_coordinates_Func ecPointGetAffineCoordinates;
    EC_POINT_set_affine_coordinates_Func ecPointSetAffineCoordinates;
    EC_GROUP_get_curve_GFp_Func ecGroupGetCurve;
};

/**
 *  通过数据获取预置资源加解密iv
 *  @param data1 原始数据1
 *  @param data2 原始数据2
 *  @param data3 原始数据3
 *  @param key 计算出的key（32字节）
 *  @param iv 计算出的iv（16字节）
 */

void camstvn(const ERTUserData &data1, const ERTUserData &data2, const ERTUserData &data3, ERTUserData &key, ERTUserData &iv);

#endif /* defined(__ERTCore__ERTCryptoSM2__) */
