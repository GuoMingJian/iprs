


#ifndef _VECTOR_DATA_H_
#define _VECTOR_DATA_H_

#ifdef _UNSUPPORT_C11
#ifndef nullptr
#define nullptr 0
#endif
namespace std
{
    template<typename T>
    T move(const T &t)
    {
        return t;
    }
};

#endif

template <typename TYPE, typename ARG_TYPE = const TYPE &, typename NUM_TYPE = long long>
class vector_data
{
public:
    typedef NUM_TYPE num_type;
public:
	vector_data(num_type block = 10)
	{
		if(block < 1)
			block = 1;
		m_Block = block;
		m_Size = 0;
		m_Count = 0;
		m_Data = 0;
	}
	vector_data(const vector_data &vec)
	{
		m_Block = vec.m_Block;
		m_Size = 0;
		m_Count = 0;
		m_Data = 0;
		*this = vec;
    }
    
#ifndef _UNSUPPORT_C11
    vector_data(vector_data &&vec)
    {
        m_Size		= vec.m_Size;
        m_Data		= vec.m_Data;
        m_Block	= vec.m_Block;
        m_Count	= vec.m_Count;
        vec.m_Size		= 0;
        vec. m_Data	= nil;
        vec.m_Count	= 0;
    }
#endif
    
	virtual ~vector_data()
	{
		reset();
	}
	vector_data &operator =(const vector_data &vec)
	{
		if(this != &vec)
		{
			resize(vec.m_Count);
			m_Count = vec.m_Count;
			for(num_type i = 0; i < m_Count; i++)
				m_Data[i] = vec.m_Data[i];
		}
		return *this;
	}
    
#ifndef _UNSUPPORT_C11
	vector_data &operator =(vector_data &&vec)
	{
		if(this != &vec)
		{
			//
			reset();
			m_Size		= vec.m_Size;
			m_Data		= vec.m_Data;
			m_Block	= vec.m_Block;
			m_Count	= vec.m_Count;

			 vec.m_Size		= 0;
			 vec. m_Data	= nil;
			 vec.m_Count	= 0;
		}
		return *this;
	}
#endif
    
    bool operator == (const vector_data &vec) const
    {
        if(m_Count != vec.m_Count)
            return false;
        
        for(num_type nIndex = 0; nIndex < m_Count; nIndex++)
        {
            if(m_Data[nIndex] != vec.m_Data[nIndex])
                return false;
        }
        
        return true;
    }
    
    bool operator != (const vector_data &vec) const
    {
        return !(*this == vec);
    }


	TYPE &operator [](const num_type &nIndex)
	{
		return m_Data[nIndex];
	}

	const TYPE &operator [](const num_type &nIndex) const
	{
		return m_Data[nIndex];
	}
    
    vector_data operator+(const vector_data &vec)const
    {
        vector_data vecNew;
        vecNew.append(*this);
        vecNew.append(vec);
        return vecNew;
    }

	void resize(num_type size)
	{
		if(m_Size >= size)
			return;
		size = (size + m_Block - 1) / m_Block * m_Block;
		if(size < m_Count * 2)
			size *= 2;
		TYPE *data = new TYPE[size];
		for(num_type i = 0; i < m_Count; i++)
			data[i] = m_Data[i];
		delete []m_Data;
		m_Data = data;
		m_Size = size;
	}
	void reset()
	{
		if(m_Size > 0)
		{
			delete []m_Data;
			m_Data = 0;
			m_Count = 0;
			m_Size = 0;
		}
	}
	void clear()
	{
		reset();
	}
	void append(ARG_TYPE elem)
	{
		resize(m_Count + 1);
		m_Data[m_Count++] = elem;
    }
    
    void append(ARG_TYPE elem, num_type n)
    {
        resize(m_Count + n);
        for(num_type i=0; i<n; i++)
            m_Data[m_Count++] = elem;
    }
    
	void append(const TYPE *elem, num_type n)
	{
		if(n < 1)
			return;
		resize(m_Count + n);
		if(elem)
		{
			for(num_type i = 0; i < n; i++)
				m_Data[m_Count + i] = elem[i];
		}
		m_Count += n;
	}

	void append(const vector_data &vec)
	{
		const num_type count = vec.count();
		if(count < 1)
			return;
		resize(m_Count + count );

			for(num_type i = 0; i < count; i++)
				m_Data[m_Count + i] = vec[i];
		m_Count += count;
	}
    void fill(ARG_TYPE elem, num_type nNum)
    {
        if(nNum <0)
            return;
        
        clear();
        resize(nNum);
        
        for(num_type i = 0; i < nNum; i++)
            m_Data[i] = elem;
        m_Count = nNum;
    }
	void erasure(num_type nIndex)
	{
		vector_data vec;
		vec = *this;
		reset();
		for(num_type i=0;i<vec.m_Count;i++)
		{
			if(i==nIndex)
				continue;
			append(vec.m_Data[i]);
		}
	}
    
    vector_data reverse()const
    {
        vector_data tmp;
        tmp.resize(m_Size);
        for(num_type i=m_Count-1; i>=0; i--)
        {
            tmp.append(m_Data[i]);
        }
        return tmp;
    }

	TYPE *getData()
	{
		return m_Data;
	}
    
    const TYPE *getData() const
    {
        return m_Data;
    }
	num_type count()
	{
		return m_Count;
	}
	const num_type count() const
	{
		return m_Count;
	}
    num_type size() const
    {
        return m_Count;
    }
    bool isEmpty()
    {
        if(m_Count>0)
            return false;
        return true;
    }
private:
    num_type m_Block;
	num_type m_Size;
	num_type m_Count;
	TYPE *m_Data;
};

#endif //_VECTOR_DATA_H_
