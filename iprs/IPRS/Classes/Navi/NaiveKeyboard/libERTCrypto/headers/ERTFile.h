
///////////////////////////////////////////////////////////
// ERTFile.h
// ERTFile
// Created by wu.linchen on 2014-11-4.
// Copyright (c) 2014 rytong. All rights reserved.
///////////////////////////////////////////////////////////




#ifndef _ERT_FILE_H
#define _ERT_FILE_H
#include <string>
#include "ERTUserData.h"

class ERTFile
{
public:
	enum enFileMode
	{
		enFileMode_Replace	= 0x00001,	// if file exist replease if
		enFileMode_App		= 0x00002	// if file exist append to the end
	};

private:
	ERTFile();
	~ERTFile();
public:
	// read file as string ,note: the char* may be change by the  object,pelease save it to your memory;
	static ERTUserData readFile(const char *filePathName);
    static std::string readFile(std::string filePathName);
	static bool readFile(const char *filePathName, ERTUserData &data);
	static bool readFileBinary(const char *filePathName, ERTUserData &data);

	// output file
	static bool writeFile(const char* filePathName, const char *ch, enFileMode mode = enFileMode_Replace);
	static bool writeFileBinary(const char* filePathName, const char *ch, long size, enFileMode mode = enFileMode_Replace);

	// To determine whether a file or directory exists
	static bool isExistFile( const char* filePath);

	// delete the file
	static bool removeFile(const char* filePath);
};

#endif