//
//  ERTCryptoSM4.h
//  ERTCore
//
//  Created by rytong on 15/2/3.
//  Copyright (c) 2015年 rytong. All rights reserved.
//

#ifndef __ERTCore__ERTCryptoSM4__
#define __ERTCore__ERTCryptoSM4__

#include <stdio.h>
#include "ERTUserData.h"

//  密钥为128位即16字节固定，IV16字节固定，区域块待填充
class ERTCryptoSM4
{
public:
    enum SM4Type
    {
        SM4TypeECB,
        SM4TypeCBC
    };
    
public:
    ERTCryptoSM4(SM4Type type = SM4TypeECB);
    ~ERTCryptoSM4();
    
    void setKey(unsigned char key[16]);
    void setSM4Type(SM4Type type);
    void setIV(unsigned char iv[16]);
    /**
     *  加密数据
     *  @param stringIn 要进行加密的数据,
     *  @param length 要进行加密的数据长度
     *  @param encrypedData  加密后的数据
     *  @return 加密结果是否成功
     */
    bool encryptData(const unsigned char *stringIn, const unsigned int length, ERTUserData &encrypedData);
    
    /**
     *  加密数据
     *  @param stringIn 要进行解密的数据,
     *  @param length 要进行解密的数据长度
     *  @param decryptedData 解密后的数据
     *  @return 解密结果是否成功
     */
    bool decryptData(const unsigned char *stringIn, const unsigned int length, ERTUserData &decryptedData);
    
    
    /**
     *  获取到当前状态下的IV数据
     *  @return IV
     */
    ERTUserData getCurrentIV();
    
private:
    // 反序变化R
    void R(unsigned int *rk, int num);
    
#ifdef __PerformanceTest_Function
public:
#endif
    
    // 计算顺序轮密钥
    void calrk();
    
    void oneRound(const unsigned char inPut[16], unsigned char outPut[16]);
    
#ifdef __PerformanceTest_Function
    void oneRound2(const unsigned char input[16], unsigned char output[16]);
#endif
private:
    /**
     *  准备加密数据PKCS7方式
     *  @param length 要进行解密的数据长度,
     *  @param stringIn 要进行解密的数据
     *  @param temp 获得的填充完毕的数据
     *  @return 填充完毕后的数据长度
     *  @discussion 数据块采用PKCS7方式
     */
    long prePKCS7(const unsigned int length, const unsigned char *stringIn, ERTUserData &temp);
    bool deprocPKCS7(ERTUserData &data);

private:
    unsigned char key_[16]; // 密钥固定为128bit
    
    unsigned int rk_[32];
    SM4Type type_;
    unsigned char iv_[16];
    
};

#endif /* defined(__ERTCore__ERTCryptoSM4__) */
