//
//  ERTLog.h
//  ERTCore
//
//  Created by zhang.jian on 14-7-15.
//  Modified by li.hao85 on 14-11-12.
//  Copyright (c) 2014 rytong. All rights reserved.
//

#ifndef __ERTCore__ERTLog_H__
#define __ERTCore__ERTLog_H__

#include <time.h>
#include <string.h>
#include <stdarg.h>

/**
 * Return the base name from a file name. Don't do this if we
 * are doing a debugging build.
 */
inline const char *
strip_dirname(const char *fname)
{
	/*This works in windows & linux.*/
	const char *p = strrchr(fname, '/');
    return p ? ++p : fname;
}

// use android log.
#ifdef ANDROID_LOG
#include "android/log.h"
#define ALog(...) __android_log_print(ANDROID_LOG_DEBUG, "ERTCore", __VA_ARGS__);
#define AError(...) __android_log_print(ANDROID_LOG_ERROR, "ERTCore", __VA_ARGS__);
#define AAssert(...) __android_log_print(ANDROID_LOG_WARN, "ERTCore", __VA_ARGS__);
#endif

//#define TEST_MESSAGE

/*__PRETTY_FUNCTION__ is not defined in windows, so define it.*/
#ifndef __PRETTY_FUNCTION__
#define __PRETTY_FUNCTION__ __FUNCTION__
#endif

/*ERTLog.*/
#ifdef ANDROID_LOG
#define ERTLog(fmt, ...) \
do { \
if(1) { \
    ALog("%s:%d %s " fmt"\n", strip_dirname(__FILE__), __LINE__, __PRETTY_FUNCTION__, ##__VA_ARGS__); \
} \
} while (0);

#define ERTErrorLog(fmt, ...) \
do { \
if(1) { \
AError("%s:%d %s " fmt"\n", strip_dirname(__FILE__), __LINE__, __PRETTY_FUNCTION__, ##__VA_ARGS__); \
}\
} while (0);

#define ERTAssert(expr) \
do { \
if (!(expr)) { \
AAssert("%s:%d assertion " #expr " failed in function %s\n", strip_dirname(__FILE__), __LINE__, __PRETTY_FUNCTION__); \
} \
} while (0);

#else

#define ERTLog(fmt, ...) \
do { \
if(1) {\
    time_t t = time( 0 ); \
    char tmp[64]; \
    tmp[24] = '\0'; \
    strftime(tmp, sizeof(tmp), "%Y-%m-%d %X %z", localtime(&t)); \
    fprintf(stderr, "%s ERTCore %s:%d %s " fmt"\n", tmp, strip_dirname(__FILE__), __LINE__, __PRETTY_FUNCTION__, ##__VA_ARGS__); \
}\
} while (0);


#define ERTErrorLog(fmt, ...) \
do { \
if(1) {\
time_t t = time( 0 ); \
char tmp[64]; \
tmp[24] = '\0'; \
strftime(tmp, sizeof(tmp), "error:%Y-%m-%d %X %z",localtime(&t)); \
fprintf(stderr, "%s ERTCore %s:%d %s " fmt"\n", tmp, strip_dirname(__FILE__), __LINE__,  __PRETTY_FUNCTION__, ##__VA_ARGS__); \
}\
} while (0); 


#define ERTAssert(expr) \
do { \
if (!(expr)) { \
fprintf(stderr, "%s:%d assertion " #expr " failed in function %s\n", strip_dirname(__FILE__),  __LINE__, __PRETTY_FUNCTION__); \
} \
} while (0);

#endif

#endif /* defined(__ERTCore__ERTLog__) */
