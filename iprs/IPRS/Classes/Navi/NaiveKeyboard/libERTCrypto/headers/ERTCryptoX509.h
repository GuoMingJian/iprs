//
//  ERTCryptoX509.h
//  ERTCore
//
//  Created by wu.linchen on 15/1/16.
//  Copyright (c) 2015年 rytong. All rights reserved.
//

#ifndef __ERTCore__ERTCryptoX509__
#define __ERTCore__ERTCryptoX509__

#include <openssl/x509.h>
#include <openssl/evp.h>
#include <openssl/pem.h>
#include "ERTUserData.h"

class ERTCryptoSM2;

class ERTCryptoX509
{
public:
    enum DigestAlgorithm
    {
        DigestAlgorithm_Unknow,
        DigestAlgorithm_MD5,
        DigestAlgorithm_SHA,
        DigestAlgorithm_SHA1
    };
public:
    ERTCryptoX509();
    ~ERTCryptoX509();
    
public:
    // 静态功能函数 （添加跟证书，验证证书等函数）
    /**
     *  证书存储区中添加证书
     *  @param X509 证书
     *  @return 是否添加成功
     */
    static bool addCert(X509 *ca);
    
    /**
     *  证书存储区中添加证书
     *  @param cerFile cer文件路径
     *  @return 是否添加成功
     */
    static bool addCert(const char *cerFile);
    
    /**
     *  der格式文件数据转换成X509
     *  @param cer cer数据
     *  @return X509
     */
    static bool verify(X509 *bVerify);
    static void cleanX509(X509 *&x509);
    
    static X509 *derDataToX509(const ERTUserData &cer);
    
    
    /**
     *  初始化证书链
     */
    static void initX509Store();
    
    /**
     *  释放证书链
     */
    static void releaseX509Store();
    
    /**
     *  检查证书链是否正确初始化（未初始化的自动初始化）
     */
    static void checkX509Store();
    // 设置证书校验时使用的时间
    static void setCheckTime(time_t time);

    
public:
    // X509证书制作
    
    ERTUserData rsa2der(RSA* key);

    ERTUserData ecc2der(EC_KEY* key);
    
    ERTUserData toDer();
    
    // 设置版本
    int setVersion(long version);
    // 设置序列号
    int setSeriaNumber(long num);
    
    // 颁发者
    int setIssuerName(std::string key, std::string val);
    
    // 颁发给
    int setSubjectName(std::string key, std::string val);
    
    // 有效期起始时间
    int setNotBefore(time_t notBefore);
    
    // 有效期截止时间
    int setNotAfter(time_t notAfter);
    
    void setPrivateKey(const EC_KEY *key);
    void setPrivateKey(RSA *key);
    void setPublicKey(EC_KEY *key);
    void setPublicKey(RSA *key);
    
    void clearIssuerName();
    void clearSubjectName();
    
    bool mkcert();
    void printFp(const char* fileName);
    
    // 设置指纹算法
    void setDigestAlgorithm(DigestAlgorithm am);
    
private:
    int add_ext(int nid, char *value);
    
    void processSM2();
    static bool sm2RootVerifyCer(X509 *root, X509 *cer);
private:
    DigestAlgorithm digestAlgorithm_;
    X509 *x509_;
    // 私钥，用于签名
    EVP_PKEY *privateKey_;
    
    // 证书中的公钥
    EVP_PKEY *publicKey_;
    
private:
    
    static X509_STORE *x509store_;
    // 证书校验时使用的时间
    static time_t checkTime_;

};

#endif /* defined(__ERTCore__ERTCryptoX509__) */
