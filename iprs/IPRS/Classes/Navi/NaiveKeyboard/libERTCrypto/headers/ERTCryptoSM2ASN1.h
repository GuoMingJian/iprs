//
//  ERTCryptoSM2ASN1.h
//  ERTCore
//
//  Created by wu.linchen on 15/12/04
//  Copyright (c) 2015年 rytong. All rights reserved.
//

#ifndef __ERTCryptoSM2ASN1__
#define __ERTCryptoSM2ASN1__

#include "ERTCryptoX509.h"
#include <openssl/asn1.h>
#include <openssl/asn1t.h>


// 规范编码 签名数据结构
typedef struct SM2Signature_st
{
    ASN1_INTEGER *inr;
    ASN1_INTEGER *ins;
}SM2Signature;

DECLARE_ASN1_FUNCTIONS(SM2Signature)



// 规范编码 加密数据结构
typedef struct SM2Cipher_st
{
    ASN1_INTEGER        *XCoordinate;
    ASN1_INTEGER        *YCoordinate;
    ASN1_OCTET_STRING   *Hash;
    ASN1_OCTET_STRING   *C;
}SM2Cipher;

DECLARE_ASN1_FUNCTIONS(SM2Cipher)



typedef struct SM2EnvelopedKey_st
{
    X509_ALGOR *symAlgID;
    SM2Cipher *symEncryptedKey;
    ASN1_BIT_STRING *Sm2PublicKey;
    ASN1_BIT_STRING *Sm2EncryptedPrivateKey;
}SM2EnvelopedKey;

DECLARE_ASN1_FUNCTIONS(SM2EnvelopedKey)

#endif /* defined(__ERTCryptoSM2ASN1__) */
