///////////////////////////////////////////////////////////
// ERTCryptoHMAC.h
// ERTCryptoHMAC
// Created by wu.linchen on 2014-11-26.
// Copyright (c) 2014 rytong. All rights reserved.
///////////////////////////////////////////////////////////


#ifndef _ERTCRYPTO_HMAC_H_
#define _ERTCRYPTO_HMAC_H_

#include "ERTCryptoDigest.h"
#include <openssl/hmac.h>
class ERTCryptoHMAC :
	public ERTCryptoDigest
{
public:
	enum enEVP_MD
	{
		EVP_MD_md4,
		EVP_MD_md5,
		EVP_MD_sha,
		EVP_MD_sha1,
		EVP_MD_sha224,
		EVP_MD_sha256,
		EVP_MD_sha384,
		EVP_MD_sha512,
        EVP_MD_sm3
	};
public:
	ERTCryptoHMAC();
	~ERTCryptoHMAC();

    /**
    *  设置密钥
    *  @param key 密钥,
    *  @param length 密钥长度
    *  @discussion 密钥将参加散列算法运算，影响获取的摘要结果
    */
	void setKey(const char *key, const int length);
    
    /**
     *  设置密钥
     *  @param key 密钥
     *  @discussion 密钥将参加散列算法运算，影响获取的摘要结果
     */
    void setKey(const ERTUserData& key);

    /**
    *  设置散列算法 及Engine
    *  @param md 散列算法,
    *  @param impl ENGINE
    *  @discussion 散列算法决定摘要长度，engine参数一般不用设置。
    *  @note 当编译器支持C++11时置换NULL为nullptr关键字
    */
	void setEvpEngine(enEVP_MD md, ENGINE *impl = NULL);
public:
    /**
    *  从字符串获取消息摘要（二进制数据，非可读字符串）
    *  @param data 二进制数据,
    *  @param datalength 二进制数据长度
    *  @return 返回摘要串
    *  @discussion 摘要长度与散列算法有关
    */
	virtual ERTUserData digestFromData(void* data, long length);
private:
    ERTUserData sm3(void* data, long length);
private:
    ERTUserData key_;
	enEVP_MD md_;
	ENGINE* engine_;
};

#endif // _ERTCRYPTO_HMAC_H_