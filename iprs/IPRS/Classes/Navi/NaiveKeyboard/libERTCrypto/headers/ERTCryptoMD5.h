///////////////////////////////////////////////////////////
// ERTCryptoMD5.h
// ERTCryptoMD5
// Created by wu.linchen on 2014-11-26.
// Copyright (c) 2014 rytong. All rights reserved.
///////////////////////////////////////////////////////////


#ifndef _ERTCRYPTO_MD5_H_
#define _ERTCRYPTO_MD5_H_

#include "ERTCryptoDigest.h"

class ERTCryptoMD5 :
	public ERTCryptoDigest
{
public:
	ERTCryptoMD5();
	~ERTCryptoMD5();

public:

    /**
    *  从字符串获取消息摘要（二进制数据，非可读字符串）
    *  @param data 二进制数据,
    *  @param datalength 二进制数据长度
    *  @return 返回摘要串
    *  @discussion 摘要长度与算法有关，均为固定长度如MD5为128位（16字节）
    */
	virtual ERTUserData digestFromData(void *data, long datalength);
};

#endif // _ERTCRYPTO_MD5_H_