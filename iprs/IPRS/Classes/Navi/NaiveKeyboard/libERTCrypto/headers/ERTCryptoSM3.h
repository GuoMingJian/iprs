//
//  ERTEncryptoSM3.h
//  ERTCore
//
//  Created by rytong on 15/2/2.
//  Copyright (c) 2015年 rytong. All rights reserved.
//

#ifndef __ERTCore__ERTEncryptoSM3__
#define __ERTCore__ERTEncryptoSM3__

#include "ERTUserData.h"
#include "ERTCryptoDigest.h"

class ERTCryptoSM3 : public ERTCryptoDigest
{
public:
    ERTCryptoSM3();
    ~ERTCryptoSM3();
    
    
    /**
     *  从数据获取消息摘要（二进制数据，非可读字符串）
     *  @param data 二进制数据,
     *  @param datalength 二进制数据长度
     *  @return 返回摘要串
     *  @discussion 摘要长度与算法有关，均为固定长度如SM3为256位（32字节）
     */
    virtual ERTUserData digestFromData(void *data, long datalength);
    
    // 多段数据使用此方法，每次调用update，最后调用finish获得杂凑值
    void update(unsigned char *input, long len);
    ERTUserData finish();
private:
    void CF(unsigned char *Bi);
    void init();
    // FFj函数
    static unsigned long FF(const unsigned long &X,const unsigned long &Y,const unsigned long &Z, const unsigned long &j);
    // GGj 函数
    static unsigned long GG(const unsigned long &X,const unsigned long &Y,const unsigned long &Z, const unsigned long &j);
    
    //  循环左移函数 32位
    static unsigned long RotLeft(unsigned long X, unsigned k);
    
    static unsigned long P0(unsigned long X);
    
    static unsigned long P1(unsigned long X);
private:
    unsigned long msgLength_[2];
    unsigned long iv_[8];
    unsigned char buffer_[64];
    static unsigned long Tj_[64];
};

#endif /* defined(__ERTCore__ERTEncryptoSM3__) */
