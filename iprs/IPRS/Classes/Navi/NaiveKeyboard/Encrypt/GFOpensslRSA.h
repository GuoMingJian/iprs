//
//  RSA.h
//  RYTong
//
//  Created by Zheng Bing on 4/30/09.
//  Copyright 2009 RYTong. All rights reserved.
//


#import <Security/Security.h>

/** RSA 加密
 */

@interface GFOpensslRSA : NSObject {

}

/** return a instance of HTTPManager
 */
+ (GFOpensslRSA *) sharedOpensslRSA;


/** 用公钥进行RSA加密
 
 @param src 需要加密的内容，类型 NSString 或 NSData
 @return 返回加密数据，这个加密数据是做过Base64编码的
 @discussion 填充 kSecPaddingPKCS1
 */
- (NSData *) doRSAEncryptNoPadding:(id)src publicKey:(NSString *)publicKey;



@end
