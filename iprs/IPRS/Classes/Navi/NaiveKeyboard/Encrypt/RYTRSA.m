//
//  RSA.m
//  RYTong
//
//  Created by Zheng Bing on 4/30/09.
//  Copyright 2009 RYTong. All rights reserved.
//

#import "RYTRSA.h"

#import "Base64.h"

@implementation RYTRSA


/** singleton instance  **/
static RYTRSA                  *sharedRSA = nil;

+ (RYTRSA *) sharedRSA {
    @synchronized ([RYTRSA class]) {
        if (sharedRSA == nil) {
            [[RYTRSA alloc] init];
            
            return sharedRSA;
        }
    }
    
    return sharedRSA;
}


+ (id) alloc {
    @synchronized ([RYTRSA class]) {
        sharedRSA = [super alloc];
        return sharedRSA;
    }
    
    return nil;
}


- (id) init {
    
    self = [super init];
    
    if (self) {
        //publicKey_ = [self getPublicKeyRef:@"rytong" type:@"cer"];
    }
    
    
    return self;
}

- (void) dealloc {
    if (publicKey_)
        CFRelease(publicKey_);
//    [super dealloc];
}

+ (NSString *) removeRN:(NSString *)src {
    return [src stringByReplacingOccurrencesOfString:@"\r\n" withString:@""];
}

- (NSString *) encryptRSAWithPublicKey:(id)src {
    
    NSData *cipher = [self doRSAEncrypt:src];
    
    NSString *rsaT = [[NSString alloc] initWithData:cipher encoding:NSUTF8StringEncoding];
        
    NSString *string = [Base64 stringByEncodingData:cipher];
    string = [RYTRSA removeRN:string];
    
//    [rsaT release];
    
    return string;
}

- (NSData *) doRSAEncrypt:(id)src {
    NSData *result = [self doEncrypt:src withPadding:kSecPaddingPKCS1];
    return result;
}

- (NSData *) doRSAEncryptNoPadding:(id)src {
    NSData *result = [self doEncrypt:src withPadding:kSecPaddingNone];
    return result;
}

- (NSData *) doEncrypt:(id)src withPadding:(SecPadding)padding {
    
    OSStatus sanityCheck = noErr;
    size_t cipherBufferSize = 0;
    size_t srcBufferSize = 0;
    
    NSData * cipher = nil;
    uint8_t * cipherBuffer = NULL;
    
    
    NSData *srcData = nil;
    if ([src isKindOfClass:[NSString class]]) {
        srcData = [src dataUsingEncoding:NSUTF8StringEncoding];
    } else {
        srcData = src;
    }
    
    
    // Calculate the buffer sizes.
    cipherBufferSize = SecKeyGetBlockSize(publicKey_);
    srcBufferSize = [srcData length];
    
    cipherBuffer = malloc( cipherBufferSize * sizeof(uint8_t) );
    memset((void *)cipherBuffer, 0x0, cipherBufferSize);
    
    
    
    sanityCheck = SecKeyEncrypt(publicKey_,
                                padding,
                                (const uint8_t *)[srcData bytes],
                                srcBufferSize,
                                cipherBuffer,
                                &cipherBufferSize
                                );
    
    
    cipher = [NSData dataWithBytes:(const void *)cipherBuffer length:(NSUInteger)cipherBufferSize];
    
    if (cipherBuffer)
        free(cipherBuffer);
    
    return cipher;
}

- (void) setPublicKey:(SecKeyRef)publicKey {
	publicKey_ = publicKey;
}


- (SecKeyRef) getPublicKeyRef:(NSString *)name type:(NSString *)type {
    
    NSString *path = [[NSBundle mainBundle] pathForResource:name ofType:type];
    SecKeyRef publicKey = [self getPublicKeyFromCerPath:path];
    return publicKey;
    
}

- (SecKeyRef) getPublicKeyFromCerData:(NSData *) data {
    SecTrustRef trust = [self getSecTrustRefFromCerData:data];
    SecKeyRef publicKey = SecTrustCopyPublicKey(trust);
	CFRelease(trust);
    return publicKey;	
}

- (SecKeyRef) getPublicKeyFromCerPath:(NSString *)path {
    NSData *certData = [[NSData alloc] initWithContentsOfFile:path];
    SecKeyRef sectr = [self getPublicKeyFromCerData:certData];
//    [certData release];
	return sectr;
}

- (SecTrustRef) getSecTrustRefFromCerData:(NSData *)data {
    CFDataRef myCertData = (CFDataRef)CFBridgingRetain(data);
    
    SecCertificateRef myCert; 
    myCert = SecCertificateCreateWithData(NULL, myCertData);
    
    
    /////////////////////////////////  Ago  /////////////////////////////////////////////
    SecPolicyRef myPolicy = SecPolicyCreateBasicX509();
    SecCertificateRef certArray[1] = { myCert };
    CFArrayRef myCerts = CFArrayCreate(NULL, (void *)certArray, 1, NULL);
    SecTrustRef myTrust;
    OSStatus status = SecTrustCreateWithCertificates(myCerts, myPolicy, &myTrust);
    
    
    
    SecTrustResultType trustResult;
    if (status == noErr) {
        status = SecTrustEvaluate(myTrust, &trustResult);
    }
    
    /////// ***** Get time used to verify trust   ******//////
    //CFAbsoluteTime trustTime,currentTime,timeIncrement,newTime; 
    //    CFDateRef newDate; 
    //    if (trustResult == kSecTrustResultRecoverableTrustFailure) {
    //        trustTime = SecTrustGetVerifyTime(myTrust);             
    //        timeIncrement = 31536000;                               
    //        currentTime = CFAbsoluteTimeGetCurrent();               
    //        newTime = currentTime - timeIncrement;                  
    //        if (trustTime - newTime){                               
    //            newDate = CFDateCreate(NULL, newTime);              
    //            SecTrustSetVerifyDate(myTrust, newDate);
    //            status = SecTrustEvaluate(myTrust, &trustResult);   
    //        }
    //    }
    //    
    //    if (trustResult != kSecTrustResultProceed) {
    //    }
    /////////////////////////////////////////////////////////////////
    
    
    if (myPolicy) {
        CFRelease(myPolicy);
    }
    
    if (myCert) {
        CFRelease(myCert);
    }
    
    CFRelease(myCerts);
    
    return myTrust;
}


@end
