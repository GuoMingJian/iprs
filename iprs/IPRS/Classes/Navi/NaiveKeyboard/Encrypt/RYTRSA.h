//
//  RSA.h
//  RYTong
//
//  Created by Zheng Bing on 4/30/09.
//  Copyright 2009 RYTong. All rights reserved.
//


#import <Security/Security.h>

/** RSA 加密
 */

@interface RYTRSA : NSObject {
    // RSA 加密公钥
    SecKeyRef                           publicKey_;
}

/** return a instance of HTTPManager
 */
+ (RYTRSA *) sharedRSA;

/** 删除字符串中的‘\r\n’
 
 @param src 待处理的字符串
 @return 返回处理后的字符串
 */
+ (NSString *) removeRN:(NSString *)src;

/** 用公钥进行RSA加密
 
 @param src 需要加密的内容，类型 NSString 或 NSData
 @return 返回加密数据，这个加密数据是做过Base64编码的
 @discussion 填充 kSecPaddingPKCS1
 */
- (NSString *) encryptRSAWithPublicKey:(id)src;

/** 用公钥进行RSA加密
 
 @param src 需要加密的内容，类型 NSString 或 NSData
 @return 返回加密数据
 @discussion 填充 kSecPaddingPKCS1
 */
- (NSData *) doRSAEncrypt:(id)src;

/** 用公钥进行RSA加密
 
 @param src 需要加密的内容，类型 NSString 或 NSData
 @param padding 填充方式
 @return 返回加密数据
 
 */
- (NSData *) doEncrypt:(id)src withPadding:(SecPadding)padding;

/** 用公钥进行 RSA 加密
 
 @param src 需要加密的内容，类型 NSString 或 NSData
 @param padding 填充方式
 @return 返回加密后的数据
 @discussion 填充 kSecPaddingNone
 */
- (NSData *) doRSAEncryptNoPadding:(id)src;


/** 设置 RSA 公钥
 
 @param publicKey Abstract Core Foundation-type object representing an asymmetric key.
 */
- (void) setPublicKey:(SecKeyRef) publicKey;

/** 从证书文件中获取公钥
 
 @param name 证书文件名
 @param type 文件类型
 @return 获取的公钥
 @discussion 该方法会从NSBundle中查找文件
 @see getPublicKeyFromCerData:
 @see getPublicKeyFromCerPath:
 */
- (SecKeyRef) getPublicKeyRef:(NSString *)name type:(NSString *)type;

/** 从证书中获取公钥
 
 @param data 证书二进制数据
 @return 获取的公钥
 @see getPublicKeyFromCerPath:
 */
- (SecKeyRef) getPublicKeyFromCerData:(NSData *) data;

/** Creates a trust management object based on certificates and policies.
 
 policies is X509.
 
 @param data 证书二进制数据
 @return the trust management
 @see getPublicKeyFromCerPath:
 */
- (SecTrustRef) getSecTrustRefFromCerData:(NSData *) data;

/** 从指定的证书路径中获取公钥
 
 @param path 证书文件所在的路径
 @return 获取的公钥
 @see getPublicKeyFromCerData:
 */
- (SecKeyRef) getPublicKeyFromCerPath:(NSString *)path;

@end
