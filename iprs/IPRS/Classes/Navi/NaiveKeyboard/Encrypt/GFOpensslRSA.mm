//
//  RSA.m
//  RYTong
//
//  Created by Zheng Bing on 4/30/09.
//  Copyright 2009 RYTong. All rights reserved.
//

#import "GFOpensslRSA.h"

#import "Base64.h"
#import "ERTCryptoRSA.h"

static ERTCryptoRSA serverPubRSA_;

@implementation GFOpensslRSA

/** singleton instance  **/
static GFOpensslRSA                  *sharedRSA = nil;

+ (GFOpensslRSA *) sharedOpensslRSA {
    @synchronized ([GFOpensslRSA class]) {
        if (sharedRSA == nil) {
            [[GFOpensslRSA alloc] init];
            
            return sharedRSA;
        }
    }
    
    return sharedRSA;
}


+ (id) alloc {
    @synchronized ([GFOpensslRSA class]) {
        sharedRSA = [super alloc];
        return sharedRSA;
    }
    
    return nil;
}


- (id) init {
    
    self = [super init];
    
    if (self) {
        //publicKey_ = [self getPublicKeyRef:@"rytong" type:@"cer"];
    }
    
    
    return self;
}

- (void) dealloc {
//    [super dealloc];
}

- (NSData *) doRSAEncryptNoPadding:(id)src publicKey:(NSString *)publicKey {
    NSData *enData = nil;
    
    NSData *cerData = [Base64 decodeString:publicKey];
    NSUInteger length = cerData.length;
    unsigned char *cerBytes = (unsigned char *)[cerData bytes];
    bool success = serverPubRSA_.loadPubKey(cerBytes, length);
    
    NSData *srcData = [src dataUsingEncoding:NSUTF8StringEncoding];
    
    length = [srcData length];
    unsigned char cData[length];
    [srcData getBytes:cData length:length];
    ERTUserData encryptedData;
    success = serverPubRSA_.pubkeyEncrypt(cData, (int)length, encryptedData,ERTCryptoRSA::RSA_PADDING_NO);
    if (success) {
        enData = [NSData dataWithBytes:encryptedData.getData() length:encryptedData.count()];
    }
    
    return enData;

}


@end
