//
//  hsmcliCGB.h
//  CGB_Enterprise
//
//  Copyright (c) 2013 北京融易通信息技术有限公司. All rights reserved.
//

#import <Foundation/Foundation.h>

#define PIN_MAX_LENGTH 256

@interface hsmcliCGB : NSObject {
    
}

+ (NSString *) PkEncryptAPin:(NSString *)src crePublickeyData:(NSString *)crePublickeyData accounts:(NSString*)accountNo;
+ (NSString *) PkEncryptEPin:(NSString *)src crePublickeyData:(NSString *)crePublickeyData accounts:(NSString*)accountNo;
+ (BOOL) checkWeakPassword:(NSString *)s;
+ (BOOL) checkComplexPassword:(NSString *)s accounts:(NSString*)accountNo;
@end
