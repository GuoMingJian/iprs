//
//  hsmcliCGB.m
//  CGB_Enterprise
//
//  Copyright (c) 2013 北京融易通信息技术有限公司. All rights reserved.
//

#import "hsmcliCGB.h"
#import "GFOpensslRSA.h"
#import "Base64.h"

@implementation hsmcliCGB

+ (NSString *) genRandomWithLength:(int)count {
	NSMutableString *randomStr = [[NSMutableString alloc] init];
	NSNumber *number = nil;
    for (int i = 0; i < count; i++) {
        NSTimeInterval interval = [[NSDate date] timeIntervalSince1970];
        number = [[NSNumber alloc] initWithDouble:interval];
        int timeValue = [number intValue];
        srandom(time(NULL));
        int _random = arc4random() % (2147483647 - timeValue);
        int ch = (timeValue + _random) % 80;
        if (ch < 2) {
            ch = 2-ch;
        }
        //ZNLog(@"随机数 ： %d", ch);
		[randomStr appendFormat:@"%c", (unsigned char)ch];
//		[number release];
    }
	NSString *temp = [NSString stringWithString:randomStr];
//	[randomStr release];
	
	return temp;
}


+ (NSString *) PkEncryptAPin:(NSString *)src crePublickeyData:(NSString *)crePublickeyData  accounts:(NSString*)accountNo{
    int i = [src length];
	if(i >= PIN_MAX_LENGTH - 2)
		return nil;
	int integer = i / 10 + 48;
	int integer1 = i % 10 + 48;
	unsigned char abyte0[2] = {integer, integer1};
	NSMutableString *mustr = [[NSMutableString alloc] initWithBytes:abyte0 length:2 encoding:NSUTF8StringEncoding];
	[mustr appendString: src];
	[mustr appendString: [self genRandomWithLength:PIN_MAX_LENGTH - i - 2]];
	
    //注释原RAS加密方式 开始
//	NSData *data = [Base64 decodeString:crePublickeyData];
//	SecKeyRef key = [[GFRSA sharedRSA] getPublicKeyFromCerData:data];
//	[[GFRSA sharedRSA] setPublicKey:key];
//	NSData *enData = [[GFRSA sharedRSA] doRSAEncryptNoPadding:mustr];
    
    //	[mustr release];
//    if (key) {
//        CFRelease(key);
//    }
    //注释原RAS加密方式 结束

    NSData *enData = [[GFOpensslRSA sharedOpensslRSA] doRSAEncryptNoPadding:mustr publicKey:crePublickeyData];
    
	//16进制字符
	unsigned char temp[PIN_MAX_LENGTH] = { 0 };
	@try {
		[enData getBytes:&temp range:NSMakeRange(0, PIN_MAX_LENGTH)];
	}
	@catch (NSException * e) {
		
//		ZNLog(@"Error enData length = %d", [enData length]);
		[self PkEncryptAPin:src crePublickeyData:crePublickeyData accounts:accountNo];
	}
	NSMutableString *res = [[NSMutableString alloc] init];
	for (int i = 0; i < PIN_MAX_LENGTH; i++) {
		if (temp[i] < 16) {
			[res appendString:@"0"];
		}
		[res appendFormat:@"%X", temp[i]];
	}
	[res appendString:@"0A"];
    
    //3月6日陈德龙改密码键盘验证规则********************************
    if (accountNo) {
        if ([self checkWeakPassword111:src])
        {//简单密码
            [res appendString:@"06"];
        }
        else if ([self checkKindsOfPassword:src])
        {//至少数字，字母，符号中的两种
            [res appendString:@"04"];   //密码不是复杂组合的
        }else if ([self samePassword:src withAccounts:accountNo])
        {//连续手机号
            [res appendString:@"05"];   //密码不是复杂组合的
        }else {
            if ([src length] < 16) {
                [res appendString:@"0"];
            }
            [res appendFormat:@"%lX", (unsigned long)[src length]];	
        }
        NSString *result = [NSString stringWithString:res];
        return result;
    }
    //**********************************************************
	if ([self checkWeakPassword:src])
    {
		[res appendString:@"05"];
	}
    else if ([self checkComplexPassword:src accounts:accountNo])
    {
        [res appendString:@"04"];   //密码不是复杂组合的
    } else {
		if ([src length] < 16) {
			[res appendString:@"0"];
		}
		[res appendFormat:@"%lX", (unsigned long)[src length]];	
	}
	NSString *result = [NSString stringWithString:res];
	return result;
}

+ (NSString *) PkEncryptEPin:(NSString *)src crePublickeyData:(NSString *)crePublickeyData accounts:(NSString*)accountNo {
    int i = [src length];
	if(i >= PIN_MAX_LENGTH - 2)
		return nil;
	int integer = 0;
	int integer1 = i;
	unsigned char abyte0[2] = {integer, integer1};
	NSMutableString *mustr = [[NSMutableString alloc] initWithBytes:abyte0 length:2 encoding:NSUTF8StringEncoding];
	[mustr appendString: src];
	[mustr appendString: [self genRandomWithLength:PIN_MAX_LENGTH - i - 2]];
	
    //注释原RAS加密方式 开始
//	NSData *data = [Base64 decodeString:crePublickeyData];
//	SecKeyRef key = [[GFRSA sharedRSA] getPublicKeyFromCerData:data];
//	[[GFRSA sharedRSA] setPublicKey:key];
//	NSData *enData = [[GFRSA sharedRSA] doRSAEncryptNoPadding:mustr];
////	[mustr release];
//	if (key)
//    {
//		CFRelease(key);
//	}
    //注释原RAS加密方式 结束
    
    NSData *enData = [[GFOpensslRSA sharedOpensslRSA] doRSAEncryptNoPadding:mustr publicKey:crePublickeyData];
	
	//16进制字符
	unsigned char temp[PIN_MAX_LENGTH] = { 0 };
	@try {
		[enData getBytes:&temp range:NSMakeRange(0, PIN_MAX_LENGTH)];
	}
	@catch (NSException * e) {
		
//		ZNLog(@"Error enData length = %d", [enData length]);
		[self PkEncryptEPin:src crePublickeyData:crePublickeyData accounts:accountNo];
	}
	NSMutableString *res = [[NSMutableString alloc] init];
	for (int i = 0; i < PIN_MAX_LENGTH; i++) {
		if (temp[i] < 16) {
			[res appendString:@"0"];
		}		
		[res appendFormat:@"%X", temp[i]];
	}
	[res appendString:@"0E"];
    
    //3月6日陈德龙改密码键盘验证规则********************************
    if (accountNo) {
        if ([self checkWeakPassword111:src])
        {//简单密码
            [res appendString:@"06"];
        }else if ([self checkKindsOfPassword:src])
        {//至少数字，字母，符号中的两种
            [res appendString:@"04"];   //密码不是复杂组合的
        }else if ([self samePassword:src withAccounts:accountNo])
        {//连续手机号
            [res appendString:@"05"];   //密码不是复杂组合的
        }else {
            if ([src length] < 16) {
                [res appendString:@"0"];
            }
            [res appendFormat:@"%lX", (unsigned long)[src length]];
        }
        NSString *result = [NSString stringWithString:res];
        return result;
    }
    //**********************************************************

    
    
    
	if ([self checkWeakPassword:src]) {
		[res appendString:@"05"];   //密码连续一样的
	} else if ([self checkComplexPassword:src accounts:accountNo]) {
        [res appendString:@"04"];   //密码不是复杂组合的
    } else {
		if ([src length] < 16) {
			[res appendString:@"0"];
		}
		[res appendFormat:@"%lX", (unsigned long)[src length]];	
	}
	NSString *result = [NSString stringWithString:res];
//	[res release];
	
	return result;
}

+ (BOOL) checkComplexPassword:(NSString *)s accounts:(NSString*)accountNo {
    if ([s length] >= 8) {//大于等于8位才检查
        /////////////////////// start //////////////////////密码要两种类型组合以上的才行,但必需包括字母
        int number = 0;
        int charNumber = 0;
        int symbolNumber = 0;
        
        NSString *numbers = @"1234567890";
        NSString *characters = @"ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        NSString *symbols = @"~`!@#$%^&*()_-+={}[]|\\:;\"'<,>.?/";
        for (int i = 0; i < [s length]; i++) {
            NSString *c = [NSString stringWithFormat:@"%c", [s characterAtIndex:i]];
            if ([numbers rangeOfString:c].length>0) {
                if (number>0) {
                    continue;
                }
                number++;
            } else if ([characters rangeOfString:[c uppercaseString]].length>0){
                if (charNumber>0) {
                    continue;
                }
                charNumber++;
            } else if ([symbols rangeOfString:[c uppercaseString]].length>0){
                if (symbolNumber>0) {
                    continue;
                }
                symbolNumber++;
            }
        }
        
        if (charNumber==0) {
            return TRUE;
        } else if (number==0 && symbolNumber==0) {
            return TRUE;
        }
        //////////////////////////// end ///////////////////
        
        ///////////////// start /////////////////////
        const char* chs = [s UTF8String];
		int num = 0; //重复。
		int decNum = 0;//递减。
		int incNum = 0;//递增。
		int len = [s length] - 1;
        float M = 6 + (len - 5)*1.5;
		for (int i = 0; i < len; i++) {
			if (chs[i] == chs[i + 1]) {
				num++;
			}
			if (chs[i] == chs[i + 1] - 1) {
				decNum++;
			}
			if (chs[i] == chs[i + 1] + 1) {
				incNum++;
			}	
            if (num * 2 + (decNum+incNum)* 1.5 >= M) {
                return TRUE;
            }
		}
        ///////////////// end ///////////////////
        
        ////////////////////////// start //////////////////密码与用户名进行比较
       
        if (accountNo) {
            if ([accountNo isEqualToString:s]) {   //用户名和密码相同时
                return TRUE;
            } else if ([accountNo rangeOfString:s].length>0) { //密码与帐号中的某一段相同时（帐号比密码长时）
                return TRUE;
            } else if ([s rangeOfString:accountNo].length>0) { //帐号与密码中的某一段相同时（密码比帐号长时）
                return TRUE;
            }
                    
            //密码的某一段连续的组成字符与用户名的某一段连续的组成字符（含全部）相同，且该连续密码字符段的长度为m-k（m为密码长度，0<=k<=3）
            int k = 3;
            for (int i=0 ; i<=k; i++) {
                for (int j=i; j<=k; j++) {
                    NSString *passwordNo = [s substringFromIndex:i];
                    passwordNo = [passwordNo substringToIndex:[passwordNo length]-(k-j)];
                    if ([accountNo rangeOfString:passwordNo].length>0) {
                        return TRUE;
                        
                    }
                }
            }
        }
            
        ///////////////////////////// end //////////////
        
    }
    return FALSE;
}

+ (BOOL) checkWeakPassword:(NSString *)s{
    //密码不能够为连续8位相同的数字或字母，密码不能够为连续的8位顺序递增或者递减的数字或字母。
	if ([s length] >= 8) {//大于等于8位才检查
        const char* chs = [s UTF8String];
		int num = 0; //重复。
		int decNum = 0;//递减。
		int incNum = 0;//递增。
		int len = [s length] - 1;
		for (int i = 0; i < len; i++) {
			if (chs[i] == chs[i + 1]) {
				num++;
			} else {
				num = 0;
			}
			if (chs[i] == chs[i + 1] - 1) {
				decNum++;
			} else {
				decNum = 0;
			}
			if (chs[i] == chs[i + 1] + 1) {
				incNum++;
			} else {
				incNum = 0;
			}			
			if (num >= 7 || decNum >= 7 || incNum >= 7)
            {
                
				return TRUE;
			}
		}
		
	}
    
	return FALSE;	
}    

+ (BOOL)checkKindsOfPassword:(NSString *)s
{
    //密码要两种类型组合以上的才行
    if ([s length] >= 8) {//大于等于8位才检查
        int number = 0;
        int charNumber = 0;
        int symbolNumber = 0;
        
        NSString *numbers = @"1234567890";
        NSString *characters = @"ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        NSString *symbols = @"~`!@#$%^&*()_-+={}[]|\\:;\"'<,>.?/";
        for (int i = 0; i < [s length]; i++) {
            NSString *c = [NSString stringWithFormat:@"%c", [s characterAtIndex:i]];
            if ([numbers rangeOfString:c].length>0) {
                if (number>0) {
                    continue;
                }
                number++;
            } else if ([characters rangeOfString:[c uppercaseString]].length>0){
                if (charNumber>0) {
                    continue;
                }
                charNumber++;
            } else if ([symbols rangeOfString:[c uppercaseString]].length>0){
                if (symbolNumber>0) {
                    continue;
                }
                symbolNumber++;
            }
        }
        
        if (charNumber==0 && number==0) {
            return TRUE;
        }else if (charNumber==0 && symbolNumber==0) {
            return TRUE;
        }else if (number==0 && symbolNumber==0) {
            return TRUE;
        }
        //////////////////////////// end ///////////////////
        
        ///////////////// start /////////////////////
//        const char* chs = [s UTF8String];
//        int num = 0; //重复。
//        int decNum = 0;//递减。
//        int incNum = 0;//递增。
//        int len = [s length] - 1;
//        float M = 6 + (len - 5)*1.5;
//        for (int i = 0; i < len; i++) {
//            if (chs[i] == chs[i + 1]) {
//                num++;
//            }
//            if (chs[i] == chs[i + 1] - 1) {
//                decNum++;
//            }
//            if (chs[i] == chs[i + 1] + 1) {
//                incNum++;
//            }	
//            if (num * 2 + (decNum+incNum)* 1.5 >= M) {
//                return TRUE;
//            }
//        }
    }
    return FALSE;
}

+ (BOOL)samePassword:(NSString *)s withAccounts:(NSString *)accountNo
{
   ///密码与用户名进行比较
    if (accountNo) {
        if ([accountNo isEqualToString:s]) {   //用户名和密码相同时
            return TRUE;
        } else if ([accountNo rangeOfString:s].length>0) { //密码与帐号中的某一段相同时（帐号比密码长时）
            return TRUE;
        } else if ([s rangeOfString:accountNo].length>0) { //帐号与密码中的某一段相同时（密码比帐号长时）
            return TRUE;
        }
        
        //密码的某一段连续的组成字符与用户名的某一段连续的组成字符（含全部）相同，且该连续密码字符段的长度为m-k（m为密码长度，0<=k<=3）
        int k = 3;
        for (int i=0 ; i<=k; i++) {
            for (int j=i; j<=k; j++) {
                NSString *passwordNo = [s substringFromIndex:i];
                passwordNo = [passwordNo substringToIndex:[passwordNo length]-(k-j)];
                if ([accountNo rangeOfString:passwordNo].length>0) {
                    return TRUE;
                    
                }
            }
        }
    }
    return FALSE;
}

+ (BOOL)checkWeakPassword111:(NSString *)s
{
    const char* chs = [s UTF8String];
    int num = 0; //重复。
    int decNum = 0;//递减。
    int incNum = 0;//递增。
    int len = [s length] - 1;
    float M = 6 + (len - 5)*1.5;
    for (int i = 0; i < len; i++) {
        if (chs[i] == chs[i + 1]) {
            num++;
        }
        if (chs[i] == chs[i + 1] - 1) {
            decNum++;
        }
        if (chs[i] == chs[i + 1] + 1) {
            incNum++;
        }
        if (num * 2 + (decNum+incNum)* 1.5 >= M) {
            return TRUE;
        }
    }
    return FALSE;
}
@end
