//
//  InAppBrowser.m
//  IPRS
//
//  Created by 郭明健 on 2018/7/26.
//

#import "InAppBrowser.h"

@implementation InAppBrowser

- (void)open:(CDVInvokedUrlCommand *)command
{
    NSString *urlStr = command.arguments[0];
    NSURL *url = [NSURL URLWithString:urlStr];
    [[UIApplication sharedApplication] openURL:url];
}

@end
