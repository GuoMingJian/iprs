/**
 * 控制HTML字体大小
 * @auth chenzhiwen on 2017-7-18
 */
/**
 * 根据设备宽度控制字体大小
 * @param doc,win.document,window对象
 * @return html font-size
 */

(function (doc, win) {
  var docEl = doc.documentElement
  var resizeEvt = 'orientationchange' in window ? 'orientationchange' : 'resize'
  var recalc = function () {
    var clientWidth = docEl.clientWidth
    if (!clientWidth) return
    // 0.0378为1rem，标准为47px，w1242px
    docEl.style.fontSize = 29 * (clientWidth / 750) + 'px'
    docEl.style.height = '100%'
  }

  if (!doc.addEventListener) return
  win.addEventListener(resizeEvt, recalc, false)
  doc.addEventListener('DOMContentLoaded', recalc, false)
})(document, window)
